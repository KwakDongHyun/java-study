package dataStructure.set;

/**
 * Java의 Set Interface를 직접 구현해본다.
 *
 */
public interface Set<E> {

    /**
     * 요소가 set에 없는 경우 요소를 추가합니다.
     * @param e
     * @return
     */
    boolean add(E e);

    /**
     * 지정된 요소가 set에 있는 경우 해당 요소를 삭제합니다.
     * @param o
     * @return
     */
    boolean remove(Object o);

    /**
     * 현재 집합에 특정 요소가 포함되어 있는지 여부를 반환합니다.
     * @param o
     * @return
     */
    boolean contains(Object o);

    /**
     * 지정된 객체가 현재 집합과 같은지 여부를 반환합니다.
     * @param o
     * @return
     */
    boolean equals(Object o);

    /**
     * 현재 집합이 빈 상태인지 여부를 반환합니다.
     * @return
     */
    boolean isEmpty();

    /**
     * 현재 집합의 요소의 개수를 반환합니다.
     * @return
     */
    int size();

    /**
     * 집합의 모든 요소를 제거합니다.
     */
    void clear();

}
