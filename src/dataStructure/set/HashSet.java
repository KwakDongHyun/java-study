package dataStructure.set;

import java.util.Arrays;

/**
 * 실제로 Java에서는 HashSet은 HashMap으로 구현이 되어 있다.
 * 1. Hash란?
 * 임의의 길이를 갖는 데이터를 '고정된 길이의 데이터'로 변환(매핑)하는 것
 * SHA-512는 512bit의 고정된 길이, SHA-256은 256bit의 고정된 길이로 매핑한다는 의미다.
 * 이러한 과정을 hashing한다고 하며, 해시함수를 통해 얻어진 값을 다이제스트(Digest)라고 한다.
 *
 * 2. Hash를 사용하는 이유
 * 동일한 값에 대해서는 동일한 다이제스트를 갖기 때문에, 검색이 용이하다.
 * 즉, 특정 값에 대한 다이제스트는 변하지 않기 때문에 이 다이제스트 값을 index로 활용하여 검색을 빠르게 하기 위함이다.
 *
 * 3. HashSet의 기본 개념 정리
 * Hash Function을 통해 특정 값에 대한 다이제스트를 얻고, 해당 다이제스트를 index로 하는 메모리의 위치를 검색하여
 * 요소를 찾고 검사하는 원리이다.
 * 한편 해시 충들이 발생할 경우, Open Addressing 방식(인덱스와 인접한 빈 곳에 데이터를 채우는 방식)과
 * Separate Chaining 방식(연결리스트 형식으로 같은 인덱스에 리스트로 연결)이 있다.
 *
 * 4. Hash 주의사항
 * Java에서는 객체의 참조값으로 hashCode를 얻기 때문에, 객체 내부의 값을 비교하기 위해서는 재정의를 해야한다.
 * 즉, 해당 객체 클래스 내의 hashCode 메소드를 Override 해줘야 한다.
 *
 */
public class HashSet<E> implements Set<E> {

    private static final int DEFAULT_CAPACITY = 16;
    private static final float LOAD_FACTOR = 0.75f;

    Node<E>[] table;
    private int size;

    @SuppressWarnings({"unchecked"})
    public HashSet() {
        this.table = (Node<E>[]) new Node[DEFAULT_CAPACITY];
        this.size = 0;
    }

    // 보조 해시 함수 (상속 방지를 위해 private static final로 선언)
    private static final int hash(Object key) {
        int hash;
        if(key == null) {
            return 0;
        } else {
            // hashCode 메서드 결과에 bit 연산을 가하면 음수가 나올 수도 있으므로 절대값으로 양수로 변환해준다.
            /*
             * 요즘에는 hash 함수도 고른 분포를 갖을  수 있게 설계되어있기 때문에 큰 역할을 하지는 못하지만, 원래는 최대한 해시충돌을 피하기 위해 보조해시함수를 써서 고른 분포를 할 수 있도록 하기 위한 함수가 바로 위와같은 보조 해시 함수다.
             * 여기서는 Java 11의 구현과 유사하게 만들었다. key의 hashCode()의 절댓값과 그 값을 오른쪽으로 4비트 연산을 한 값하고의 XOR 값을 반환하도록 만들었다.
             * (만약 나머지 연산(%)을 할 경우에는 hashCode() 값이 음수가 나올 때를 고려하여 Math.abs() 함수를 써야하나, 만약 비트 &(AND) 연산을 통해 할 경우 Math.abs()를 써줄 필요가 없다.)
             */
            return Math.abs(hash = key.hashCode()) ^ (hash >>> 16);
        }
    }

    @Override
    public boolean add(E e) {
        return add(hash(e), e) == null;
    }

    private E add(int hash, E key) {
        int index = hash % table.length;
        // 2^n 일 경우, 비트 연산으로 해줄 수도 있다. 더군다나 비트 연산은 절대값 메소드를 사용하지 않아도 된다.
        // int index = hash & (table.length - 1);

        if(table[index] == null) {
            table[index] = new Node<E>(hash, key, null);
        } else {
            Node<E> temp = table[index];
            Node<E> prevNode = null;

            while(temp != null) {
                /*
                 * 현재 hash table에 저장된 노드의 객체와 같은 경우(hash 값이 동일하면서 key(data)가 같은 경우)
                 * 중복을 허용하지 않으므로 key를 반납(반환)
                 * key가 같은 경우는 객체 내용이 같거나 참조값이 같은 경우 2가지가 존재
                 */
                if((temp.hash == hash) && (temp.key == key || temp.key.equals(key))) {
                    return key;
                }
                prevNode = temp;
                temp = temp.next;
            }

            prevNode.next = new Node<E>(hash, key, null);
        }

        size++;
        if(size >= LOAD_FACTOR * table.length) {
            resizeSpecializedInBinary();
        }
        return null; // 정상적으로 추가되었을 경우 null을 반환
    }

    /**
     * 가장 보편적인 방식의 resize 메서드
     * 여태까지 자료구조를 직접 구현한 방법과 동일하다.
     */
    @SuppressWarnings({"unchecked"})
    private void resizeUniversally() {
        int newCapacity = table.length * 2;
        final Node<E>[] newTable = (Node<E>[]) new Node[newCapacity];

        for(int i = 0; i < table.length; i++) {
            // 기존 Hash Table에 존재하는 값
            Node<E> value = table[i];
            if(value == null) {
                continue;
            }

            table[i] = null;
            Node<E> nextNode; // Separate chaining 방식이므로 다음 노드의 데이터의 처리를 위해서 필요한 변수
            while(value != null) {
                int index = value.hash % newCapacity; // 새로운 인덱스를 구함.

                /*
                 * newTable에 index값이 겹칠 경우(해시 충돌)
                 */
                if(newTable[index] != null) {
                    Node<E> tail = newTable[index];
                    while(tail.next != null) {
                        tail = tail.next;
                    }

                    nextNode = value.next;
                    value.next = null; // 뒤에 연결된 다른 노드들은 포함시키지 않기 위해서
                    tail.next = value;
                } else {
                    nextNode = value.next;
                    value.next = null; // 뒤에 연결된 다른 노드들은 포함시키지 않기 위해서
                    newTable[index] = value;
                }

                value = nextNode;
            }

        }
        table = null;
        table = newTable;
    }

    /**
     * 용적이 2의 지수일 경우 사용가능한 resize 메서드
     * 수학적 특성 때문에 훨씬 빠르고 계산하기 쉽다.
     * 물론 단순하게 매번 (newCapacity - 1)을 & 연산으로 해도 되지만, 조금이라도 속도와 성능을 개선하기 위해서
     * 이와 같은 방법을 사용한다는 것을 알았으면 한다.
     * 예시를 먼저 보자.
     *
     * example 1
     * 기존 용적 : 8, 새로운 용적 : 16
     * (hash % capacity)
     * 9 % 8 = 1 index  → 9 % 16 = 9 index (high)
     * 17 % 8 = 1 index → 17 % 16 = 1 index (low)
     * 25 % 8 = 1 index → 25 % 16 = 9 index (high)
     * 33 % 8 = 1 index → 33 % 16 = 1 index (low)
     *
     * 2의 지수라는 특성상 기존 용적으로 나누었을 때 quotient이 even이면 새로운 해시 테이블에서도 같은 index에
     * 노드가 들어갈 것이다. 반면, odd인 경우 기존 용적 크기만큼 나머지가 발생하기 때문에 새로운 index 위치는
     * (기존 해시 테이블에서의 index + 기존 용적 크기의 값)이 될 것이다. (여기서는 1과 1+8=9 가 될 것이다)
     *
     */
    @SuppressWarnings({"unchecked"})
    private void resizeSpecializedInBinary() {

        int oldCapacity = table.length;
        int newCapacity = oldCapacity << 1;
        final Node<E>[] newTable = (Node<E>[]) new Node[newCapacity];

        for(int i = 0; i < oldCapacity; i++) {
            Node<E> data = table[i];
            if(data == null) {
                continue;
            }

            table[i] = null;
            if(data.next == null) {
                newTable[data.hash & (newCapacity - 1)] = data;
                continue;
            }

            /*
             * 두 리스트로 나눈 이유는 일일이 노드 1개씩 검사해서 넣으려면, 새로운 Hash table의 마지막 노드까지
             * 매 번 찾아가야한다. 어차피 새로운 hash table로 가는 index는 2가지 경우의 수 뿐이므로 불필요한
             * 탐색 과정을 없애기 위해서 연결리스트를 완성한 다음 한 번에 넣는 방법을 택한 것이다.
             */
            // 기존 hash table의 index와 동일한 곳에 배치되는 연결리스트의 head와 tail
            Node<E> lowHead = null;
            Node<E> lowTail = null;

            // 새로운 index에 배치되는 연결리스트의 head와 tail
            Node<E> highHead = null;
            Node<E> highTail = null;

            while(data != null) {

                if((data.hash & oldCapacity) == 0) {
                    // 몫이 짝수일 경우. low에 배정
                    if(lowHead == null) {
                        lowHead = data;
                    } else {
                        lowTail.next = data;
                    }
                    lowTail = data; // tail을 갱신.
                } else {
                    // 몫이 홀수일 경우. high에 배정
                    if(highHead == null) {
                        highHead = data;
                    } else {
                        highTail.next = data;
                    }
                    highTail = data; // tail을 갱신.
                }

                data = data.next; // Separate chaining 방식이므로 다음 노드의 데이터의 처리를 위해서 대입.
            }

            if(lowTail != null) {
                lowTail.next = null;
                newTable[i] = lowHead;
            }

            if(highTail != null) {
                highTail.next = null;
                newTable[i + oldCapacity] = highHead;
            }
        }

        table = newTable;
    }

    @Override
    public boolean remove(Object o) {
        return remove(hash(o), o) != null;
    }

    private Object remove(int hash, Object key) {

        int index = hash % table.length;
        Node<E> node = table[index];
        Node<E> removeNode = null;
        Node<E> prevNode = null;

        if(node == null) {
            return null;
        }

        while(node != null) {
            if(node.hash == hash && (node.key == key || node.key.equals(key))) {
                removeNode = node;

                if (prevNode == null) {
                    table[index] = node.next;
                    node = null;
                } else {
                    prevNode.next = node.next;
                    node = null;
                }

                size--;
                break;
            }

            prevNode = node;
            node = node.next;
        }

        return removeNode;
    }

    @Override
    public boolean contains(Object o) {
        int index = hash(o) % table.length;
        Node<E> temp = table[index];

        while(temp != null) {
            if(o == temp.key || (o != null && (o.equals(temp.key)))) {
                return true;
            }
            temp = temp.next;
        }

        return false;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void clear() {
        if(table != null && size > 0) {
            for(int i = 0; i < table.length; i++) {
                table[i] = null;
            }
            size = 0;
        }
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }

        if(!(obj instanceof HashSet)) {
            return false;
        }

        try {
            HashSet<E> set = (HashSet<E>) obj;
            if(set.size() != this.size) {
                return false;
            }

            for(int i = 0; i < set.table.length; i++) {
                Node<E> setElement = set.table[i];

                while(setElement != null) {
                    if(!contains(setElement)) {
                        return false;
                    }
                    setElement = setElement.next;
                }
            }

        } catch (ClassCastException e) {
            return false;
        }

        return true;
    }

    public Object[] toArray() {
        if(table == null) {
            return null;
        }

        Object[] array = new Object[size];
        int index = 0;

        for(int i = 0; i < table.length; i++) {
            Node<E> node = table[i];
            while(node != null) {
                array[index] = node.key;
                index++;
                node = node.next;
            }
        }

        return array;
    }

    @SuppressWarnings({"unchecked"})
    public <T> T[] toArray(T[] a) {
        Object[] copy = toArray();
        if(a.length < size) {
            return (T[]) Arrays.copyOf(copy, size, a.getClass());
        }

        System.arraycopy(copy, 0, a, 0, size);
        return a;
    }

}
