package dataStructure.set;

import java.util.Arrays;

public class LinkedHashSet<E> implements Set<E> {

    private static final int DEFAULT_CAPACITY = 16;
    private static final float LOAD_FACTOR = 0.75f;

    Node<E>[] table;
    private int size;

    private Node<E> head;
    private Node<E> tail;

    @SuppressWarnings({"unchecked"})
    public LinkedHashSet() {
        table = (Node<E>[]) new Node[DEFAULT_CAPACITY];
        this.size = 0;
        this.head = null;
        this.tail = null;
    }

    // 보조 해시 함수
    private static final int hash(Object key) {
        int hash;
        if(key == null) {
            return 0;
        } else {
            return Math.abs(hash = key.hashCode()) ^ (hash >>> 16);
        }
    }

    private void linkLastNode(Node<E> node) {
        Node<E> last = tail;
        tail = node;

        if(last == null) {
            head = node;
        } else {
            node.prevLink = last;
            last.nextLink = node;
        }
    }

    @Override
    public boolean add(E e) {
        return add(hash(e), e) == null;
    }

    private E add(int hash, E key) {
        int index = hash % table.length;
        Node<E> newNode = new Node<E>(hash, key, null);

        if(table[index] == null) {
            table[index] = newNode;
        } else {
            Node<E> node = table[index];
            Node<E> prevNode = null;

            while(node != null) {
                if((node.hash == hash) && (node.key == key || node.key.equals(key))) {
                    return key;
                }
                prevNode = node;
                node = node.next;
            }

            prevNode.next = newNode;
        }

        size++;
        linkLastNode(newNode);
        if(size >= LOAD_FACTOR * table.length) {
            resizeSpecializedInBinary();
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    private void resizeUniversally() {
        int newCapacity = table.length * 2;
        final Node<E>[] newTable = (Node<E>[]) new Node[newCapacity];

        for(int i = 0; i < table.length; i++) {
            Node<E> value = table[i];
            if(value == null) {
                continue;
            }

            table[i] = null;
            Node<E> nextNode;
            while(value != null) {
                int index = value.hash % newCapacity;

                if(newTable[index] != null) {
                    Node<E> tail = newTable[index];
                    while(tail.next != null) {
                        tail = tail.next;
                    }

                    nextNode = value.next;
                    value.next = null;
                    tail.next = value;
                } else {
                    nextNode = value.next;
                    value.next = null;
                    newTable[index] = value;
                }

                value = nextNode;
            }
        }

        table = null;
        table = newTable;
    }

    /**
     * 용적이 2의 지수일 경우 사용가능한 resize 메서드
     * 수학적 특성 때문에 훨씬 빠르고 계산하기 쉽다.
     * 물론 단순하게 매번 (newCapacity - 1)을 & 연산으로 해도 되지만, 조금이라도 속도와 성능을 개선하기 위해서
     * 이와 같은 방법을 사용한다는 것을 알았으면 한다.
     * 예시를 먼저 보자.
     *
     * example 1
     * 기존 용적 : 8, 새로운 용적 : 16
     * (hash % capacity)
     * 9 % 8 = 1 index  → 9 % 16 = 9 index (high)
     * 17 % 8 = 1 index → 17 % 16 = 1 index (low)
     * 25 % 8 = 1 index → 25 % 16 = 9 index (high)
     * 33 % 8 = 1 index → 33 % 16 = 1 index (low)
     *
     * 2의 지수라는 특성상 기존 용적으로 나누었을 때 quotient이 even이면 새로운 해시 테이블에서도 같은 index에
     * 노드가 들어갈 것이다. 반면, odd인 경우 기존 용적 크기만큼 나머지가 발생하기 때문에 새로운 index 위치는
     * (기존 해시 테이블에서의 index + 기존 용적 크기의 값)이 될 것이다. (여기서는 1과 1+8=9 가 될 것이다)
     *
     */
    @SuppressWarnings({"unchecked"})
    private void resizeSpecializedInBinary() {
        int oldCapacity = table.length;
        int newCapacity = oldCapacity << 1; // 왼쪽 1비트 shift는 이진수에서는 2배와 같음.
        final Node<E>[] newTable = (Node<E>[]) new Node[newCapacity];

        for(int i = 0; i < oldCapacity; i++) {
            Node<E> data = table[i];
            if(data == null) {
                continue;
            }
            /*
             * next가 없다는 건 해당 인덱스에 단일노드만 존재한다는 것과 같다.
             * 불필요한 연산없이 나머지 연산과 동일한 수식으로 그냥 넣기만 하면 된다.
             */
            table[i] = null;
            if(data.next == null) {
                newTable[data.hash & (newCapacity - 1)] = data;
                continue;
            }

            /*
             * 두 리스트로 나눈 이유는 일일이 노드 1개씩 검사해서 넣으려면, 새로운 Hash table의 마지막 노드까지
             * 매 번 찾아가야한다. 어차피 새로운 hash table로 가는 index는 2가지 경우의 수 뿐이므로 불필요한
             * 탐색 과정을 없애기 위해서 연결리스트를 완성한 다음 한 번에 넣는 방법을 택한 것이다.
             */
            // 기존 hash table의 index와 동일한 곳에 배치되는 연결리스트의 head와 tail
            Node<E> lowHead = null;
            Node<E> lowTail = null;

            // 새로운 index에 배치되는 연결리스트의 head와 tail
            Node<E> highHead = null;
            Node<E> highTail = null;

            while(data != null) {

                if((data.hash & oldCapacity) == 0) {
                    if(lowHead == null) {
                        lowHead = data;
                    } else {
                        lowTail.next = data;
                    }
                    lowTail = data;
                } else {
                    if(highHead == null) {
                        highHead = data;
                    } else {
                        highTail.next = data;
                    }
                    highTail = data;
                }

                data = data.next;
            }

            if(lowTail != null) {
                lowTail.next = null;
                newTable[i] = lowHead;
            }

            if(highTail != null) {
                highTail.next = null;
                newTable[i + oldCapacity] = highHead;
            }
        }

        table = newTable;
    }

    private void unlinkNode(Node<E> node) {
        Node<E> prevNode = node.prevLink;
        Node<E> nextNode = node.nextLink;

        if(prevNode == null) {
            head = nextNode;
        } else {
            prevNode.nextLink = nextNode;
            node.prevLink = null;
        }

        if(nextNode == null) {
            tail = prevNode;
        } else {
            nextNode.prevLink = prevNode;
            node.nextLink = null;
        }
    }

    @Override
    public boolean remove(Object o) {
        return remove(hash(o), o) != null;
    }

    private Object remove(int hash, Object key) {
        int index = hash % table.length;

        Node<E> node = table[index];
        Node<E> removeNode = null;
        Node<E> prevNode = null;

        if(node == null) {
            return null;
        }

        while(node != null) {
            if((node.hash == hash) && (node.key == key || node.key.equals(key))) {
                removeNode = node;

                if(prevNode == null) {
                    table[index] = node.next;
                } else {
                    prevNode.next = node.next;
                }

                unlinkNode(node);
                node = null;
                size--;
                break;
            }

            prevNode = node;
            node = node.next;
        }

        return removeNode;
    }

    @Override
    public boolean contains(Object o) {
        int index = hash(o) % table.length;
        Node<E> temp = table[index];

        while(temp != null) {
            if(o == temp.key || (o != null && temp.key.equals(o))) {
                return true;
            }
            temp = temp.next;
        }

        return false;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void clear() {
        if(table != null && size > 0) {
            for(int i = 0; i < table.length; i++) {
                table[i] = null;
            }
            size = 0;
        }

        head = null;
        tail = null;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }

        if(!(obj instanceof LinkedHashSet)) {
            return false;
        }

        try {
            LinkedHashSet<E> set = (LinkedHashSet<E>) obj;

            if(set.size() != this.size) {
                return false;
            }

            for(int i = 0; i < set.table.length; i++) {
                Node<E> setNode = set.table[i];

                while(setNode != null) {
                    if(!contains(setNode)) {
                        return false;
                    }
                    setNode = setNode.next;
                }

            }
        } catch (ClassCastException e) {
            return false;
        }

        return true;
    }

    public Object[] toArray() {
        if(table == null || head == null) {
            return null;
        }

        Object[] array = new Object[this.size];
        int index = 0;
        for(Node<E> node = head; node != null; node = node.nextLink) {
            array[index++] = node.key;
        }

        return array;
    }

    @SuppressWarnings({"unchecked"})
    public <T> T[] toArray(T[] a) {
        Object[] copy = toArray();
        if(a.length < this.size) {
            return (T[]) Arrays.copyOf(copy, size, a.getClass());
        }

        System.arraycopy(copy, 0, a, 0, size);
        return a;
    }

}
