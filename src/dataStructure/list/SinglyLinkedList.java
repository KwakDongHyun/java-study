package dataStructure.list;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. search 메소드 구현
 * 3. add 메소드 구현
 * 4. get, set, indexOf, contains 메소드 구현
 * 5. remove 메소드 구현
 * 6. size, isEmpty, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray, sort 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 */
public class SinglyLinkedList<E> implements List<E>, Cloneable {

    private Node<E> head; // 리스트의 첫번째 노드를 가리키는 변수
    private Node<E> tail; // 리스트의 마지막 노드를 가리키는 변수
    private int size; // 요소의 개수

    // 기본 생성자
    public SinglyLinkedList() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    // 특정 위치의 노드를 반환하는 메소드
    private Node<E> search(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        Node<E> x = head; // head가 가리키는 첫번째 노드부터 검색 시작

        for(int i = 0; i < index; i++) {
            x = x.next;
        }
        return x;
    }

    @Override
    public boolean add(E value) {
        addLast(value);
        return true;
    }

    public void addFirst(E value) {
        Node<E> newNode = new Node<E>(value);
        newNode.next = head; // 기존의 head를 새로운 노드의 뒤에 붙이기. 뒤에 이어붙인다고 생각하면 된다.
        head = newNode; // 새로운 노드를 head로 설정.
        size++;

        /*
         * 다음에 가리킬 노드가 없는 경우(노드가 1개만 있을 경우)
         * head와 tail이 같은 노드를 가리키도록 한다.
         */
        if(head.next == null) {
            tail = head;
        }
    }

    public void addLast(E value) {
        // head에 붙이는 것을 따로 분리하였기 때문에, 최초의 노드를 생성하는 거라면 addFirst를 실행한다.
        if(size == 0) {
            addFirst(value);
            return;
        }

        // 새로운 노드를 기존의 tail 뒤에 붙이기.
        Node<E> newNode = new Node<E>(value);
        tail.next = newNode;
        tail = newNode;
        size++;
    }

    @Override
    public void add(int index, E value) {
        if(index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        if(index == 0) {
            addFirst(value);
            return;
        }

        if(index == size) {
            addLast(value);
            return;
        }

        Node<E> previousNode = search(index - 1); // 추가하려는 위치의 이전 노드
        Node<E> nextNode = previousNode.next; // 추가하려는 위치의 노드
        Node<E> newNode = new Node<E>(value); // 추가하려는 새로운 노드

        previousNode.next = null;
        previousNode.next = newNode;
        newNode.next = nextNode;
        size++;
    }

    // 기본 삭제 메서드. head의 데이터를 지우는 것을 기본으로 한다.
    public E remove() {
        if(head == null) {
            throw new NoSuchElementException();
        }

        E element = head.data; // 삭제된 노드를 반환하기 위한 변수
        Node<E> nextNode = head.next; // head의 다음 노드
        head.data = null;
        head.next = null;

        head = nextNode;
        size--;

        /*
         * 삭제된 노드가 리스트의 유일한 노드였으면 tail도 null로 해줘야한다.
         * 즉 size가 0일 경우, head와 tail은 모두 없는 것과 마찬가지다.
         */
        if(size == 0) {
            tail = null;
        }
        return element;
    }

    @Override
    public E remove(int index) {
        if(index == 0) {
            return remove();
        }

        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        Node<E> previousNode = search(index - 1); // 삭제할 노드의 이전 노드
        Node<E> removeNode = previousNode.next; // 삭제할 노드
        Node<E> nextNode = removeNode.next; // 삭제할 노드의 다음 노드
        E element = removeNode.data; // 삭제할 노드의 데이터를 반환하기 위한 변수

        previousNode.next = nextNode;
        // 마지막 노드를 삭제한 경우, 삭제할 노드의 이전 노드가 tail이 된다.
        if(previousNode.next == null) {
            tail = previousNode;
        }

        removeNode.data = null;
        removeNode.next = null;
        size--;

        return element;
    }

    /*
     * 검색을 먼저 하는 이유는 다음과 같다.
     * 리스트가 비어있으면 head가 null일텐데 이 경우까지 고려해서 처리할 거라면 겉보기에는 비효율적으로 보일지라도
     * 검색을 먼저한 후에 데이터를 검증하는 것이 올바르다.
     */
    @Override
    public boolean remove(Object value) {
        // head부터 value와 동일한 데이터를 가진 node를 탐색한다.
        Node<E> targetNode = head;
        Node<E> previousNode = null;

        while(targetNode.next != null) {
            if(value.equals(targetNode.data)) {
                break;
            }
            previousNode = targetNode;
            targetNode = targetNode.next;
        }

        // 리스트가 비어있거나 끝까지 순회해봤지만 value를 찾지 못했다면 자연스레 null일 것이다.
        // 이 경우는 삭제를 못하므로 false를 반환한다.
        if(targetNode == null) {
            return false;
        }

        // head가 대상이라면 기본 삭제 메소드를 호출한다.
        // 같은 reference 값을 참조할 것이므로 equals로 충분하다.
        if(targetNode.equals(head)) {
            remove();
            return true;
        }

        previousNode.next = targetNode.next;
        // 마지막 노드를 삭제한 경우, 삭제할 노드의 이전 노드가 tail이 된다.
        if(previousNode.next == null) {
            tail = previousNode;
        }
        targetNode.data = null;
        targetNode.next = null;
        size--;
        return true;
    }

    @Override
    public E get(int index) {
        return search(index).data;
    }

    @Override
    public void set(int index, E value) {
        Node<E> targetNode = search(index);
        targetNode.data = null;
        targetNode.data = value;
    }

    @Override
    public boolean contains(Object value) {
        return indexOf(value) > -1;
    }

    @Override
    public int indexOf(Object value) {
        int index = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            if(value.equals(node.data)) {
                return index;
            }
            index++;
        }

        return -1;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public void clear() {
        for(Node<E> node = head; node != null;) {
            Node<E> nextNode = node.next;
            node.data = null;
            node.next = null;
            node = nextNode;
        }
        head = null;
        tail = null;
        size = 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object clone() throws CloneNotSupportedException {
        SinglyLinkedList<E> clone = (SinglyLinkedList<E>) super.clone();

        clone.head = null;
        clone.tail = null;
        clone.size = 0;

        for(Node<E> node = head; node != null; node = node.next) {
            clone.addLast(node.data);
        }
        return clone;
    }

    public Object[] toArray() {
        Object[] array = new Object[size];
        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            array[idx++] = node.data;
        }
        return array;
    }

    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] a) {
        if(a.length < size) {
            /*
             * Array.newInstance(컴포넌트 타입, 생성할 크기)
             * Generic의 특성상 component type을 모르기 때문에 생성할 수가 없다.
             * 이를 위해 고안한 API가 Array클래스의 newInstance인 것 같다.
             */
            a = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
        }

        int idx = 0;
        Object[] result = a;
        for(Node<E> node = head; node != null; node = node.next) {
            result[idx++] = node.data;
        }
        return a;
    }

    public void sort() {
        sort(null);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void sort(Comparator<? super E> c) {
        Object[] a = this.toArray();
        // 상속관계이면서 부모 클래스에서 정렬 방식을 따르는 경우도 생각하여 <? super E>로 하였다.
        // generic으로 구현되어 있지만 parameter로 넣어주는 것은 최상위 타입인 Object이므로
        // Comparator도 generic을 쓰지 않는 rawType(generic이 없는 보통 클래스)으로 선언해주는 것이다.
        Arrays.sort(a, (Comparator) c);

        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next, idx++) {
            node.data = (E) a[idx];
        }
    }

}
