package dataStructure.list;

import java.util.Arrays;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. resize 메소드 구현
 * 3. add 메소드 구현
 * 4. get, set, indexOf, contains 메소드 구현
 * 5. remove 메소드 구현
 * 6. size, isEmpty, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 */
public class ArrayList<E> implements List<E>, Cloneable {

    private static final int DEFAULT_CAPACITY = 10; // 기본 용적 크기. 최소 할당 용적
    private static final Object[] EMPTY_ARRAY = {}; // 기본 빈 배열. 용적이 0인 배열

    private Object[] array; // 요소를 담을 배열 (실질적인 저장소)
    private int size; // 배열에 담긴 요소의 개수.

    // 기본 생성자. 초기 공간 할당이 없는 경우.
    public ArrayList() {
        this.array = EMPTY_ARRAY;
        this.size = 0;
    }

    // 초기 공간 할당이 있는 경우.
    public ArrayList(int capacity) {
        this.array = new Object[capacity];
        this.size = 0;
    }

    @Override
    public boolean add(E value) {
        addLast(value);
        return true;
    }

    public void addLast(E value) {
        if(size == array.length) {
            resize();
        }
        array[size] = value; // 마지막 위치에 요소 추가
        size++; // 요소의 개수 1 증가
    }

    @Override
    public void add(int index, E value) {
        // 영역을 벗어날 경우 예외 발생
        if(index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        if(index == size) {
            addLast(value);
        } else {
            if(size == array.length) {
                resize();
            }

            for(int i = size; i > index; i--) {
                array[i] = array[i - 1]; // size가 다음에 들어갈 index를 가리키므로, 1칸씩 뒤로 당기는 효과가 있다.
            }

            array[index] = value;
            size++;
        }
    }

    public void addFirst(E value) {
        add(0, value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public E remove(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        E element = (E) array[index];

        // 삭제한 요소의 뒤에 있는 모든 요소들을 한 칸씩 당겨준다.
        for(int i = index; i < size-1; i++) {
            array[i] = array[i + 1];
        }
        array[size - 1] = null;
        size--;
        resize();
        return element;
    }

    @Override
    public boolean remove(Object value) {
        // 삭제하고자 하는 요소의 인덱스 검색색
        int index = indexOf(value);

        if(index == -1) {
            return false;
        }

        remove(index);
        return true;
    }

    // 배열의 위치를 찾아가는 것이기 때문에 반드시 잘못된 위치 참조에 대한 예외처리를 해주어야 한다.
    // type safe(타입 안정성)에 대해 경고하는 것을 무시한다. E타입만을 add하기 때문에 변환할 수 없는 타입이 들어올 가능성은 없다.
    // 이 메소드는 ClassCastException이 발생하지 않는다는 것을 보장한다.
    @SuppressWarnings("unchecked")
    @Override
    public E get(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        return (E) array[index]; // Object Type에서 E타입으로 캐스팅 후 반환.
    }

    // 기존에 index에 위치한 데이터를 새로운 데이터(value)로 '교체'하는 것이다.
    @Override
    public void set(int index, E value) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        array[index] = value; // 해당 인덱스의 요소를 교체
    }

    @Override
    public boolean contains(Object value) {
        if(indexOf(value) > -1) {
            return true;
        }

        return false;
    }

    // 사용자가 찾고자 하는 요소(value)의 '위치(index)'를 반환하는 메소드다.
    @Override
    public int indexOf(Object value) {
        for(int i = 0; i < size; i++) {
            if(array[i].equals(value)) {
                return i;
            }
        }

        return -1;
    }

    // 사용자가 찾고자 하는 요소(value)의 '위치(index)'를 반환하는 메소드다.
    // 뒤에서부터 검색한다.
    public int lastIndexOf(Object value) {
        for(int i = size-1; i > -1; i--) {
            if(array[i].equals(value)) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public int size() {
        return this.size;
    }

    // 데이터에 따라서 최적화 된 용적이 필요할 경우 사용.
    // 데이터 손상을 방지하기 위해서 private으로 접근 제어자를 설정
    private void resize() {
        int arrayCapacity = array.length;

        // 배열의 용적이 0과 마찬가지일 경우.
        // 또한 주소가 아닌 값을 비교해야 하기 때문에 Arrays.equals() 메소드를 사용하도록 하자.
        if(Arrays.equals(array, EMPTY_ARRAY)) {
            array = new Object[DEFAULT_CAPACITY];
            return;
        }

        // 배열이 꽉 찼다면 2배만큼 용량을 늘린다. 요소의 개수가 용적만큼 꽉 차있는 것을 의미함.
        // 만약 복사할 배열보다 용적의 크기가 클 경우 먼저 배열을 복사한 뒤, 나머지 빈 공간은 null로 채워지기 때문에 편리하다.
        if(size == arrayCapacity) {
            int newCapacity = arrayCapacity * 2;
            array = Arrays.copyOf(array, newCapacity);
            return;
        }

        // 요소의 개수가 용적의 절반 미만일 경우, 최적화를 위해 용량을 현재 배열의 절반으로 줄인다.
        // 만약 복사할 배열보다 용적의 크기가 작을경우 새로운 용적까지만 복사하고 반환하기 때문에 예외발생에 대해 안전하다.
        if(size < (arrayCapacity / 2)) {
            int newCapacity = arrayCapacity / 2;
            array = Arrays.copyOf(array, newCapacity);
            return;
        }
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public void clear() {
        for(int i = 0; i < size; i++) {
            array[i] = null;
        }
        size = 0;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        // 새로운 ArrayList 객체 생성
        ArrayList<E> cloneList = (ArrayList<E>) super.clone();

        // 내부의 참조값들은 Shallow Copy가 되므로 새로 생성해줘야 한다.
        cloneList.array = new Object[size];

        System.arraycopy(array, 0, cloneList.array, 0, size);

        return cloneList;
    }

    // ArrayList를 Object 배열로 반환
    public Object[] toArray() {
        return Arrays.copyOf(array, size);
    }

    // ArrayList의 데이터를 parameter로 들어온 타입의 배열에 복사하여 반환.
    // 상속관계에 있는 객체 클래스나 Wrapper Class 타입의 배열로 반환 가능
    public <T> T[] toArray(T[] a) {
        if(a.length < size) {
            // Class<? extends T[]> 타입 파라미터 추가
            // 파라미터 배열의 크기가 작을 경우, 원본 배열 크기만큼을 복사한 배열을 return
            return (T[]) Arrays.copyOf(array, size, a.getClass());
        }

        // 파라미터 배열에 데이터를 복사. parameter를 반환
        System.arraycopy(array, 0, a, 0, size);
        return a;
    }

}
