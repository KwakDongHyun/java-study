package dataStructure.list;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. search 메소드 구현
 * 3. add 메소드 구현
 * 4. get, set, indexOf, contains 메소드 구현
 * 5. remove 메소드 구현
 * 6. size, isEmpty, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray, sort 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 */
public class DoublyLinkedList<E> implements List<E>, Cloneable {

    private Node<E> head; // 리스트의 첫번째 노드를 가리키는 변수
    private Node<E> tail; // 리스트의 마지막 노드를 가리키는 변수
    private int size; // 요소의 개수

    public DoublyLinkedList() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    // 특정 위치의 노드를 반환하는 메소드
    // 가능하면 검색 메소드를 따로 구현하는 것이 편리. 여러 곳에서 사용하게 된다.
    private Node<E> search(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        // 인덱스가 size의 절반 이하라면 앞에서부터 검색. 그게 아니라면 뒤에서부터 검색
        Node<E> targetNode;
        if(index <= size / 2) {
            targetNode = head;
            for(int i = 0; i < index; i++) {
                targetNode = targetNode.next;
            }
        } else {
            targetNode = tail;
            for(int i = size-1; i > index; i--) {
                targetNode = targetNode.prev;
            }
        }
        return targetNode;
    }

    @Override
    public boolean add(E value) {
        addLast(value);
        return true;
    }

    public void addFirst(E value) {
        Node<E> newNode = new Node<E>(value);
        newNode.next = head;

        /*
         * head가 null이 아닐 경우에만 기존의 첫번째 노드의 prev 변수가 새 노드를 가리키도록 한다.
         * null이면 새로 추가한 노드가 첫번째 노드가 되므로 이전 노드는 없기 때문이다.
         */
        if(head != null) {
            head.prev = newNode;
        }
        head = newNode;
        size++;

        /*
         * 새로운 노드밖에 없다면 tail과 head는 같은 값을 가리켜야한다.
         * 즉, 첫번째이자 마지막 노드가 된다.
         * 이전 단일 연결리스트에서도 설명했듯이 미리 점검할 수 없는 이유는 만약 head가 null이면
         * head.next는 NullPointerException을 발생시키기 때문이다.
         */
        if(head.next == null) {
            tail = head;
        }
    }

    public void addLast(E value) {
        if(size == 0) {
            addFirst(value);
            return;
        }

        Node<E> newNode = new Node<E>(value);
        tail.next = newNode;
        newNode.prev = tail;
        tail = newNode;
        size++;
    }

    @Override
    public void add(int index, E value) {
        if(index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        if(index == 0) {
            addFirst(value);
            return;
        }

        if(index == size) {
            addLast(value);
            return;
        }

        Node<E> previousNode = search(index - 1);
        Node<E> nextNode = previousNode.next;
        Node<E> newNode = new Node<E>(value);

        previousNode.next = null;
        nextNode.prev = null;

        previousNode.next = newNode;
        newNode.prev = previousNode;
        newNode.next = nextNode;
        nextNode.prev = newNode;
        size++;
    }

    // 기본 삭제 메서드. head의 데이터를 지우는 것을 기본으로 한다.
    public E remove() {
        if(head == null) {
            throw new NoSuchElementException();
        }

        E element = head.data;
        Node<E> nextNode = head.next;

        head.data = null;
        head.next = null;

        /*
         * 두번째 노드가 존재할 경우에만 prev 변수를 null처리 해줘야한다.
         * 첫번째 노드밖에 없다면 null에서 prev 변수를 찾는 것과 동일하므로 NullPointerException이 발생한다.
         */
        if(nextNode != null) {
            nextNode.prev = null;
        }

        head = nextNode;
        size--;

        /*
         * 리스트에 1개의 노드만 있는 상황에서 삭제를 한 것이라면 tail도 null을 가리켜야한다.
         */
        if(size == 0) {
            tail = null;
        }

        return element;
    }

    @Override
    public E remove(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if(index == 0) {
            return remove();
        }

        Node<E> previousNode = search(index - 1);
        Node<E> removeNode = previousNode.next;
        Node<E> nextNode = removeNode.next;

        E element = removeNode.data;

        previousNode.next = null;
        removeNode.next = null;
        removeNode.prev = null;
        removeNode.data = null;

        /*
         * 삭제할 노드가 마지막 노드라면 tail을 마지막 노드의 이전 노드를 가리켜야한다.
         */
        if(nextNode == null) {
            tail = previousNode;
        } else {
            nextNode.prev = null;
            nextNode.prev = previousNode;
            previousNode.next = nextNode;
        }

        size--;
        return element;
    }

    /*
     * 검색을 먼저 하는 이유는 다음과 같다.
     * 리스트가 비어있으면 head가 null일텐데 이 경우까지 고려해서 처리할 거라면 겉보기에는 비효율적으로 보일지라도
     * 검색을 먼저한 후에 데이터를 검증하는 것이 올바르다.
     */
    @Override
    public boolean remove(Object value) {
        Node<E> prevNode = null;
        Node<E> targetNode = head;

        for(; targetNode != null; targetNode = targetNode.next) {
            if(value.equals(targetNode.data)) {
                break;
            }
            prevNode = targetNode;
        }

        // 리스트가 비어있거나 끝까지 순회해봤지만 value를 찾지 못했다면 자연스레 null일 것이다.
        // 이 경우는 삭제를 못하므로 false를 반환한다.
        if(targetNode == null) {
            return false;
        }

        // head가 대상이라면 기본 삭제 메소드를 호출한다.
        // 같은 reference 값을 참조할 것이므로 equals로 충분하다.
        if(targetNode.equals(head)) {
            remove();
            return true;
        }

        Node<E> nextNode = targetNode.next;

        prevNode.next = null;
        targetNode.data = null;
        targetNode.next = null;
        targetNode.prev = null;

        // 마지막 노드를 삭제한 경우, 삭제할 노드의 이전 노드가 tail이 된다.
        if(nextNode == null) {
            tail = prevNode;
        } else {
            nextNode.prev = null;
            nextNode.prev = prevNode;
            prevNode.next = nextNode;
        }

        size--;
        return true;
    }

    @Override
    public E get(int index) {
        return search(index).data;
    }

    @Override
    public void set(int index, E value) {
        Node<E> replacementNode = search(index);
        replacementNode.data = null;
        replacementNode.data = value;
    }

    @Override
    public boolean contains(Object value) {
        return indexOf(value) > -1;
    }

    @Override
    // head부터 정방향으로 탐색을 한다.
    public int indexOf(Object value) {
        int index = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            if(value.equals(node.data)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    // tail부터 역방향으로 탐색을 한다.
    public int lastIndexOf(Object value) {
        int index = size-1;
        for(Node<E> node = tail; node != null; node = node.prev) {
            if(value.equals(node.data)) {
                return index;
            }
            index--;
        }
        return -1;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void clear() {
        for(Node<E> node = head; node != null;) {
            Node<E> nextNode = node.next;
            node.data = null;
            node.next = null;
            node.prev = null;
            node = nextNode;
        }
        head = null;
        tail = null;
        size = 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Object clone() throws CloneNotSupportedException {
        DoublyLinkedList<E> clone = (DoublyLinkedList<E>) super.clone();

        clone.head = null;
        clone.tail = null;
        clone.size = 0;

        for(Node<E> node = head; node != null; node = node.next) {
            clone.addLast(node.data);
        }

        return clone;
    }

    public Object[] toArray() {
        Object[] array = new Object[size];
        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            array[idx++] = node.data;
        }
        return array;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> T[] toArray(T[] a) {
        if(a.length < size) {
            a = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
        }

        int idx = 0;
        Object[] result = a;
        for(Node<E> node = head; node != null; node = node.next) {
            result[idx++] = node.data;
        }

        return a;
    }

    public void sort() {
        sort(null);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void sort(Comparator<? super E> c) {
        Object[] a = this.toArray();
        // generic으로 구현되어 있지만 parameter로 넣어주는 것은 최상위 타입인 Object이므로
        // Comparator도 generic을 쓰지 않는 rawType(generic이 없는 보통 클래스)으로 선언해줘도 것이다.
        // 본래는 아래와 같이 하는게 맞다. 그냥 (Comparator)도 위의 이론대로 되기는 하지만..
        Arrays.sort(a, (Comparator<? super Object>) c);

        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next, idx++) {
            node.data = (E) a[idx];
        }
    }

}
