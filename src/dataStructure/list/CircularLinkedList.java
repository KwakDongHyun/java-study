package dataStructure.list;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. search 메소드 구현
 * 3. add 메소드 구현
 * 4. get, set, indexOf, contains 메소드 구현
 * 5. remove 메소드 구현
 * 6. size, isEmpty, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray, sort 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 */
public class CircularLinkedList<E> implements List<E>, Cloneable {

    private Node<E> head; // 리스트의 첫번째 노드를 가리키는 변수
    private Node<E> tail; // 리스트의 마지막 노드를 가리키는 변수
    private int size; // 요소의 개수

    // 기본 생성자
    public CircularLinkedList() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    // 특정 위치의 노드를 반환하는 메소드
    private Node<E> search(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        Node<E> targetNode = head;
        for(int i = 0; i < index; i++) {
            targetNode = targetNode.next;
        }
        return targetNode;
    }

    /**
     * 실제 Java Library에서도 그렇지만 리스트에서 add는 기본적으로 마지막에 데이터를 넣는다.
     * 즉 addLast가 기본이다.
     * @param value 리스트에 추가할 요소
     * @return
     */
    @Override
    public boolean add(E value) {
        addLast(value);
        return false;
    }

    public void addFirst(E value) {
        Node<E> newNode = new Node<E>(value);
        newNode.next = head; // 기존의 head를 새로운 노드의 뒤에 붙이기.
        head = newNode;

        /*
         * 노드가 1개만 있을 경우, 원형 리스트의 특성상 포인터가 자기 자신을 가리켜야 한다.
         * 즉, 첫번째이자 마지막 노드가 된다.
         * 즉 head.next가 head 본인을 가리켜야함.
         * 이전 단일 연결리스트에서도 설명했듯이 미리 점검할 수 없는 이유는 만약 head가 null이면
         * head.next는 NullPointerException을 발생시키기 때문이다.
         */
        if(head.next == null) {
            tail = head;
        }
        tail.next = head;
        size++;
    }

    public void addLast(E value) {
        if(size == 0) {
            addFirst(value);
            return;
        }

        Node<E> newNode = new Node<E>(value);
        tail.next = newNode;
        tail = newNode;
        newNode.next = head;
        size++;
    }

    @Override
    public void add(int index, E value) {
        if(index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        if(index == 0) {
            addFirst(value);
            return;
        }

        if(index == size) {
            addLast(value);
            return;
        }

        Node<E> previousNode = search(index - 1);
        Node<E> nextNode = previousNode.next;
        Node<E> newNode = new Node<E>(value);

        previousNode.next = null;
        previousNode.next = newNode;
        newNode.next = nextNode;
        size++;
    }

    // 기본 삭제 메서드. head의 데이터를 지우는 것을 기본으로 한다.
    public E remove() {
        if(head == null) {
            throw new NoSuchElementException();
        }

        E element = head.data;
        Node<E> nextNode = head.next;
        head.data = null;
        head.next = null;
        size--;

        /*
         * 삭제된 노드가 리스트의 유일한 노드였으면 tail도 null로 해줘야한다.
         * 원형 연결리스트의 특성상 1개의 노드일 경우 자기 참조를 하기 때문에 명시적으로 null을 선언해줘야 한다.
         * 또한, tail은 head가 삭제될 경우 첫번째 노드의 다음 노드를 가리켜서 순환을 유지시켜야 한다.
         */
        if(size == 0) {
            head = null; // 1개의 노드일 경우 자기 자신을 계속 참조하기 때문에, 명시적으로 null을 대입해줘야 한다.
            tail = null;
        } else {
            head = nextNode;
            tail.next = nextNode; // 두번째 노드를 head로 가리켰기 때문에, tail.next도 변경해줘야 한다.
        }
        return element;
    }

    @Override
    public E remove(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if(index == 0) {
            return remove();
        }

        Node<E> previousNode = search(index - 1);
        Node<E> removeNode = previousNode.next;
        Node<E> nextNode = removeNode.next;
        E element = removeNode.data;

        previousNode.next = nextNode;
        /*
         * 마지막 노드를 삭제한 경우, 삭제할 노드의 이전 노드가 tail이 된다.
         * 마지막 노드를 삭제했더라도 tail.next는 head이므로 자동으로 가리키게 된다.
         */
        if(previousNode.equals(head)) {
            tail = previousNode;
        }

        removeNode.data = null;
        removeNode.next = null;
        size--;

        return element;
    }

    @Override
    public boolean remove(Object value) {
        // head부터 value와 동일한 데이터를 가진 node를 탐색한다.
        Node<E> targetNode = head;
        Node<E> previousNode = null;
        boolean hasValue = false;

        for(; targetNode != null; targetNode = targetNode.next) {
            if(value.equals(targetNode.data)) {
                hasValue = true;
                break;
            }
            previousNode = targetNode;

            // tail까지 도달했을 경우, 순환 구조이기 때문에 강제로 나와야한다.
            if(targetNode.next.equals(head)) {
                break;
            }
        }

        // 리스트가 비어있거나 끝까지 순회해봤지만 value를 찾지 못했을 경우
        // 단일 연결리스트와 다른 점은 순환 구조이기 때문에 null만으로 구분하기가 어렵다.
        // 별도의 boolean 변수를 쓸 수 밖에 없다.
        if(!hasValue) {
            return false;
        }

        if(targetNode.equals(head)) {
            remove();
            return true;
        }

        previousNode.next = targetNode.next;
        // 마지막 노드를 삭제한 경우, 삭제할 노드의 이전 노드가 tail이 된다.
        // 원형 연결리스트의 경우 targetNode의 next가 head이면 tail 노드인 것을 알 수 있다.
        if(previousNode.next.equals(head)) {
            tail = previousNode;
        }
        targetNode.data = null;
        targetNode.next = null;
        size--;
        return true;
    }

    @Override
    public E get(int index) {
        return search(index).data;
    }

    @Override
    public void set(int index, E value) {
        Node<E> replacementNode = search(index);
        replacementNode.data = null;
        replacementNode.data = value;
    }

    @Override
    public boolean contains(Object value) {
        return indexOf(value) > -1;
    }

    @Override
    public int indexOf(Object value) {
        int index = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            if(value.equals(node.data)) {
                return index;
            }
            index++;

            // tail까지 도달해서 검사했음에도 불구하고 찾지 못했을 경우
            // 순환 구조이기 때문에 강제로 나와야한다.
            if(node.next.equals(head)) {
                return -1;
            }
        }

        return -1;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public void clear() {
        for(Node<E> node = head; node != null;) {
            Node<E> nextNode = node.next;
            node.data = null;
            node.next = null;
            node = nextNode;

            // tail까지 모두 clear 했을 경우. 순환 구조이기 때문에 강제로 나와야한다.
            if(nextNode.equals(head)) {
                break;
            }
        }
        head = null;
        tail = null;
        size = 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Object clone() throws CloneNotSupportedException {
        CircularLinkedList<E> clone = (CircularLinkedList<E>) super.clone();

        clone.head = null;
        clone.tail = null;
        clone.size = 0;

        for(Node<E> node = head; node != null; node = node.next) {
            clone.addLast(node.data);
            // tail까지 모든 노드를 순회했을 경우. 순환 구조이기 때문에 강제로 나와야한다.
            if(node.next.equals(head)) {
                break;
            }
        }
        return clone;
    }

    public Object[] toArray() {
        Object[] array = new Object[size];
        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            array[idx++] = node.data;
            // tail까지 모든 노드를 순회했을 경우. 순환 구조이기 때문에 강제로 나와야한다.
            if(node.next.equals(head)) {
                break;
            }
        }

        return array;
    }

    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] a) {
        if(a.length < size) {
            /*
             * Array.newInstance(컴포넌트 타입, 생성할 크기)
             * Generic의 특성상 component type을 모르기 때문에 생성할 수가 없다.
             * 이를 위해 고안한 API가 Array클래스의 newInstance인 것 같다.
             */
            a = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
        }

        int idx = 0;
        Object[] result = a;
        for(Node<E> node = head; node != null; node = node.next) {
            result[idx++] = node.data;
            // tail까지 모든 노드를 순회했을 경우. 순환 구조이기 때문에 강제로 나와야한다.
            if(node.next.equals(head)) {
                break;
            }
        }

        return a;
    }

    public void sort() {
        sort(null);
    }

    @SuppressWarnings({"unchecked"})
    public void sort(Comparator<? super E> c) {
        Object[] a = this.toArray();
        // 상속관계이면서 부모 클래스에서 정렬 방식을 따르는 경우도 생각하여 <? super E>로 하였다.
        // generic으로 구현되어 있지만 parameter로 넣어주는 것은 최상위 타입인 Object이므로
        // Comparator도 generic을 쓰지 않는 rawType(generic이 없는 보통 클래스)으로 선언해주는 것이다.
        Arrays.sort(a, (Comparator<? super Object>) c);

        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            node.data = (E) a[idx++];
            // tail까지 모든 노드를 순회했을 경우. 순환 구조이기 때문에 강제로 나와야한다.
            if(node.next.equals(head)) {
                break;
            }
        }
    }

}
