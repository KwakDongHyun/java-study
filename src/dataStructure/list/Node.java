package dataStructure.list;

public class Node<E> {

    E data;
    Node<E> next; // 다음 노드를 가리키는 Reference 변수
    Node<E> prev; // 이전 노드를 가리키는 Reference 변수

    Node(E data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }

}
