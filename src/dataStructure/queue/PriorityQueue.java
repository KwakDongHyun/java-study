package dataStructure.queue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자와 필수 메소드 구성
 * 2. resize 메소드 구현
 * 3. offer(=add) 메소드 구현
 * 4. poll(=remove) 메소드 구현
 * 5. size, peek, isEmpty, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 * -- 메모 --
 * 1. 배열을 기반으로 구현한 Heap을 사용해서 우선순위큐를 구현한다. (우선순위큐는 '추상적인 모델'이고
 *    구현하는데 있어서 가장 대표적인 구현 방식은 'Heap' 이라는 자료구조를 활용하는 방식이다.)
 * 2. 실제 자바에서는 Queue Interface를 구현하는 것은 ArrayDeque, LinkedList, PriorityQueue 3가지가 있다.
 */
public class PriorityQueue<E> implements Queue<E>, Cloneable {

    private static final int DEFAULT_CAPACITY = 16;
    private Comparator<? super E> comparator;

    private int size;
    private Object[] array;

    public PriorityQueue() {
        this(null);
    }

    public PriorityQueue(Comparator<? super E> comparator) {
        this.array = new Object[DEFAULT_CAPACITY];
        this.comparator = comparator;
        this.size = 0;
    }

    public PriorityQueue(int capacity) {
        this(null, capacity);
    }

    public PriorityQueue(Comparator<? super E> comparator, int capacity) {
        this.array = new Object[capacity];
        this.comparator = comparator;
        this.size = 0;
    }

    private int getParent(int index) {
        return index / 2;
    }

    private int getLeftChild(int index) {
        return index * 2;
    }

    private int getRightChild(int index) {
        return index * 2 + 1;
    }

    private void resize(int newCapacity) {
        Object[] newArray = new Object[newCapacity];
        for(int i = 1; i <= size; i++) {
            newArray[i] = array[i];
        }
        this.array = null;
        this.array = newArray;
    }

    @Override
    public boolean offer(E e) {
        if(size + 1 == array.length) {
            resize(array.length * 2);
        }
        siftUp(size + 1, e);
        size++;
        return true;
    }

    private void siftUp(int index, E target) {
        if(this.comparator != null) {
            siftUpComparator(index, target);
        } else {
            siftUpComparable(index, target);
        }
    }

    @SuppressWarnings({"unchecked"})
    private void siftUpComparator(int index, E target) {
        int parent;
        Object parentVal;

        while(index > 1) {
            parent = getParent(index);
            parentVal = array[parent];

            if(this.comparator.compare(target, (E) parentVal) >= 0) {
                break;
            }

            array[index] = parentVal;
            index = parent;
        }

        array[index] = target;
    }

    @SuppressWarnings({"unchecked"})
    private void siftUpComparable(int index, E target) {
        int parent;
        Object parentVal;
        Comparable<? super E> comparable = (Comparable<? super E>) target;

        while(index > 1) {
            parent = getParent(index);
            parentVal = array[parent];

            if(comparable.compareTo((E) parentVal) >= 0) {
                break;
            }

            array[index] = parentVal;
            index = parent;
        }

        array[index] = target;
    }

    @Override
    public E poll() {
       if(array[1] == null) {
           return null;
       }
       return remove();
    }

    @SuppressWarnings({"unchecked"})
    public E remove() {
        if(array[1] == null) {
            throw new NoSuchElementException();
        }

        E result = (E) array[1];
        E target = (E) array[size];

        array[size] = null;
        size--;
        siftDown(1, target);
        return result;
    }

    private void siftDown(int index, E target) {
        if(this.comparator != null) {
            siftDownComparator(index, target);
        } else {
            siftDownComparable(index, target);
        }
    }

    @SuppressWarnings({"unchecked"})
    private void siftDownComparator(int index, E target) {
        array[index] = null;
        int parent = index;
        int child, rightChild;
        Object childVal;

        while((child = getLeftChild(parent)) <= size) {
            rightChild = getRightChild(parent);
            childVal = array[child];

            if(rightChild <= size && this.comparator.compare((E) childVal, (E) array[rightChild]) > 0) {
                child = rightChild;
                childVal = array[child];
            }

            if(this.comparator.compare(target, (E) childVal) <= 0) {
                break;
            }

            array[parent] = childVal;
            parent = child;
        }

        array[parent] = target;
        if(size < array.length / 4) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }
    }

    @SuppressWarnings({"unchecked"})
    private void siftDownComparable(int index, E target) {
        Comparable<? super E> comparable = (Comparable<? super E>) target;
        array[index] = null;
        int parent = index;
        int child, rightChild;
        Object childVal;

        while((child = getLeftChild(parent)) <= size) {
            rightChild = getRightChild(parent);
            childVal = array[child];

            if(rightChild <= size && ((Comparable<? super E>) childVal).compareTo((E) array[rightChild]) > 0) {
                child = rightChild;
                childVal = array[child];
            }

            if(this.comparator.compare(target, (E) childVal) <= 0) {
                break;
            }

            array[parent] = childVal;
            parent = child;
        }

        array[parent] = target;
        if(size < array.length / 4) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }
    }

    public int size() {
        return this.size;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public E peek() {
        if(array[1] == null) {
            throw new NoSuchElementException();
        }
        return (E) array[1];
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean contains(Object value) {
        for(int i = 1; i <= size; i++) {
            if(array[i].equals(value)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        for(int i = 1; i <= size; i++) {
            array[i] = null;
        }
        size = 0;
    }

    public Object[] toArray() {
        return toArray(new Object[size]);
    }

    @SuppressWarnings({"unchecked"})
    public <T> T[] toArray(T[] a) {
        if(a.length <= size) {
            return (T[]) Arrays.copyOfRange(array, 1, size + 1, a.getClass());
        }
        System.arraycopy(array, 1, a, 0, size);
        return a;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Object clone() throws CloneNotSupportedException {
        PriorityQueue<E> clone = (PriorityQueue<E>) super.clone();
        clone.array = new Object[size + 1];
        System.arraycopy(array, 0, clone.array, 0, size + 1);
        return clone;
    }
}
