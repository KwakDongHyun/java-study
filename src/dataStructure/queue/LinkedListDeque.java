package dataStructure.queue;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. offer 계열 메소드 구현
 * 3. poll 계열 메소드 구현
 * 4. peek 계열 메소드 구현
 * 5. size, isEmpty, contains, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray, sort 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 * -- 메모 --
 * 1. 덱(Double-Ended Queue)는 내부적으로 Doubly LinkedList를 사용하여 구현하겠다.
 * 2. 큐는 Singly LinkedList와 유사한 메커니즘이지만, 덱은 Doubly LinkedList와 유사한 메커니즘이다.
 * 3. 실제 자바에서는 Queue Interface를 구현하는 것은 ArrayDeque, LinkedList, PriorityQueue 3가지가 있다.
 */
public class LinkedListDeque<E> implements Queue<E>, Cloneable {

    private Node<E> head;
    private Node<E> tail;
    private int size;

    public LinkedListDeque() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public boolean offerFirst(E value) {
        Node<E> newNode = new Node<E>(value);
        newNode.next = head;

        if(head != null) {
            head.prev = newNode;
        }
        head = newNode;
        size++;

        if(head.next == null) {
            tail = head;
        }
        return true;
    }

    @Override
    public boolean offer(E e) {
        return offerLast(e);
    }

    public boolean offerLast(E value) {
        if(size == 0) {
            return offerFirst(value);
        }

        Node<E> newNode = new Node<E>(value);
        tail.next = newNode;
        newNode.prev = tail;
        tail = newNode;
        size++;
        return true;
    }

    @Override
    public E poll() {
        return pollFirst();
    }

    public E pollFirst() {
        if(size == 0) {
            return null;
        }

        E element = head.data;
        Node<E> nextNode = head.next;
        head.data = null;
        head.next = null;

        if(nextNode != null) {
            nextNode.prev = null;
        }
        head = null;
        head = nextNode;
        size--;

        if(size == 0) {
            tail = null;
        }
        return element;
    }

    public E remove() {
        return removeFirst();
    }

    public E removeFirst() {
        E element = poll();
        if(element == null) {
            throw new NoSuchElementException();
        }
        return element;
    }

    public E pollLast() {
        if(size == 0) {
            return null;
        }

        E element = tail.data;
        Node<E> prevNode = tail.prev;
        tail.data = null;
        tail.prev = null;

        if(prevNode != null) {
            prevNode.next = null;
        }
        tail = null;
        tail = prevNode;
        size--;

        if(size == 0) {
            head = null;
        }
        return element;
    }

    public E removeLast() {
        E element = pollLast();
        if(element == null) {
            throw new NoSuchElementException();
        }
        return element;
    }

    @Override
    public E peek() {
        return peekFirst();
    }

    public E peekFirst() {
        if(size == 0) {
            return null;
        }
        return head.data;
    }

    public E peekLast() {
        if(size == 0) {
            return null;
        }
        return tail.data;
    }

    public E element() {
        return getFirst();
    }

    public E getFirst() {
        E element = peek();
        if(element == null) {
            throw new NoSuchElementException();
        }
        return element;
    }

    public E getLast() {
        E element = peekLast();
        if(element == null) {
            throw new NoSuchElementException();
        }
        return element;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(Object value) {
        for(Node<E> node = head; node != null; node = node.next) {
            if(value.equals(node.data)) {
                return true;
            }
        }

        return false;
    }

    public void clear() {
        for(Node<E> node = head; node != null;) {
            Node<E> next = node.next;

            node.data = null;
            node.next = null;
            node.prev = null;
            node = next;
        }
        size = 0;
        head = null;
        tail = null;
    }

    public Object[] toArray() {
        Object[] array = new Object[size];
        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            array[idx++] = node.data;
        }
        return array;
    }

    @SuppressWarnings({"unchecked"})
    public <T> T[] toArray(T[] a) {
        if(a.length < size) {
            a = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
        }

        int idx = 0;
        Object[] result = a;
        for(Node<E> node = head; node != null; node = node.next) {
            result[idx++] = node.data;
        }
        return a;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Object clone() throws CloneNotSupportedException {
        LinkedListDeque<E> clone = (LinkedListDeque<E>) super.clone();
        clone.head = null;
        clone.tail = null;
        clone.size = 0;

        for(Node<E> node = head; node != null; node = node.next) {
            clone.offerLast(node.data);
        }
        return clone;
    }

    public void sort() {
        sort(null);
    }

    @SuppressWarnings({"unchecked"})
    public void sort(Comparator<? super E> c) {
        Object[] a = this.toArray();
        Arrays.sort((E[]) a, c);

        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next, idx++) {
            node.data = (E) a[idx];
        }
    }

}
