package dataStructure.queue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. resize 메소드 구현
 * 3. add 메소드 구현
 * 4. get, set, indexOf, contains 메소드 구현
 * 5. remove 메소드 구현
 * 6. size, isEmpty, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 * -- 메모 --
 * 1. 큐는 내부적으로 배열을 사용하여 Circular Queue(원형 큐 또는 환형 큐)의 원리로 구현하도록 하겠다.
 * 2. 실제 자바에서는 Queue Interface를 구현하는 것은 ArrayDeque, LinkedList, PriorityQueue 3가지가 있다.
 */
public class ArrayQueue<E> implements Queue<E>, Cloneable {

    private static final int DEFAULT_CAPACITY = 64; // 기본 용적 크기. 최소 할당 용적

    private Object[] array; // 요소를 담을 배열 (실질적인 저장소)
    private int size; // 배열에 담긴 요소의 개수

    private int front; // 시작 인덱스를 가리키는 변수(빈 공간임을 유의). 첫번째 요소의 인덱스의 이전 인덱스를 가리킨다.
    private int rear; // 마지막 요소의 인덱스를 가리키는 변수

    // 기본 생성자. 초기 공간 할당이 없는 경우.
    public ArrayQueue() {
        this.array = new Object[DEFAULT_CAPACITY];
        this.size = 0;
        this.front = 0;
        this.rear = 0;
    }

    // 초기 공간 할당이 있는 경우.
    public ArrayQueue(int capacity) {
        this.array = new Object[capacity];
        this.size = 0;
        this.front = 0;
        this.rear = 0;
    }

    private void resize(int newCapacity) {
        int arrayCapacity = array.length;

        Object[] newArray = new Object[newCapacity];

        /*
         * i = new array index
         * j = original array index
         * 원본 배열의 값을 복사해서 새로운 배열의 인덱스 1부터 차례대로 넣는다
         */
        for(int i = 1, j = front + 1; i <= size; i++, j++) {
            newArray[i] = array[j % arrayCapacity];
        }

        this.array = null;
        this.array = newArray;
        this.front = 0;
        this.rear = size;

    }

    @Override
    public boolean offer(E e) {
        if((rear + 1) % array.length == front) {
            resize(array.length * 2);
        }
        // 용적 크기를 넘길 경우 원형 큐 특성상 index는 0부터 다시 가리켜야 한다.
        rear = (rear + 1) % array.length;
        array[rear] = e;
        size++;
        return true;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public E poll() {
        // 삭제할 요소가 없을 경우 null 반환. 실제 자바에서는 Queue interface에서 Exception이 아니라 null을 반환함.
        if(size == 0) {
            return null;
        }

        front = (front + 1) % array.length;
        E item = (E) array[front];
        array[front] = null;
        size--;

        /*
         * 요소의 개수가 용적의 1/4 미만일 경우, 최적화를 위해 용량을 현재 배열의 절반으로 줄인다.
         * 큐도 스택과 마찬가지로 일반적으로 중요한 곳에 많이 사용되므로 일정 크기 만큼은 필요한 경우가 많다고 생각한다.
         * 그렇기 때문에 최소한 DEFAULT SIZE만큼은 크기를 보장해주고자 한다.
         */
        if(size < (array.length / 4)) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }
        return item;
    }

    public E remove() {
        E item = poll();

        if(item == null) {
            throw new NoSuchElementException();
        }

        return item;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public E peek() {
        if(size == 0) {
            return null;
        }

        E item = (E) array[(front + 1) % array.length];
        return item;
    }

    public E element() {
        E item = peek();

        if(item == null) {
            throw new NoSuchElementException();
        }

        return item;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean contains(Object value) {
        for(int i = 0, idx = (front + 1) % array.length; i < size; i++, idx = (idx + 1) % array.length) {
            if(array[idx].equals(value)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        for(int i = 0; i < array.length; i++) {
            array[i] = null;
        }
        this.front = 0;
        this.rear = 0;
        this.size = 0;
    }

    public Object[] toArray() {
        return toArray(new Object[size]);
    }

    /**
     * 복잡해보이지만 사실 그렇게 복잡한 것은 없다. 인덱스 위치나 길이를 알아내는 과정이 헷갈려서 그렇다.
     * @param a
     * @param <T>
     * @return
     */
    @SuppressWarnings({"unchecked"})
    public <T> T[] toArray(T[] a) {
        T[] response = a;

        /*
         * parameter로 입력 받은 배열의 용적 크기가 큐의 요소의 개수보다 작기 때문에 새로 배열을 생성해줘야 한다.
         * 데이터를 전부 담을 수 없기 때문이다.
         * 반면 parameter로 입력 받은 배열의 용적 크기가 큐의 요소의 개수보다 더 클 경우, 새로운 배열을 생성할 필요가 없다
         */
        if(a.length < size) {
            response = (T[]) Arrays.copyOfRange(array, 0, size, a.getClass());
        }

        // front가 rear보다 앞에 있는 경우 또는 요소가 없을 경우 (일반적인 상황)
        if(front <= rear) {
            System.arraycopy(array, front + 1, response, 0, size);
        } else {
            /*
             * front가  rear보다 뒤에 있을 경우 (사용하다 보면 front가 size에 가까운 인덱스를 가리키는 상황)
             * 큐의 데이터 기입순서 및 배열의 현재 데이터 자장 상황을 유지하기 위해서 뒤와 앞 각각을 구분하여 작업
             */
            int rearlength = (array.length - 1) - front; // 뒷 부분의 요소의 개수

            // front pointer 이후에 데이터가 있을 경우, 먼저 넣어줘야한다.
            if(rearlength > 0) {
                System.arraycopy(array, front + 1, response, 0, rearlength);
            }

            // front 데이터가 있던 없던 배열의 0번째 인덱스부터 rear까지 데이터를 모두 넣으면 된다.
            System.arraycopy(array, 0, response, rearlength, rear + 1);
        }

        return response;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ArrayQueue<E> clone = (ArrayQueue<E>) super.clone();
        clone.array = Arrays.copyOf(array, array.length);
        return clone;
    }

    public void sort() {
        sort(null);
    }

    @SuppressWarnings({"unchecked"})
    public void sort(Comparator<? super E> c) {
        // null 접근 방지를 위해 toArray로 요소만 있는 배열을 얻어서 사용한다.
        Object[] response = toArray();
        Arrays.sort((E[]) response, c);

        /*
         * 기존 데이터르 전부 삭제하고, 정렬된 데이터를 인덱스 0부터 다시 채워넣는다.
         */
        clear();
        System.arraycopy(response, 0, array, 1, response.length);
        this.rear = response.length;
        this.size = response.length;
    }

}
