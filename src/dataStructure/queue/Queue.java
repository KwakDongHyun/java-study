package dataStructure.queue;

/**
 * Java의 Queue Interface를 직접 구현해본다.
 *
 */
public interface Queue<E> {

    /**
     * 큐의 가장 마지막에 요소를 추가합니다.
     *
     * @param e 큐에 추가할 요소
     * @return 큐에 요소가 정상적으로 추가되었는지 여부를 반환
     */
    boolean offer(E e);

    /**
     * 큐의 첫번째 요소를 삭제하고 삭제한 요소를 반환합니다.
     *
     * @return 큐의 삭제된 요소를 반환
     */
    E poll();

    /**
     * 큐의 첫번째 요소를 반환합니다. poll과 다르게 삭제를 하지 않습니다.
     *
     * @return 큐의 첫번째 요소를 반환
     */
    E peek();

}
