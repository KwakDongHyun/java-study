package dataStructure.queue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. resize 메소드 구현
 * 3. offer 계열 메소드 구현
 * 4. poll 계열 메소드 구현
 * 5. peek 계열 메소드 구현
 * 6. size, isEmpty, contains, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray, sort 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 * -- 메모 --
 * 1. 덱(Double-Ended Queue)는 내부적으로 배열을 사용하여 Circular Queue(원형 큐 또는 환형 큐)의 원리로 구현하도록 하겠다.
 * 2. 큐는 Singly LinkedList와 유사한 메커니즘이지만, 덱은 Doubly LinkedList와 유사한 메커니즘이다.
 * 3. 실제 자바에서는 Queue Interface를 구현하는 것은 ArrayDeque, LinkedList, PriorityQueue 3가지가 있다.
 */
public class ArrayDeque<E> implements Queue<E>, Cloneable {

    private static final int DEFAULT_CAPACITY = 64; // 기본 용적 크기. 최소 할당 용적

    private Object[] array; // 요소를 담을 배열 (실질적인 저장소)
    private int size; // 배열에 담긴 요소의 개수

    private int front; // 시작 인덱스를 가리키는 변수(빈 공간임을 유의). 첫번째 요소의 인덱스의 이전 인덱스를 가리킨다.
    private int rear; // 마지막 요소의 인덱스를 가리키는 변수

    // 기본 생성자. 초기 용적 할당이 없는 경우
    public ArrayDeque() {
        this.array = new Object[DEFAULT_CAPACITY];
        this.size = 0;
        this.front = 0;
        this.rear = 0;
    }

    // 초기 용적 할당이 있는 경우
    public ArrayDeque(int capacity) {
        this.array = new Object[capacity];
        this.size = 0;
        this.front = 0;
        this.rear = 0;
    }

    private void resize(int newCapacity) {
        int arrayCapacity = array.length;
        Object[] newArray = new Object[newCapacity];

        /*
         * i = new array index
         * j = original array index
         * 원본 배열의 값을 복사해서 새로운 배열의 인덱스 1부터 차례대로 넣는다
         */
        for(int i = 1, j = front + 1; i <= size; i++, j++) {
            newArray[i] = array[j % arrayCapacity];
        }

        this.array = null;
        this.array = newArray;
        front = 0;
        rear = size;
    }

    @Override
    public boolean offer(E e) {
        return offerLast(e);
    }

    public boolean offerLast(E e) {
        if((rear + 1) % array.length == front) {
            resize(array.length * 2);
        }
        // 용적 크기를 넘길 경우 원형 큐 특성상 index는 0부터 다시 가리켜야 한다.
        rear = (rear + 1) % array.length;
        array[rear] = e;
        size++;
        return true;
    }

    public boolean offerFirst(E e) {
        // 0 다음에는 마지막 인덱스를 가리켜야 하는데 이를 위해 용적 크기를 더해준다.
        // 용적 크기가 8이라면 (-1 + 8)은 7이 되어서 마지막 인덱스를 가리킬 수 있다.
        if((front - 1 + array.length) % array.length == rear) {
            resize(array.length * 2);
        }

        array[front] = e;
        front = (front - 1 + array.length) % array.length;
        size++;
        return true;
    }

    @Override
    public E poll() {
        return pollFirst();
    }

    @SuppressWarnings({"unchecked"})
    public E pollFirst() {
        if(size == 0) {
            return null;
        }

        front = (front + 1) % array.length;
        E e = (E) array[front];
        array[front] = null;
        size--;

        /*
         * 요소의 개수가 용적의 1/4 미만일 경우, 최적화를 위해 용량을 현재 배열의 절반으로 줄인다.
         * 큐도 스택과 마찬가지로 일반적으로 중요한 곳에 많이 사용되므로 일정 크기 만큼은 필요한 경우가 많다고 생각한다.
         * 그렇기 때문에 최소한 DEFAULT SIZE만큼은 크기를 보장해주고자 한다.
         */
        if(size < (array.length / 4)) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }
        return e;
    }

    public E remove() {
        return removeFirst();
    }

    public E removeFirst() {
        E e = pollFirst();
        if(e == null) {
            throw new NoSuchElementException();
        }
        return e;
    }

    @SuppressWarnings({"unchecked"})
    public E pollLast() {
        if(size == 0) {
            return null;
        }

        E e = (E) array[rear];
        array[rear] = null;
        // offerFirst의 주석 참고. 같은 이유로 인해서 용적 크기를 더해준다.
        rear = (rear - 1 + array.length) % array.length;
        size--;

        if(size < array.length / 4) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }
        return e;
    }

    public E removeLast() {
        E e = pollLast();
        if(e == null) {
            throw new NoSuchElementException();
        }
        return e;
    }

    @Override
    public E peek() {
        return peekFirst();
    }

    @SuppressWarnings({"unchecked"})
    public E peekFirst() {
        if(size == 0) {
            return null;
        }

        E e = (E) array[(front + 1) % array.length];
        return e;
    }

    @SuppressWarnings({"unchecked"})
    public E peekLast() {
        if(size == 0) {
            return null;
        }

        E e = (E) array[rear];
        return e;
    }

    public E element() {
        return getFirst();
    }

    public E getFirst() {
        E e = peekFirst();
        if(e == null) {
            throw new NoSuchElementException();
        }
        return e;
    }

    public E getLast() {
        E e = peekLast();
        if(e == null) {
            throw new NoSuchElementException();
        }
        return e;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean contains(Object value) {
        int start = (front + 1) % array.length;
        for(int i = 0, idx = start; i < size; i++, idx = (idx + 1) % array.length) {
            if(value.equals(array[idx])) {
                return true;
            }
        }

        return false;
    }

    public void clear() {
        for(int i = 0; i < array.length; i++) {
            array[i] = null;
        }
        this.front = 0;
        this.rear = 0;
        this.size = 0;
    }

    public Object[] toArray() {
        return toArray(new Object[size]);
    }

    /**
     * 복잡해보이지만 사실 그렇게 복잡한 것은 없다. 인덱스 위치나 길이를 알아내는 과정이 헷갈려서 그렇다.
     * @param a
     * @param <T>
     * @return
     */
    @SuppressWarnings({"unchecked"})
    public <T> T[] toArray(T[] a) {
        T[] response = a;

        /*
         * parameter로 입력 받은 배열의 용적 크기가 큐의 요소의 개수보다 작기 때문에 새로 배열을 생성해줘야 한다.
         * 데이터를 전부 담을 수 없기 때문이다.
         * 반면 parameter로 입력 받은 배열의 용적 크기가 큐의 요소의 개수보다 더 클 경우, 새로운 배열을 생성할 필요가 없다
         */
        if(a.length < size) {
            response = (T[]) Arrays.copyOfRange(array, 0, size, a.getClass());
        }

        // front가 rear보다 앞에 있는 경우 또는 요소가 없을 경우 (일반적인 상황)
        if(front <= rear) {
            System.arraycopy(array, front + 1, response, 0, size);
        } else {
            /*
             * front가  rear보다 뒤에 있을 경우 (사용하다 보면 front가 size에 가까운 인덱스를 가리키는 상황)
             * 큐의 데이터 기입순서 및 배열의 현재 데이터 자장 상황을 유지하기 위해서 뒤와 앞 각각을 구분하여 작업
             */
            int rearLength = (array.length - 1) - front; // 뒷 부분의 요소의 개수

            if(rearLength > 0) {
                System.arraycopy(array, front + 1, response, 0, rearLength);
            }
            System.arraycopy(array, 0, response, rearLength, rear + 1);
        }

        return response;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Object clone() throws CloneNotSupportedException {
        ArrayDeque<E> clone = (ArrayDeque<E>) super.clone();
        clone.array = Arrays.copyOf(array, array.length);
        return clone;
    }

    public void sort() {
        sort(null);
    }

    @SuppressWarnings({"unchecked"})
    public void sort(Comparator<? super E> c) {
        Object[] response = toArray();
        Arrays.sort((E[]) response, c);
        clear();

        System.arraycopy(response, 0, array, 1, response.length);
        this.rear = response.length;
        this.size = response.length;
    }

}
