package dataStructure.queue;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. offer, poll, peek 메소드 구현
 * 3. size, isEmpty, contains, clear 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray, sort 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 * -- 메모 --
 * 1. 큐는 Singley LinkedList로 구현하겠다.
 * 2. 실제 자바에서는 Queue Interface를 구현하는 것은 ArrayDeque, LinkedList, PriorityQueue 3가지가 있다.
 */
public class LinkedListQueue<E> implements Queue<E>, Cloneable {

    private Node<E> head;
    private Node<E> tail;
    private int size;

    public LinkedListQueue() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    @Override
    public boolean offer(E value) {
        Node<E> newNode = new Node<E>(value);

        if(size == 0) {
            head = newNode;
        } else {
            tail.next = newNode;
        }

        tail = newNode;
        size++;
        return true;
    }

    @Override
    public E poll() {
        if(size == 0) {
            return null;
        }

        E element = head.data;
        Node<E> nextNode = head.next;
        head.data = null;
        head.next = null;
        head = nextNode;
        size--;

        return element;
    }

    public E remove() {
        E element = poll();
        if(element == null) {
            throw new NoSuchElementException();
        }
        return element;
    }

    @Override
    public E peek() {
        if(size == 0) {
            return null;
        }
        return head.data;
    }

    public E element() {
        E element = peek();
        if(element == null) {
            throw new NoSuchElementException();
        }
        return element;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean contains(Object value) {
        for(Node<E> node = head; node != null; node = node.next) {
            if(value.equals(node.data)) {
                return true;
            }
        }

        return false;
    }

    public void clear() {
        for(Node<E> node = head; node != null;) {
            Node<E> nextNode = node.next;
            node.data = null;
            node.next = null;
            node = nextNode;
        }

        this.size = 0;
        head = null;
        tail = null;
    }

    public Object[] toArray() {
        Object[] array = new Object[size];
        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next) {
            array[idx++] = node.data;
        }
        return array;
    }

    @SuppressWarnings({"unchecked"})
    public <T> T[] toArray(T[] a) {
        if(a.length < size) {
            a = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
        }

        int idx = 0;
        Object[] result = a;
        for(Node<E> node = head; node != null; node = node.next) {
            result[idx++] = node.data;
        }
        return a;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Object clone() throws CloneNotSupportedException {
        LinkedListQueue<E> clone = (LinkedListQueue<E>) super.clone();
        clone.head = null;
        clone.tail = null;
        clone.size = 0;

        for(Node<E> node = head; node != null; node = node.next) {
            clone.offer(node.data);
        }

        return clone;
    }

    public void sort() {
        sort(null);
    }

    @SuppressWarnings({"unchecked"})
    public void sort(Comparator<? super E> c) {
        Object[] a = this.toArray();
        Arrays.sort((E[]) a, c);

        int idx = 0;
        for(Node<E> node = head; node != null; node = node.next, idx++) {
            node.data = (E) a[idx];
        }
    }

}
