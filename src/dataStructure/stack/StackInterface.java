package dataStructure.stack;

/**
 * Java의 Stack Interface를 직접 구현해본다.
 *
 */
public interface StackInterface<E> {

    /**
     * 스택의 맨 위에 요소를 추가
     *
     * @param item 스택에 추가할 요소
     * @return 스택에 추가된 요소
     */
    E push(E item);

    /**
     * 스택의 맨 위에 있는 요소를 제거하고 반환
     *
     * @return 제거된 요소
     */
    E pop();

    /**
     * pop과 다르게 맨 위에 있는 요소를 제거하지 않고 반환
     *
     * @return 스택의 맨 위에 있는 요소
     */
    E peek();

    /**
     * 스택의 최상단부터 특정 요소가 상대적으로 몇 번째 위치에 있는지를 반환
     * 중복되는 요소가 있을 경우 최상단에 가장 가까운 위치를 반환 (먼저 찾은 위치를 반환)
     *
     * @param value 스택에서 위치를 찾을 요소
     * @return 스택의 상단부터 처음으로 요소와 일치하는 위치를 반환.
     *         만약 일치하는 요소가 없을 경우 -1을 반환
     *
     */
    int search(Object value);

    /**
     * 스택의 요소 개수를 반환
     *
     * @return
     */
    int size();

    /**
     * 스택에 있는 모든 요소를 삭제
     */
    void clear();

    /**
     * 스택에 요소가 비어있는지 여부부를 반환
     *
     * @return
     */
    boolean empty();

}
