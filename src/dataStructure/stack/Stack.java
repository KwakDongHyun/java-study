package dataStructure.stack;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EmptyStackException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자 구성
 * 2. resize 메소드 구현
 * 3. push 메소드 구현
 * 4. pop 메소드 구현
 * 5. peek 메소드 구현
 * 6. search, size, clear, empty 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray 구현
 *
 * -- 필수 구현 조건 --
 * 1. 모든 자료구조는 '동적 할당'을 전제로 한다.
 *    리스트가 가득 찼다고 해서 데이터를 받지 않는 것이 아니라, 크기를 동적으로 늘려서 데이터를 계속해서 수용한다.
 *
 */
public class Stack<E> implements StackInterface<E>, Cloneable {

    private static final int DEFAULT_CAPACITY = 10; // 기본 용적 크기. 최소 할당 용적
    private static final Object[] EMPTY_ARRAY = {}; // 기본 빈 배열. 용적이 0인 배열

    private Object[] array; // 요소를 담을 배열 (실질적인 저장소)
    private int size; // 배열에 담긴 요소의 개수.

    // 기본 생성자. 초기 공간 할당이 없는 경우
    public Stack() {
        this.array = EMPTY_ARRAY;
        this.size = 0;
    }

    // 초기 공간 할당이 있는 경우
    public Stack(int capacity) {
        this.array = new Object[capacity];
        this.size = 0;
    }

    @Override
    public E push(E item) {
        // 용적이 가득 채워졌을 경우 용적을 재할당한다.
        if(size == array.length) {
            resize();
        }

        array[size] = item;
        size++;
        return item;
    }

    /**
     * E 타입의 데이터만을 수용하므로 ClassCastException이 발생할 가능성은 없다.
     * 그러므로 형 변환에서 발생하는 타입 안정성 관련 예외에 대한 경고를 무시하게 만든다.
     *
     * @return
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public E pop() {
        // 삭제할 요소가 없다면 스택이 비어있다는 예외를 발생시킨다.
        if(size == 0) {
            throw new EmptyStackException();
        }

        E element = (E) array[size - 1];
        array[size - 1] = null;
        size--;
        resize();

        return element;
    }

    /**
     * E 타입의 데이터만을 수용하므로 ClassCastException이 발생할 가능성은 없다.
     * 그러므로 형 변환에서 발생하는 타입 안정성 관련 예외에 대한 경고를 무시하게 만든다.
     *
     * @return
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public E peek() {
        // 삭제할 요소가 없다면 스택이 비어있다는 예외를 발생시킨다.
        if(size == 0) {
            throw new EmptyStackException();
        }

        return (E) array[size - 1];
    }

    @Override
    public int search(Object value) {
        // null도 비교할 수 있도록 만든다
        if(value == null) {
            for(int idx = size - 1; idx > -1; idx--) {
                if(array[idx] == null) {
                    return size - idx;
                }
            }
        } else {
            for(int idx = size - 1; idx > -1; idx--) {
                if(array[idx].equals(value)) {
                    return size - idx;
                }
            }
        }
        // 스택 모든 영역을 탐색했지만 데이터를 찾지 못한 경우
        return -1;
    }

    @Override
    public int size() {
        return this.size;
    }

    /**
     * 데이터에 따라서 최적화 된 용적이 필요할 경우 사용
     * 데이터 손상을 방지하기 위해서 private으로 접근 제어자를 설정
     */
    private void resize() {
        int arrayCapacity = array.length;

        /*
         * 배열의 용적이 0과 마찬가지일 경우.
         * 또한 주소가 아닌 값을 비교해야 하기 때문에 Arrays.equals() 메소드를 사용하도록 하자.
         */
        if(Arrays.equals(array, EMPTY_ARRAY)) {
            array = new Object[DEFAULT_CAPACITY];
            return;
        }

        /*
         * 요소의 개수가 용적만큼 가득 채워져 있다면 2배만큼 용량을 늘린다.
         * 만약 복사할 배열보다 용적의 크기가 클 경우 먼저 배열을 복사한 뒤, 나머지 빈 공간은 null로 채워지기 때문에 편리하다.
         */
        if(size == arrayCapacity) {
            int newCapacity = arrayCapacity * 2;
            array = Arrays.copyOf(array, newCapacity);
            return;
        }

        /*
         * 요소의 개수가 용적의 절반 미만일 경우, 최적화를 위해 용량을 현재 배열의 절반으로 줄인다.
         * 만약 복사할 배열보다 용적의 크기가 작을경우 새로운 용적까지만 복사하고 반환하기 때문에 예외발생에 대해 안전하다.
         * 한편, 스택은 일반적으로 중요한 곳에 많이 사용되므로 일정 크기 만큼은 필요한 경우가 많다고 생각한다.
         * 그렇기 때문에 최소한 DEFAULT SIZE만큼은 크기를 보장해주고자 한다.
         */
        if(size < (arrayCapacity / 2)) {
            int newCapacity = arrayCapacity / 2;
            array = Arrays.copyOf(array, Math.max(newCapacity, DEFAULT_CAPACITY));
            return;
        }
    }

    @Override
    public void clear() {
        for(int idx = 0; idx < size; idx++) {
            array[idx] = null;
        }
        size = 0;
        resize();
    }

    @Override
    public boolean empty() {
        return this.size == 0;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Stack<E> clone = (Stack<E>) super.clone();
        // 내부의 참조값들은 Shallow Copy가 되므로 새로 생성해줘야 한다.
        clone.array = new Object[size];
        // 현재 배열을 새로운 스택의 배열에 값을 복사
        System.arraycopy(array, 0, clone.array, 0, size);
        return clone;
    }

    // Array로 이루어져 있기 때문에, 배열을 복사해서 반환해주면 된다. (새로운 참조변수를 줘야하기 때문)
    public Object[] toArray() {
        return Arrays.copyOf(array, size);
    }

    @SuppressWarnings({"unchecked"})
    public <T> T[] toArray(T[] a) {
        if(a.length < size) {
            return (T[]) Arrays.copyOf(array, size, a.getClass());
        }

        System.arraycopy(array, 0, a, 0, size);
        return a;
    }

    public void sort() {
        sort(null);
    }

    @SuppressWarnings({"unchecked"})
    public void sort(Comparator<? super E> c) {
        Arrays.sort((E[]) array, 0, size, c);
    }

}
