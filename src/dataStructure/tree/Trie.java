package dataStructure.tree;

/**
 *  Trie 자료구조
 *  트리 자료구조의 일종으로 prefix tree, radix tree, retrieval tree 라고도 한다.
 *  이는 우리가 사전의 구조와 사전을 통해서 문자열을 찾는 메커니즘을 생각하면 이해가 쉽다.
 *
 *  - 기본 개념 -
 *  1. 검색어 자동완성, 사전 검색 등 문자열을 탐색하는데 특화되어 있다.
 *  2. 트리의 레벨에 따라서 문자열의 문자 1개씩 저장되어 있고, 단어의 끝인지를 알려주는 boolean field variable를 통해서
 *     여태까지 탐색해온 문자에 더해서 해당 문자를 끝으로 하는 단어가 존재하는지 알려주는 컨셉으로 되어있다.
 *  3. 우리가 사전의 구조와 사전을 통해서 문자열을 찾는 메커니즘을 생각하면 이해가 쉽다.
 *  4. 트리의 시간 복잡도는 생성과 탐색 모두 시간 복잡도는 O(L)이다.
 *     제일 긴 문자열의 길이를 L이라 가정하면, 문자열의 문자마다 하나씩 트리에서 비교하면서 탐색하고 저장하기 때문에 O(L)만큼 걸린다.
 *  5. 메모리 사용량이 많은 것이 단점이다. O(포인터 크기 * 포인터 배열 개수 * 총 노드의 개수).
 *     포인터 크기 = 저장할 데이터 타입에 따른 용량
 *     포인터의 개수 = 노드마다 저장하고자 하는 문자들의 집합 크기. 예를 들어 알파벳만 저장한다고 하면 집합 크기는 26이 된다.
 *     총 노드의 개수 = 문자열 집합의 수. 저장하고자 하는 데이터의 총 개수라고 생각하면 된다.
 *
 *  - 동작 과정 -
 *  1. 사전을 통해서 문자열을 찾는 메커니즘과 동일하다.
 *  2. 루트는 아무런 데이터가 저장되어 있지는 않지만, 루트부터 탐색을 시작한다.
 *  3. 검색 대상의 문자마다 자식노드의 집합에 존재하는지 트리를 타면서 계속 검사한다.
 *     만약 해당 문자에 대해서 존재하지 않으면 노드를 새로 생성해서 저장하고 탐색을 이어서 진행한다.
 *  4. 검색 대상의 마지막 문자까지 탐색에 도달할 경우, 문자의 끝을 알리는 boolean 값을 true로 바꾸고 저장한다.
 *     이는 해당 트리의 위치에 단어가 존재한다는 것을 의미한다.
 *
 */
public class Trie implements Cloneable {

    private TrieNode root;

    public Trie() {
        this.root = new TrieNode();
    }

    private TrieNode search(String str) {
        /*
         * root부터 탐색을 시작하여 각 문자마다 검색하면서 자식 노드가 존재하는지 체크
         * 자식노드가 존재하지 않는 없으면 null을 반환하고 탐색 종료
         * Map Interface의 getOrDefault method 참고
         */
        TrieNode trieNode = this.root;
        char[] chars = str.toCharArray();

        for(int i = 0; i < chars.length; i++) {
            trieNode = trieNode.children.getOrDefault(chars[i], null);
        }

        return trieNode;
    }

    /**
     * 트리에 문자열을 추가합니다.
     *
     * @param str 트리에 추가할 문자열
     * @return 트리에 이미 중복되는 문자열이 저장되어 있을 경우 {@code false}를 반환하고,
     *         문자열 저장에 성공했을 경우 {@code true}를 반환
     */
    public boolean insert(String str) {
        /*
         * root부터 탐색을 시작하여 각 문자마다 검색하면서 자식 노드가 존재하는지 체크
         * 자식노드가 존재하지 않는 없으면 자식 노드를 새로 생성하면서 마지막 문자까지 탐색을 진행
         * Map Interface의 computeIfAbsent method 참고
         */
        TrieNode trieNode = this.root;
        char[] chars = str.toCharArray();

        for(int i = 0; i < chars.length; i++) {
            trieNode = trieNode.children.computeIfAbsent(chars[i], key -> new TrieNode());
        }

        // 마지막 단어에 매핑되는 노드에 단어의 끝임을 명시하기 위해 flag값을 true로 저장
        if(trieNode.endOfWord) {
            return false;
        }

        trieNode.endOfWord = true;
        return true;
    }

    /**
     * 트리에 저장된 문자열을 삭제합니다.
     *
     * @param str 트리에서 삭제할 문자열
     * @return 트리에 삭제할 문자열이 없거나 삭제에 실패할 경우 {@code false}를 반환하고,
     *         문자열 삭제에 성공했을 경우 {@code true}를 반환
     */
    public boolean remove(String str) {
        TrieNode trieNode = search(str);

        if(trieNode == null || !trieNode.endOfWord) {
            return false;
        }

        trieNode.endOfWord = false;
        return true;
    }

    /**
     * 트리에 특정 문자열이 저장되어 있는지 여부를 확인합니다.
     *
     * @param str 트리에서 찾을 특정 문자열
     * @return 트리에 특정 문자열이 존재할 경우 {@code true}를 반환하고,
     *         문자열이 존재하지 않을 경우 {@code false}를 반환
     */
    public boolean contains(String str) {
        /*
         * 마지막 문자까지 탐색을 성공했더라도 정작 해당 단어가 존재한다는 의미의 boolean flag값이 저장되어 있지 않을 가능성이 있다.
         * 예를 들어 extra와 extraordinary 두 단어가 있고 extraordinary만 저장되어 있는 상황이라고 가정하자.
         * extraordinary가 저장되어 있기 때문에 extra의 마지막 문자인 a에 해당하는 자식 노드가 저장되어 있을 것이다.
         * 그러나 해당 단어를 저장한 적 없기 때문에 boolean flag값은 false로 유지되어 있다.
         * 이에 대해서는 검색의 실패로 처리해줘야 한다.
         */
        TrieNode trieNode = search(str);
        return trieNode == null ? false : trieNode.endOfWord;
    }

    public boolean isEmpty() {
        return this.root.children.isEmpty();
    }


}
