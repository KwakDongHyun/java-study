package dataStructure.tree;

/*
 * Trie 개념을 설명하기 위해서 간단하게 구현할 때는 주로 Array를 이용한다.
 * 하지만 배열을 사용할 경우 Trie 구조상 필연적으로 방대한 메모리를 사용할 수 밖에 없다.
 * 이 단점을 보완하기 위해서 실제로는 동적 할당 방식으로 구현되어 있다.
 *
 * 동적 할당 + 빠른 데이터 검색을 위해서 Map Interface를 사용해서 구현한다.
 * Hash Code를 이용하는 Hash Map를 사용할 것이다.
 */
import java.util.HashMap;
import java.util.Map;

class TrieNode {

    // 자식 노드는 동적 할당을 위해서 Map으로 구현한다.
    Map<Character, TrieNode> children = new HashMap<Character, TrieNode>();
    // 단어의 끝인지 확인하기 위한 flag. true일 경우 해당 문자를 끝으로 하는 조합 단어가 존재함.
    boolean endOfWord;

}
