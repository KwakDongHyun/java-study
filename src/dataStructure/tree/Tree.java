package dataStructure.tree;

import java.util.List;

/**
 * Tree Interface를 직접 구현해본다.
 */
public interface Tree<E> {

    /**
     * 트리에 요소를 추가합니다.
     *
     * @param value 트리에 추가할 요소
     * @return 트리에 이미 중복되는 요소가 저장되어 있을 경우 {@code false}를 반환하고,
     *         원소가 없을 경우 {@code true}를 반환
     */
    boolean insert(E value);

    /**
     * 트리에서 특정 요소를 삭제합니다.
     * 동일한 요소가 여러 개일 경우 가장 처음 발견한 요소만 삭제됩니다.
     *
     * @param value 트리에서 삭제할 요소
     * @return 트리에 삭제할 요소가 없거나 삭제에 실패할 경우 {@code false}를 반환하고,
     *         삭제에 성공할 경우 {@code true}를 반환
     */
    boolean remove(Object value);

    /**
     * 트리에 특정 요소가 있는지 여부를 확인합니다.
     *
     * @param value 트리에서 찾을 특정 요소
     * @return 트리에 특정 요소가 존재할 경우 {@code true}를 반환하고,
     *         존재하지 않을 경우 {@code false}를 반환
     */
    boolean contains(Object value);

    /**
     * 트리에 요소가 비어있는지를 반환합니다.
     *
     * @return 트리에 요소가 없을 경우 {@code true}를 반환하고, 요소가 없을 경우 {@code false}를 반환
     */
    boolean isEmpty();


}
