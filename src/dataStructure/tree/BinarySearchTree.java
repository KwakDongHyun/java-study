package dataStructure.tree;

import java.util.Comparator;

/**
 * 기본적으로 중복을 허용하지 않는 방향으로 설계한다.
 * 중복을 허용하는 경우, 왼쪽 자식노드 혹은 오른쪽 자식노드와 비교할 때 오직 한 자식노드에 대해서만
 * 같은 경우를 추가로 고려해서 처리하면 되긴 하다. (그러나 최대한 기초 학습을 위해서 이번에는 그렇게 하진 않겠다)
 *
 * 추가 구현 목록
 * 1. PreOrder, InOrder, PostOrder
 *
 * @param <E>
 */
public class BinarySearchTree<E> {

    private Node<E> root;
    private int size;

    private final Comparator<? super E> comparator;

    public BinarySearchTree() {
        this(null);
    }

    public BinarySearchTree(Comparator<? super E> comparator) {
        this.comparator = comparator;
        this.root = null;
        this.size = 0;
    }

    /**
     *
     * @param value
     * @return 정상적으로 삽입되었을 경우 true, 중복 원소를 삽입했을 경우 false
     */
    public boolean add(E value) {
        if(this.comparator == null) {
            return addUsingComparable(value) == null;
        } else {
            return addUsingComparator(value) == null;
        }
    }

    @SuppressWarnings({"unchecked"})
    private E addUsingComparable(E value) {
        Node<E> currentNode = root;

        if(currentNode == null) {
            root = new Node<E>(value);
            size++;
            return null;
        }

        Node<E> currentParentNode;
        Comparable<? super E> comparable = (Comparable<? super E>) value;
        int compareResult;

        do {
            currentParentNode = currentNode;
            compareResult = comparable.compareTo(currentNode.value);

            if(compareResult < 0) {
                currentNode = currentNode.left;
            } else if(compareResult > 0) {
                currentNode = currentNode.right;
            } else {
                return value;
            }
        } while(currentNode != null);

        Node<E> newNode = new Node<E>(value, currentParentNode);
        if(compareResult < 0) {
            currentParentNode.left = newNode;
        } else {
            currentParentNode.right = newNode;
        }

        size++;
        return null;
    }

    private E addUsingComparator(E value) {
        Node<E> currentNode = root;

        if(currentNode == null) {
            root = new Node<E>(value);
            size++;
            return null;
        }

        Node<E> currentParentNode;
        int compareResult;

        do {
            currentParentNode = currentNode;
            compareResult = this.comparator.compare(value, currentNode.value);

            if(compareResult < 0) {
                currentNode = currentNode.left;
            } else if(compareResult > 0) {
                currentNode = currentNode.right;
            } else {
                return value;
            }
        } while(currentNode != null);

        Node<E> newNode = new Node<E>(value, currentParentNode);
        if(compareResult < 0) {
            currentParentNode.left = newNode;
        } else {
            currentParentNode.right = newNode;
        }

        size++;
        return null;
    }

    /**
     * 삭제하는 노드의 자리를 대체할 노드(후계자)를 찾는 메소드
     * 두 가지 방법 중 택한 것은 오른쪽 자식 노드 중 가작 작은 노드를 탐색하는 것
     * 대체할 노드를 찾아서 반환하고, 링크를 정리하는 역항만을 한다.
     *
     * @param node 삭제되는 노드
     * @return 대체할 노드
     */
    private Node<E> getSuccessorAndUnlink(Node<E> node) {
        Node<E> currentParentNode = node;
        Node<E> currentNode = node.right;

        /*
         * 첫번째 탐색 과정에서 오른쪽 자식노드의 서브트리에서 왼쪽 자식 서브트리가 없을 경우이다.
         * 즉 삭제되는 노드의 오른쪽 자식노드가 삭제되는 노드와 가장 근접한 값일 경우를 말한다.
         */
        if(currentNode.left == null) {
            // 오른쪽 자식노드의 서브트리를 그대로 이어주면 된다.
            currentParentNode.right = currentNode.right;
            if(currentParentNode.right != null) {
                // 위와 반대로 서브트리의 부모노드를 설정해주는 과정이다.
                currentParentNode.right.parent = currentParentNode;
            }
            currentNode.right = null; // for gc
            return currentNode;
        }

        while(currentNode.left != null) {
            currentParentNode = currentNode;
            currentNode = currentNode.left;
        }

        /*
         * 오른쪽 자식노드의 서브트리 중에서 가장 왼쪽에 있는 값(삭제되는 값과 가장 가까운 값)을 찾았다면
         * 그 대체 노드의 오른쪽 자식노드가 존재하는 경우가 있을 수 있다.
         * 그걸 해결하는 로직이다.
         */
        currentParentNode.left = currentNode.right;
        if(currentParentNode.left != null) {
            currentParentNode.left.parent = currentParentNode;
        }
        currentNode.right = null;
        return currentNode;
    }

    /**
     * 대체노드를 찾는 경우에는 대체노드의 값을 삭제할 노드의 값에 덮어씌운다.
     * 대체노드를 찾지 않는 경우에는 링크 정리만을 하고, 다음에 연결되는 노드를 반환한다.
     * 결국 삭제하면서 새롭게 들어온 노드를 반환하기는 하지만
     * 대체노드의 경우 참조값은 삭제할 노드 참조값을 반환하는 것과 같다.
     *
     * @param node 삭제할 노드
     * @return 삭제 후 대체하고 난 뒤, 해당 위치의 노드를 반환.
     */
    private Node<E> deleteNode(Node<E> node) {
        /*
         * parameter로 입력받은 삭제할 노드가 없을 경우
         */
        if(node == null) {
            return null;
        }

        /*
         * 삭제의 경우 첫 번째. 자식노드가 없을 경우
         * Leaf Node 또는 노드가 단 1개인 경우(즉 root 노드만 있는 경우)
         */
        if(node.left == null && node.right == null) {
            if(node == root) {
                root = null;
            } else {
                node = null;
            }
            return null;
        }

        /*
         * 삭제의 경우 두 번째. 자식노드가 모두 있을 경우
         *           세 번째. 자식노드가 어느 한 쪽만 있을 경우
         */
        if(node.left != null && node.right != null) {
            Node<E> successorNode = getSuccessorAndUnlink(node);
            node.value = successorNode.value;

        } else if(node.left != null) {
            if(node == root) {
                node = node.left;
                root = node;
                root.parent = null;
            } else {
                node = node.left;
            }

        } else {
            if(node == root) {
                node = node.right;
                root = node;
                root.parent = null;
            } else {
                node = node.right;
            }
        }

        return node;
    }

    /**
     * 삭제 메서드
     * @param o 삭제할 값
     * @return 삭제된 노드의 value 값 혹은 매칭되는 값이 없을 경우 null을 반환.
     */
    public E remove(Object o) {
        if(root == null) {
            return null;
        }

        if(this.comparator == null) {
            return removeUsingComparable(o);
        } else {
            return removeUsingComparator(o);
        }
    }

    @SuppressWarnings({"unchecked"})
    private E removeUsingComparable(Object value) {
        if(root == null) {
            return null;
        }

        E oldValue = (E) value;
        Node<E> parentNode = null;
        Node<E> currentNode = root;
        // 삭제하고자 하는 노드가 부모 노드로부터 왼쪽 혹은 오른쪽 자식 노드인지 구분하기 위한 변수
        boolean hasLeft = false;
        Comparable<? super E> comparable = (Comparable<? super E>) value;

        while(currentNode != null) {
            int compareResult = comparable.compareTo(currentNode.value);
            // 삭제할 노드를 찾았을 경우
            if(compareResult == 0) {
                break;
            }

            parentNode = currentNode;
            if(compareResult < 0) {
                hasLeft = true;
                currentNode = currentNode.left;
            } else {
                hasLeft = false;
                currentNode = currentNode.right;
            }
        }

        /*
         * 탐색을 했지만 삭제할 노드를 찾지 못했을 경우 null 반환
         */
        if(currentNode == null) {
            return null;
        }

        /*
         * 삭제할 노드가 root일 경우
         */
        if(parentNode == null) {
            deleteNode(currentNode); // root일 경우 null을 반환함.
            size--;
            return oldValue;
        }

        /*
         * 삭제하는 경우에서 대체 노드(계승자)를 찾는 경우에는 사실 필요 없는 작업이 맞지만
         * 그 외의 경우에는 삭제할 노드와 관계있는 노드들의 참조변수(포인터)를 변경해줘야 하기 때문에 필요하다.
         */
        if(hasLeft) {
            parentNode.left = deleteNode(currentNode);
            if(parentNode.left != null) {
                parentNode.left.parent = parentNode;
            }
        } else {
            parentNode.right = deleteNode(currentNode);
            if(parentNode.right != null) {
                parentNode.right.parent = parentNode;
            }
        }

        size--;
        return oldValue;
    }

    @SuppressWarnings({"unchecked"})
    private E removeUsingComparator(Object value) {
        if(root == null) {
            return null;
        }

        E oldValue = (E) value;
        Node<E> parentNode = null;
        Node<E> currentNode = root;
        // 삭제하고자 하는 노드가 부모 노드로부터 왼쪽 혹은 오른쪽 자식 노드인지 구분하기 위한 변수
        boolean hasLeft = false;

        while(currentNode != null) {
            int compareResult = this.comparator.compare(oldValue, currentNode.value);
            // 삭제할 노드를 찾았을 경우
            if(compareResult == 0) {
                break;
            }

            parentNode = currentNode;
            if(compareResult < 0) {
                hasLeft = true;
                currentNode = currentNode.left;
            } else {
                hasLeft = false;
                currentNode = currentNode.right;
            }
        }

        /*
         * 탐색을 했지만 삭제할 노드를 찾지 못했을 경우 null 반환
         */
        if(currentNode == null) {
            return null;
        }

        /*
         * 삭제할 노드가 root일 경우
         */
        if(parentNode == null) {
            deleteNode(currentNode); // root일 경우 null을 반환함.
            size--;
            return oldValue;
        }

        /*
         * 삭제하는 경우에서 대체 노드(계승자)를 찾는 경우에는 사실 필요 없는 작업이 맞지만
         * 그 외의 경우에는 삭제할 노드와 관계있는 노드들의 참조변수(포인터)를 변경해줘야 하기 때문에 필요하다.
         */
        if(hasLeft) {
            parentNode.left = deleteNode(currentNode);
            if(parentNode.left != null) {
                parentNode.left.parent = parentNode;
            }
        } else {
            parentNode.right = deleteNode(currentNode);
            if(parentNode.right != null) {
                parentNode.right.parent = parentNode;
            }
        }

        size--;
        return oldValue;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean contains(Object o) {
        if(this.comparator == null) {
            return containsUsingComparable(o);
        } else {
            return containsUsingComparator(o);
        }
    }

    @SuppressWarnings({"unchecked"})
    private boolean containsUsingComparable(Object o) {
        Comparable<? super E> value = (Comparable<? super E>) o;

        Node<E> node = root;
        while(node != null) {
            int compareResult = value.compareTo(node.value);
            if(compareResult < 0) {
                node = node.left;
            } else if(compareResult > 0) {
                node = node.right;
            } else {
                return true;
            }
        }

        return false;
    }

    @SuppressWarnings({"unchecked"})
    private boolean containsUsingComparator(Object o) {
        E value = (E) o;
        Node<E> node = root;
        while(node != null) {
            int compareResult = this.comparator.compare(value, node.value);
            if(compareResult < 0) {
                node = node.left;
            } else if(compareResult > 0) {
                node = node.right;
            } else {
                return true;
            }
        }

        return false;
    }

    public void clear() {
        size = 0;
        root = null;
    }

    public void preorder() {
        preorder(this.root);
    }

    public void preorder(Node<E> o) {
        if(o != null) {
            System.out.println(o.value + " "); // 부모노드
            preorder(o.left);
            preorder(o.right);
        }
    }

    public void inorder() {
        inorder(this.root);
    }

    public void inorder(Node<E> o) {
        if(o != null) {
            inorder(o.left);
            System.out.println(o.value + " "); // 부모노드
            inorder(o.right);
        }
    }

    public void postorder() {
        postorder(this.root);
    }

    public void postorder(Node<E> o) {
        if(o != null) {
            postorder(o.left);
            postorder(o.right);
            System.out.println(o.value + " "); // 부모노드
        }
    }

}

