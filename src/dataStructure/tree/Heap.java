package dataStructure.tree;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/*
 * -- 구현 필수 목록 --
 * 1. 클래스 및 생성자와 필수 메소드 구성
 * 2. resize 메소드 구현
 * 3. add 계열 메소드 구현
 * 4. remove 계열 메소드 구현
 * 6. size, peek, isEmpty, toArray 메소드 구현
 *
 * -- 추가 구현 --
 * 1. clone, toArray, sort 구현
 *
 * -- 메모 --
 * 1. '최솟값 또는 최댓값을 빠르게 찾아내기 위해 완전이진트리 형태로 만들어진 자료구조'다.
 * 2. 최소 힙과 최대 힙이 있다. (최소 힙은 오름차순, 최대 힙은 내림차순)
 * 3. 형제끼리는 우선 순위를 가리지 않기 때문에 '반 정렬 상태' or '느슨한 정렬 상태'라고 한다.
 *    다른 말로는 '약한 힙(weak heap)'이라고도 한다.
 * 4. 배열로 구현하면 고유 특성을 활용하기 좋기 때문에 배열을 기반으로 구현한다.
 *    여기서는 최소 힙을 기준으로 구현한다.
 */
public class Heap<E> {

    private static final int DEFAULT_CAPACITY = 16; // 기본 용적 크기. 최소 할당 용적

    private Comparator<? super E> comparator; // sort를 위한 Comparator 참조변수
    private Object[] array; // 요소를 담을 배열 (실질적인 저장소)
    private int size; // 배열에 담긴 요소의 개수

    // 기본 생성자. parameter가 없는 경우
    public Heap() {
        this(null);
    }

    // Comparator만 있는 경우
    public Heap(Comparator<? super E> comparator) {
        this.array = new Object[DEFAULT_CAPACITY];
        this.size = 0;
        this.comparator = comparator;
    }

    // 초기 용적 할당만 있는 경우
    public Heap(int capacity) {
        this(null, capacity);
    }

    // Comparator와 초기 용적 할당 모두를 parameter로 받는 경우
    public Heap(Comparator<? super E> comparator, int capacity) {
        this.array = new Object[capacity];
        this.size = 0;
        this.comparator = comparator;
    }

    private int getParent(int index) {
        return index / 2;
    }

    private int getLeftChild(int index) {
        return index * 2;
    }

    private int getRightChild(int index) {
        return index * 2 + 1;
    }

    private void resize(int newCapacity) {
        Object[] newArray = new Object[newCapacity];
        for(int i = 1; i <= size; i++) {
            newArray[i] = array[i];
        }
        this.array = null;
        this.array = newArray;
    }

    public void add(E value) {
        if(size + 1 == array.length) {
            resize(array.length * 2);
        }

        // 가장 마지막에 추가되는 새로운 값을 넣어준다. 그리고 나서 sift-up(상향 선별)을 진행하면서 재배치를 해준다.
        siftUp(size + 1, value);
        size++;
    }

    /**
     * 새로운 값을 Heap에 추가하면 반드시 해줘야 하는 작업이다.
     * 최소 Heap 또는 최대 Heap의 규칙대로 새로 추가한 value를 위치해야할 곳에 재배치시키는 작업이다.
     * @param index
     * @param target
     */
    private void siftUp(int index, E target) {
        // comparator가 별도로 존재한다면 parameter로 넣어준다.
        if(this.comparator != null) {
            siftUpComparator(index, target);
        } else {
            siftUpComparable(index, target);
        }
    }

    /**
     * Comparator를 이용한 sift-up
     * @param index
     * @param target
     */
    @SuppressWarnings({"unchecked"})
    private void siftUpComparator(int index, E target) {
        // root에 도달하기 전까지만 탐색한다.
        while(index > 1) {
            int parent = getParent(index);
            Object parentVal = array[parent];

            // 최소 힙을 구현하기 때문에 target >= parentVal 라면 재배열 과정을 끝낸다.
            if(this.comparator.compare(target, (E) parentVal) >= 0) {
                break;
            }

            array[index] = parentVal;
            index = parent;
        }

        array[index] = target;
    }

    /**
     * Comparable을 구현한 클래스는 이 메서드를 사용한 sift-up
     * @param index
     * @param target
     */
    @SuppressWarnings({"unchecked"})
    private void siftUpComparable(int index, E target) {
        // Generic이라서 사용하려면 interface로 casting 해줘야함
        Comparable<? super E> comparable = (Comparable<? super E>) target;

        // root에 도달하기 전까지만 탐색한다.
        while(index > 1) {
            int parent = getParent(index);
            Object parentVal = array[parent];

            // 최소 힙을 구현하기 때문에 target >= parentVal 라면 재배열 과정을 끝낸다.
            if(comparable.compareTo((E) parentVal) >= 0) {
                break;
            }

            array[index] = parentVal;
            index = parent;
        }

        array[index] = target;
    }

    public E remove() {
        // root가 비어있을 경우, 예외 발생
        if(array[1] == null) {
            throw new NoSuchElementException();
        }

        E result = (E) array[1];
        E target = (E) array[size]; // 맨 마지막 노드. 재배열을 위해서
        array[size] = null;

        // 삭제할 노드의 인덱스와 이후 재배치 할 타겟 노드를 넘겨준다.
        siftDown(1, target); // 루트 노드를 삭제하므로 인덱스를 1로 넘겨준다.
        return result;
    }

    /**
     * remove는 root에 있는 value를 삭제하고 반환한 후, 다시 재배열을 해주는 작업이 필요하다
     * sift-down(하향 선별)의 원리는 맨 마지막 노드를 루트에 놓은 후, 재배열을 진행하면서 heap의 성질을 유지하는 것이다.
     * 모든 노드와 비교할 수 있기 위해서 맨 마지막 노드를 선택하는 것이다.
     * @param index
     * @param target
     */
    private void siftDown(int index, E target) {
        if(this.comparator != null) {
            siftDownComparator(index, target);
        } else {
            siftDownComparable(index, target);
        }
    }

    /**
     * Comparator를 이용한 sift-down
     * @param index
     * @param target
     */
    @SuppressWarnings({"unchecked"})
    private void siftDownComparator(int index, E target) {
        array[index] = null;
        size--;

        int parent = index; // target을 넣을 인덱스. 재배열 과정에서 탐색 중인 인덱스이기도 하다.
        int child; // 교환할 자식을 가리키는 변수
        int rightChild; // 다른 자식 노드

        while((child = getLeftChild(parent)) <= size) {
            rightChild = getRightChild(parent);
            Object childVal = array[child];

            // 오른쪽 자식노드가 더 작을 경우, 오른쪽 자식노드와 target을 교체
            if(rightChild <= size && this.comparator.compare((E) childVal, (E) array[rightChild]) > 0) {
                child = rightChild;
                childVal = array[child];
            }

            // 최소 힙을 구현하기 때문에 target <= childVal 라면 재배열 과정을 끝낸다.
            if(this.comparator.compare(target, (E) childVal) <= 0) {
                break;
            }

            array[parent] = childVal;
            parent = child;
        }

        // target을 탐색이 끝난 index인 parent에 넣는다.
        array[parent] = target;

        if(size < array.length / 4) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }
    }

    /**
     * Comparable을 구현한 클래스는 이 메서드를 사용한 sift-down
     * @param index
     * @param target
     */
    @SuppressWarnings({"unchecked"})
    private void siftDownComparable(int index, E target) {
        // Generic이라서 사용하려면 interface로 casting 해줘야함
        Comparable<? super E> comparable = (Comparable<? super E>) target;
        array[index] = null;
        size--;

        int parent = index; // target을 넣을 인덱스. 재배열 과정에서 탐색 중인 인덱스이기도 하다.
        int child; // 교환할 자식을 가리키는 변수
        int rightChild; // 다른 자식 노드

        while((child = getLeftChild(parent)) <= size) {
            rightChild = getRightChild(parent);
            Object childVal = array[child];

            if(rightChild <= size && ((Comparable<? super E>)childVal).compareTo((E) array[rightChild]) > 0) {
                child = rightChild;
                childVal = array[child];
            }

            // 최소 힙을 구현하기 때문에 target <= childVal 라면 재배열 과정을 끝낸다.
            if(comparable.compareTo((E) childVal) <= 0) {
                break;
            }

            array[parent] = childVal;
            parent = child;
        }

        array[parent] = target;
        if(size < array.length / 4) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }

    }

    public int size() {
        return this.size;
    }

    public E peek() {
        if(array[1] == null) {
            throw new NoSuchElementException();
        }
        return (E) array[1];
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public Object[] toArray() {
        return Arrays.copyOf(array, size + 1);
    }

}
