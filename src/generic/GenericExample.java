package generic;

/**
 * 단일 Generic Class
 */
class GenericExample<E> {
    private E element;  // Type E Instance variable

    void set(E element) {   // Generic Parameter
        this.element = element;
    }

    E get() {   // Generic Return
        return element;
    }

    // 클래스의 E 유형과 별도로 메소드 레벨에서 독립적으로 Generic 선언해서 쓸 수 있음.
    <T> T genericMethod(T o) {    // T와 E는 서로 다른 Generic Type이다. T는 파라미터의 Type
        return o;
    }

    // Method의 E타입은 제네릭 클래스의 E타입과 다른 독립적인 타입이다.
    // 독립적으로 선언하는 이유는 이와 같이 static method를 선언할 때 필요하기 때문.
    // static은 stack 메모리에 먼저 올라가므로, 인스턴스 생성전에는 Generic Class Type E에 대한 정보가 없음.
    static <E> E staticGenericMethod(E o) { // 제네릭 메소드
        return o;
    }
}

/**
 * 이중 Generic Class
 * Map Collection도 이와 같이 이루어져 있다.
 * 설명을 key, value로 했으나 사실 first type, second type로 보면 된다.
 */
class GenericDoubleExample<K, V> {
    private K key;      // Type K
    private V value;    // Type V

    void set(K key, V value) {   // Generic Parameter
        this.key = key;
        this.value = value;
    }

    K getKey() {   // Key return
        return key;
    }

    V getValue() {   // Key return
        return value;
    }
}

/* ============================================= */
class Person {

}

class Student extends Person implements Comparable<Person> {
    @Override
    public int compareTo(Person o) {
        return 0;
    }
}

class testClass <E extends Comparable<? super E>> {

}


/**
 * Bounded Type Parameter 정의 ==========
 *
 * <K extends T>    T와 T의 자손 타입만 가능 (K는 들어오는 타입으로 지정 됨)
 * <K super T>      T와 T의 부모(조상) 타입만 가능 (K는 들어오는 타입으로 지정 됨)
 * <p>
 * <? extends T>	T와 T의 자손 타입만 가능
 * <? super T>	    T와 T의 부모(조상) 타입만 가능
 * <?>		        모든 타입 가능. <? extends Object>랑 같은 의미
 * <p>
 * Number와 이를 상속하는 Integer, Short, Double, Long 등의
 * 타입이 지정될 수 있으며, 객체 혹은 메소드를 호출 할 경우 K는
 * 지정된 타입으로 변환이 된다.
 * <K extends Number>
 * <p>
 * Number와 이를 상속하는 Integer, Short, Double, Long 등의
 * 타입이 지정될 수 있으며, 객체 혹은 메소드를 호출 할 경우 지정 되는 타입이 없어
 * 타입 참조를 할 수는 없다.
 * <? extends T>    // T와 T의 자손 타입만 가능
 */
class Main {
    public static void main(String[] args) {

        // Example 1
        GenericExample<Integer> exampleInt = new GenericExample<>();
        GenericExample<String> exampleString = new GenericExample<>();

        exampleInt.set(100);
        exampleString.set("one hundred");

        System.out.println("exampleInt data : " + exampleInt.get());
        // 반환된 변수의 타입 출력
        System.out.println("exampleInt E Type : " + exampleInt.get().getClass().getName());
        System.out.println();
        System.out.println("exampleString data : " + exampleString.get());
        // 반환된 변수의 타입 출력
        System.out.println("exampleString E Type : " + exampleString.get().getClass().getName());

        System.out.println("===========================================");

        // Example 2
        GenericDoubleExample<String, Integer> exampleDouble = new GenericDoubleExample<>();
        exampleDouble.set("apple", 990);

        System.out.println("key : " + exampleDouble.getKey());
        // 반환된 변수의 타입 출력
        System.out.println("K Type : " + exampleDouble.getKey().getClass().getName());
        System.out.println();
        System.out.println("value : " + exampleDouble.getValue());
        // 반환된 변수의 타입 출력
        System.out.println("V Type : " + exampleDouble.getValue().getClass().getName());

        System.out.println("===========================================");

        // Example 3
        // 제네릭 메소드 Integer
        System.out.println("<T> returnType : " + exampleInt.genericMethod(12500).getClass().getName());
        // 제네릭 메소드 String
        System.out.println("<T> returnType : " + exampleInt.genericMethod("ABCDEFG").getClass().getName());
        // 제네릭 메소드 ClassName b
        System.out.println("<T> returnType : " + exampleInt.genericMethod(exampleString).getClass().getName());

        System.out.println("===========================================");
        // Example 4
        System.out.println("Static method <E> returnType : " + GenericExample.staticGenericMethod(3.17).getClass().getName());

        System.out.println("===========================================");
        // Example 5
        testClass<Student> a = new testClass<>();


    }
}