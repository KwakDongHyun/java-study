package algorithm.suffixArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *  Suffix Array (접미사 배열)
 *  문자열을 다룰 때 많이 쓰는 자료구조이다.
 *  '문자열의 맥가이버 칼'로 불리며 다양한 문제를 푸는데 사용할 수 있고 대표적으로 문자열 검색이 있다.
 *  접미사 배열을 만드는 방법은 여러가지가 존재하기 때문에 method로 각각 구현하여 정리해두겠다.
 *
 *  - 기본 개념 -
 *  1. 주어진 문자열의 모든 접미사 집합을 사전순으로 정렬한 것이다.
 *
 *  2. 예를 들어 apple이라는 문자열에 대해서 접미사 집합과 접미사 배열은 아래와 같다. 접미사는 뒤에서부터 순차적으로 생성한다.
 *     A[i]를 접미사 집합을 저장하는 배열이라고 정의하면 i는 i번째 문자부터 문자열 길이 N까지의 부분 문자열(접미사)을 의미한다.
 *
 *     인덱스  |  접미사 집합  | 인덱스  |  접미사 배열
 *       4         e            0        apple
 *       3         le           1        e
 *       2         ple          2        le
 *       1         pple         3        ple
 *       0         apple        4        pple
 *
 *  3. 접미사를 저장하기 위해서 문자열을 자체를 저장하면 길이가 N인 한 문자열에 대해서 N^2의 메모리가 필요하다.
 *     그렇기 때문에 실제로는 각 접미사의 시작 위치(인덱스)를 저장한다.
 *
 *  4. 문자열 검색에 사용하는 원리를 설명하면 아래와 같다.
 *     어떤 문자열(이하 Text로 명명)이 검색어를 포함하고 있다고 가정하면, 검색어(이하 Pattern이라고 명명)는
 *     항상 어떤 접미사의 접두사가 되기에 이를 이용해서 문자열을 검색한다.
 *     예를 들어 pp라는 pattern은 apple이라는 text의 '접미사 pple의 접두사'이다.
 *
 *  5. 문자열 검색에 접미사 배열을 사용할 경우, 시간 복잡도는 단위의 의미로 표기한다면 O(NlogN)이다.
 *     접미사 배열의 길이가 L이라고 하면 Binary Search의 시간 복잡도는 O(logL)이고, pattern의 길이 만큼 문자열을 대조하기 때문에
 *     문자열에 대한 비교의 시간 복잡도는 O(N)이다. 따라서 실제 총 시간 복잡도는 O(NlogL)이다.
 *
 *
 *  - 동작 과정 -
 *  1. 접미사 집합을 만들고 이를 토대로 접미사 배열을 생성한다.
 *  2. 검색 원리를 이용하여 접미사 배열을 Binary Search를 통해서 Pattern이 출현하는 시작 위치를 찾는다.
 *
 */
public class SuffixArray {

    /**
     * Naive algorithm for construction Suffix Array
     * 가장 일반적인 접미사 배열을 만드는 알고리즘이다.
     * 정렬은 java의 내장된 Collections sort를 사용했다.
     *
     * 한편, 이 코드의 시간 복잡도는 O(N^2logN)이다.
     * 내장 정렬은 tim sort를 사용하기에 시간 복잡도는 O(NlogN)이다.
     * sort에 param으로 들어가는 comparator의 참조 method가 substring이다.
     * 문자열 1회 비교에 최악의 경우 문자열의 길이만큼 비교해야하기 때문에 최대 O(N)의 시간이 걸린다.
     * 따라서 Naive한 방법으로 접미사 배열을 생성하는데 걸리는 총 시간 복잡도는 두 개를 합쳐서 O(N^2logN)이다.
     *
     * @param text
     * @return
     */
    public List<Integer> getSuffixArray(String text) {
        int length = text.length();
        List<Integer> suffixArray = new ArrayList<Integer>(length);

        // 이 상태는 접미사 집합임.
        for(int i = 0; i < length; i++) {
            suffixArray.add(i);
        }

        /*
         * 람다나 익명 클래스 대신 static method인 comparing을 사용하여 정렬함.
         * parameter로 method reference 방식을 사용함.
         * i번째 인덱스 문자부터 시작하는 접미사를 substring으로 만들어서 접미사끼리 사전 비교를 하여
         * 접미사 집합을 접미사 배열로 만든다.
         */
        Collections.sort(suffixArray, Comparator.comparing(text::substring));
        return suffixArray;
    }

}
