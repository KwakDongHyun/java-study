package algorithm.suffixArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*
 * Manber-Myers(맨버-마이어스) 알고리즘
 * 접미사 배열에 대해서는 SuffixArray Class에 적어둔 주석과 설명을 참고하라.
 * 아무래도 따로 설명을 분리하는게 낫다고 판단해서 Suffix Array로부터 분리하였다.
 *
 *  - 기본 개념 -
 *  1. 접미사 배열을 형성하는 알고리즘 중에서 현실적으로 구현하기 쉽고 직관적으로 이해하기 쉬울만큼 간단 명료한 이유로 많이 쓰이고 있다.
 *
 *  2. 시간 복잡도는 O(N(logN)^2)으로 Naive한 방법으로 접미사 배열을 생성하는 방법의 시간 복잡도보다 훨씬 빠르다.
 *     시간 복잡도는 아래에서 좀 더 자세히 설명하도록 하겠다.
 *     이보다 더 시간이 적게 소모하는 O(N)의 시간 복잡도를 가지고 있는 알고리즘이 있긴 하다.
 *     radix 배열을 사용하는 방법이지만 너무 복잡해서 구현이 어렵다고 하여 실제로는 거의 쓰이지 않는다고 한다.
 *
 *  3. 맨버 마이어스 알고리즘의 핵심 아이디어는 접미사 집합을 처음부터 정렬하는 것이 아니라 그룹을 형성하면서 나누어 정렬하는 것이다.
 *     이 때 문자열끼리 단순 비교를 하지 않고 '2의 지수승 단위'의 index에 있는 문자만을 비교하면서 각각의 비교 Stage마다 Grouping 해나간다.
 *     즉, 접미사 집합을 정렬할 때 1,2,4,8... 2의 지수승 문자를 기준으로 그룹을 만든다.
 *
 *     자세한 예시는 https://m.blog.naver.com/zxwnstn/221573099471 에서 참고하라.
 *     직관적으로 이해하기가 매우 쉽다.
 *     문자 1개만을 비교하여 비교자체는 상수시간인 O(1)의 시간 복잡도가 걸리지만 모든 접미사가 정렬될 때까지(그룹에 자기 자신만 존재할 때까지)
 *     2의 지수승만큼 정렬을 수행해야 하기 때문에 O(logN)의 시간 복잡도가 걸린다.
 *     따라서 총 O(N * logN * logN)만큼의 시간 복잡도가 걸리게 된다.
 *     이는 N이 1,000,000이라고 할 때 Naive의 경우 3시간이 소요되지만 맨버-마이어스의 경우 약 10초가 소요된다.
 *
 *  4. 2의 지수승만큼의 문자만 비교할 수 있는 이유는 문자열의 부분 집합이 가지고 있는 독특한 속성 때문에 그러하다.
 *     어떤 접미사는 전체 길이의 접미사가 아닌 다른 접미사에 포함되는 부분 집합의 관계를 가지고 있으며,
 *     이는 달리 말해 특정 접두사를 제외하면 두 접미사는 같은 부분을 가지고 있다라는 말이 된다.
 *     어째서 이 속성으로 인해서 2의 지수승의 문자만을 비교해도 되는지 아래 예시를 통해서 설명하도록 하겠다.
 *
 *     x1x2x3...xn이라는 길이가 n인 문자열의 접미사 집합에는 x2x3x4x5...xn이라는 접미사와 x4x5...xn이라는 접미사가 존재한다.
 *     헌데 자세히 보면 두 접미사는 서로에게 부분 집합의 관계이면서 접두사 'x2x3'만 다를 뿐 나머지 부분은 같다.
 *     여기에 주목한 채로 정렬 과정을 살펴보도록 하자.
 *     편의상 x2x3x4x5...xn를 A접미사, x4x5...xn를 B접미사, A접미사와 두 번째 문자까지 같다고 가정한 C접미사가 있다고 하자.
 *     A접미사와 두 번째 문자까지 같을 경우, 2번의 정렬 과정을 거치면서 둘 다 같은 그룹에 속하게 될 것이다.
 *     그리고 나서 다음 정렬 과정을 자세히 분석해보자.
 *
 *     A접미사의 경우 3번째 문자인 x4를 기준으로 같은 그룹에 속한 다른 접미사의 비교하여 정렬해야 한다고 생각할 것이다.
 *     근데 가만 생각해보면 A접미사의 3번째 문자부터는 부분 집합인 접미사 B이고, 접미사 B 또한 정렬 과정을 거치면서
 *     똑같이 두 번째 문자까지 검증을 완료했을 것이다.
 *     A접미사 입장에서는 자신의 부분 집합인 접미사 B가 이미 두 번째 문자까지 검증을 완료했기 때문에 굳이 같은 과정을
 *     반복할 필요 없이 이 데이터를 그대로 사용하기만 하면 Stage를 단축시킬 수 있을 것이다.
 *     따라서 3번째 문자만 비교할게 아닌, 4번째 문자까지 같이 비교하면 되지 읺겠는가?
 *
 *     정렬이 완료될 때까지 단계를 이어나가면 1,2,4,8... 해당 인덱스의 문자를 확인하게 된다.
 *     이전 단계에서 검증된 문자열을 문자열의 부분 집합에서 발생하는 독특한 속성으로 인해서 사용할 수 있게 되기 때문이다.
 *     (접두사를 제외하면 두 문자열이 같다라는 속성)
 *
 *  - 동작 과정 -
 *  1. 접미사 집합을 만들고 이를 토대로 접미사 배열을 생성한다.
 *  2. 검색 원리를 이용하여 접미사 배열을 Binary Search를 통해서 Pattern이 출현하는 시작 위치를 찾는다.
 *
 */
public class ManberMyers {

    /**
     * Manber-Myers algorithm for construction Suffix Array (naive와 다르게 이렇게 설명하진 않지만 검색을 편하게 하기 위해 기입)
     * 맨버-마이어스 알고리즘을 사용하여 접미사 배열을 만든다.
     * 이 알고리즘에 대한 설명은 위에 적힌 본문을 참고하라.
     *
     * @param text
     * @return
     */
    public List<Integer> getSuffixArray(String text) {
        int nextIdx = 1; // 현재 비교할 접미사의 문자 위치 인덱스
        int length = text.length();

        List<Integer> suffixArray = new ArrayList<Integer>(length);
        // 리스트는 동적 할당이라 미리 할당해두지를 않아서 group 정보를 저장하기가 힘들다. 배열을 쓰는 이유가 있다.
        // idx는 i부터 시작하는 접미사를 가리킨다. i부터 시작하는 접미사의 group 정보가 저장된다.
        int[] group = new int[length];

        /*
         * 접미사 집합과 정렬하면서 생성될 그룹 정보를 저장할 배열을 초기화.
         * 접미사 집합의 초기값은 검색어 text를 기준으로 접미사의 첫 번째 문자 index. (experiment에서 p로 시작하는 접미사의 경우 2)
         * 그룹의 초기값은 문자의 ASCII값에 해당하는 정수를 입력.
         * 개념적으로 생각하면 i번째 문자부터 시작하는 접미사와 접미사의 첫 번째 문자를 기준으로 그룹을 지정해주는 것과 같다.
         */
        for(int i = 0; i < length; i++) {
            suffixArray.add(i);
            group[i] = text.charAt(i);
        }

        // 전체 문자열에서 2의 지수승 문자를 모두 탐색할 때까지 진행한다.
        while(nextIdx < length) {

            /*
             * 익명 클래스 또는 inner class에서 외부 변수 값을 변경하는 것을 원하지 않기 때문에 final이 아닐 경우 컴파일러 오류를 발생시킨다.
             * 내부 클래스 입장에서는 지역변수 또한 외부 변수이기 때문이다.
             * 이런 안전 장치가 있는 또다른 이유는 지역변수가 변경가능할 경우 멀티스레드 상태에서 공유변수가 되기 때문에 예상치 못한
             * 상황이 발생할 수 있기 때문이다. (일리가 있는 이유다)
             *
             * 따라서 내장된 sort를 사용하지 않고 따로 정렬을 구현하던지, 아니면 임시로 final 변수를 매 번 생성해서 거기에 값을 복사하여
             * 사용하던지 해야한다. 비효율적이라도 어쩔수 없다.
             * group -> finalGroup, nextIdx -> finalNextIdx 로 값을 복사해서 사용한다.
             */
            final int[] finalGroup = group;
            final int finalNextIdx = nextIdx;

            Collections.sort(suffixArray, new Comparator<Integer>() {
                /*
                 * 현재 stage에서 미리 nextIdx를 더해서 비교하는 작업을 하는 이유는 메모리를 효율적으로 사용할 수 있기 때문이다.
                 * 풀이법대로라면 특정 문자를 기준으로 정렬하고 나서, 다음 stage에서 같은 그룹에 속한 접미사들을 다음 2의 지수승의 문자들을 기준으로 grouping 작업을 할 것이다.
                 * 그렇게 하기 위해서는 이전에 검증된 데이터를(이전 단계에서 grouping한 결과. group의 value) 기억하고 있어야만 하는데,
                 * 이를 위해서 기존 데이터를 복사해서 간직하고 있어야한다.
                 *
                 * 그런데 이 작업을 다음 stage에서 할 필요없이 현재 stage에서 미리 수행하면, 불필요하게 메모리를 복사해둘 필요가 없다.
                 * 즉, 미래의 작업(다음 stage에서의 작업)을 현재로 가져와서 같이 수행해버리는 것이다.
                 * 사실 이렇게 진행해도 서로 독립적인 작업이라 문제도 없다.
                 *
                 * 이걸 이해하기 위해서 오랜 시간이 걸렸다... 굳이 왜 이렇게 구현했을까?라는 생각이 들어서.
                 */
                @Override
                public int compare(Integer suffix1, Integer suffix2) {
                    if(finalGroup[suffix1] != finalGroup[suffix2]) {
                        return finalGroup[suffix1] - finalGroup[suffix2];
                    }

                    /*
                     * 접미사 길이를 넘어갈 경우(검색어 길이를 넘어설 경우와 같음) grouping을 할 수 없으므로 -1로 설정해준다.
                     */
                    int nextSuffix1Group = suffix1 + finalNextIdx >= length ? -1 : finalGroup[suffix1 + finalNextIdx];
                    int nextSuffix2Group = suffix2 + finalNextIdx >= length ? -1 : finalGroup[suffix2 + finalNextIdx];

                    return nextSuffix1Group - nextSuffix2Group;
                }
            });

            /*
             * 현재 stage에 맞는 2의 지수승의 문자를 기준으로 정렬을 진행한 후에는, group 배열에 저장된 값을 기준으로 grouping 해줘야 한다.
             * grouping하면서 새로운 group 배열에 저장하는 값은 0부터 수동으로 값을 매긴다.
             * 오름차순으로 정렬하기 때문에 < 관계가 아니라면 = 관계 밖에 없다.
             */
            int[] newGroup = new int[length];
            newGroup[suffixArray.get(0)] = 0;

            for(int i = 1; i < length; i++) {
                /*
                 * 같은 그룹값이면 같은 그룹에 속하지만 다를 경우에는 다른 그룹이다. +1만큼 그룹 값을 증가해서 매긴다.
                 * 위에서 설명하였듯이 오름차순 정렬이기 때문에 =가 아니라면 뒤에 있는 값이 기본적으로 더 클 것이다.
                 */
                if(group[suffixArray.get(i)] > group[suffixArray.get(i-1)]) {
                    newGroup[suffixArray.get(i)] = newGroup[suffixArray.get(i-1)] + 1;
                } else {
                    newGroup[suffixArray.get(i)] = newGroup[suffixArray.get(i-1)];
                }
            }

            group = newGroup;
            /*
             * nextIdx의 역할은 이미 검증된 곳까지의 문자를 건너뛰고, 다음 2의 지수승 단위의 문자를 가리킬 수 있도록 더해주는 역할이다.
             * 첫 번째 작업 때, 0번째 문자와 1번째 문자를 기준으로 grouping한 결과를 new group에 저장하면 두 번째 문자까지를 기준으로
             * 정렬을 완료한 데이터를 저장한 것이다.
             * 따라서 다음 nextIdx를 더하면 자연스레 접미사의 세 번째 문자를 가리키게 된다.
             *
             * 우측으로 갈수록 stage별로 단계가 진행되는 과정을 표현한 것이며 이는 아래와 같다.
             *   0  ->  1  ->  3  -> 7  -> 15  ...
             *  +1  -> +2  -> +4  -> +8 -> +16 ...
             */
            nextIdx = nextIdx * 2; // 2의 지수승 단위이므로 2를 곱한다.

        } // 모든 정렬 과정을 거치고, 접미사 배열 생성 완료.

        return suffixArray;
    }

}
