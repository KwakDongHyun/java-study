package algorithm.networkFlow;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/*
 * Edmonds-Karp(에드몬드-카프) 알고리즘
 * 한 정점에서 다른 정점까지 흐를 수 있는 데이터의 최대 크기가 어느 정도인지를 확인하는 알고리즘이다.
 * 즉, 시작점에서 목표지점까지 데이터의 최대 유량을 확인하는 알고리즘이다.
 * 얕고 빠르게 이해하고 싶다면 https://yjg-lab.tistory.com/198 자료를 참고해서 보도록 한다.
 * 그림과 더불어서 직관적으로 이해하기 쉽게 설명하고 있다.
 * 그러나 제대로 이해하기 위해서는 논문을 필수적으로 읽어야 한다. 날먹식 설명들 때문에 절대로 이해할 수 없기 때문이다.
 * Ford-Fulkerson 방법론에 대한 설명인 https://gazelle-and-cs.tistory.com/63 와
 * Edmonds-Karp 알고리즘에 대한 설명인 https://gazelle-and-cs.tistory.com/82?category=875540 를 모두 읽기를 바란다.
 * 이것만 제대로 이해하는데 1~2주가 걸렸지만 그럴만한 가치가 있고, 그만큼 난이도가 높다.
 * 현재 모든 것을 제대로 이해했기 때문에 그 방대한 내용을 여기에 적진 않겠다. 논문의 경우 참고 링크를 통해서 다운받아서 읽도록 한다.
 *
 *  - 기본 개념 -
 *  1. Network Flow는 한 정점에서 다른 정점까지 데이터의 최대 유량을 확인하는 알고리즘이다.
 *     유향 그래프에서 각 간선은 데이터가 흐를 수 있는 정해진 용량으로 제한되어 있으며, 이를 최대한의 양으로 얼마나 흐르게 할 수 있는지
 *     확인할 수 있다. 이것을 '최대 유량 문제'(Max Flow)로 정의하며 이를 해결하기 위한 대표적인 알고리즘이 Edmonds-Karp 알고리즘이다.
 *
 *  2. Network Flow는 도로망의 교통 흐름을 분석하거나 전자 회로의 전류, 배수관을 흐르는 유체, 유량 등을 연구하는데 사용한다.
 *     현재 흐르고 있는 데이터의 양을 '유량', 간선에 흐를 수 있는 최대 데이터의 양을 '용량'이라고 표현한다.
 *     기본적으로 평면의 직선상에서 Max Flow를 구한다면 용량이 가장 작은 Edge가 답이 된다.
 *     이 원리를 기반으로 좀 더 복잡한 그래프에서도 Network Flow를 통해 Max Flow를 해결할 수 있다.
 *
 *  3. 최대 유량 문제를 해결하기 위해서는 BFS를 활용하여 가능한 모든 경우의 수를 탐색하면서 목표지점으로의 최대 유량을 구해나간다.
 *     자세한 과정은 그래프의 그림과 같이 봐야 이해가 되므로 여기에서는 따로 설명하진 않겠다.
 *     위에 참고자료를 통해서 원리는 파악하는 것이 쉽고 빠르다.
 *     한 가지 주의해야할 특징이 있다면, 최대 유량 계산을 위해서 반대로 흐르는 -유량을 가정하고 계산을 한다는 것이다.
 *     이는 본래라면 유향 그래프라서 불가능한 유량값이지만, 조금이라도 다른 경로에서 유량을 더 추가할 가능성이 있을 경우, 이를 위해서
 *     어느 특정 간선의 유량을 줄이는 경우가 필요하기 때문이다.
 *
 *  4. Edmonds-Karp 알고리즘의 상위 버전으로 '이분 매칭' 알고리즘 등이 있다.
 *     시간복잡도는 O(|V|*|E|^2)로써 (정점 수 * (간선 수의 제곱))의 제곱이다.
 *     BFS를 통해서 최단 경로를 찾는 것은 O(|E|)만큼 시간이 걸리고, Max Flow를 구할 때까지 반복문이 실행되는 시간은 O(|V|*|E|)만큼 걸린다.
 *     이는 최단 경로를 구할 때 임의의 간선이 '병목 증가 간선'이 될 횟 수는 최대 |V|/2를 넘지 않고, 잔여 네트워크에 등장할 수 있는 간선은
 *     원래 네트워크의 forward edge or backward edge이 뿐이므로 그 수는 2|E|를 넘지 않는다.
 *     따라서 시간복잡도는 O(2|E| * |V|/2 * |E|)가 되는 것이다.
 *
 *     이는 worst case에 집중하여 구한 시간 복잡도이며, 최단 경로가 다항식으로 증가한다는 것에만 초점을 맞춰서 vertex를 기준으로 시간 복잡도를 구할 경우
 *     O(|E|*|V|^2)이다.
 *     이는 흐름을 증가시키지 전의 최단 경로에서 +a만큼 선형적으로 최단 경로가 증가한다는 것을 생각했고, 증가하는 흐름을 더해서 새로운 흐름을 구하는데 걸리는
 *     시간 복잡도가 O(|V|), 잔여 네트워크를 구하는데 걸리는 시간 복잡도가 O(|V|), 최단 경로를 BFS로 탐색하는데  O(|E|)만큼 시간이 걸린다.
 *     이를 모두 곱할 경우 O(|E|*|V|^2)가 된다. 그리고 이는 worst case를 고려하지 않은 시간 복잡도이므로 정확하지 않기에 위와 같이 시간 복잡도를 구한다.
 *
 *  5. Ford-Fulkerson Method를 구현한 알고리즘이 Edmonds-Karp 알고리즘이다.
 *     Ford-Fulkerson Method의 경우 시간복잡도는 O(|E|*|F*|) <= O(|E|*|C|)만큼 걸린다.
 *     이 말은 최소 이 네트워크의 최대 유량이상, 최대 용량 이하만큼 반복문이 실행된다는 것이다.
 *
 */
public class EdmondsKarp {

    private final int INF = Integer.MAX_VALUE;

    private List<List<Integer>> graph; // 그래프 정보를 저장하고 있는 리스트

    private int vertexCnt; // 정점의 개수

    private int[][] capacity; // 용량을 저장하는 2차원 배열

    private int[][] flow; // 현재 유량을 저장하는 2차원 배열

    // 현재 정점의 방문 여부와 backtracking을 하기 위해서 어느 정점으로부터 방문한 것인지를 저장하는 1차원 배열
    private int[] visited;

    private int result; // 결과값

    public EdmondsKarp(List<List<Integer>> graph, int[][] capacity) {
        this.graph = graph;
        this.capacity = capacity;
        this.flow = new int[capacity.length][capacity[0].length];
        this.vertexCnt = graph.size() - 1; // vertex는 1부터 시작하므로, 0인 데이터를 제거해줘야함.
        this.visited = new int[vertexCnt + 1]; // 0은 무의미한 데이터이다.
    }

    private void maxFlow(int src, int dest) {
        result = 0;

        while(true) {
            Arrays.fill(visited, -1); // -1로 초기화
            Queue<Integer> queue = new LinkedList<Integer>();
            queue.offer(src);

            while(!queue.isEmpty()) {
                int curVertex = queue.poll();
                // 인접한 정점 리스트
                List<Integer> connectedVertexes = graph.get(curVertex);

                for(int adjacentVertex : connectedVertexes) {
                    /*
                     * 아직 방문하지 않은 vertex 중에서 보낼 수 있는 유량이 남아 있는 경우
                     * 해당 vertex를 queue에 넣고, 어느 vertex로부터 방문했는지를 저장한다.
                     * 기존에 봐왔던 graph의 visited 배열과 차별되는 점이다.
                     */
                    if(capacity[curVertex][adjacentVertex] - flow[curVertex][adjacentVertex] > 0 && visited[adjacentVertex] == -1) {
                        queue.offer(adjacentVertex);
                        visited[adjacentVertex] = curVertex;

                        // 목표지점까지 도달했을 경우 recursive statement escape
                        if(adjacentVertex == dest) {
                            break;
                        }
                    }
                }
            } // inner while end

            /*
             * BFS를 통해 모든 경로를 다 탐색한 경우, 목표지점을 더는 방문하지 않게 된다.
             */
            if(visited[dest] == -1) {
                break;
            }

            int minFlow = INF;
            /*
             * 가용 가능한 최소 유량 탐색
             * visit 배열을 활용하여 목표지점부터 시작지점까지 거꾸로 탐색하면서 (용량 - 유량)의 최소값을 구한다.
             * 최초에는 모든 edge의 유량이 0으로 설정되어있기 때문에 최소 유량을 찾게 될 것이다. (flow 배열의 초기값도 0)
             */
            for(int i = dest; i != src; i = visited[i]) {
                minFlow = Math.min(minFlow, capacity[visited[i]][i] - flow[visited[i]][i]);
            }

            /*
             * 탐색한 네트워크 경로에 위에서 찾은 가용 가능한 최소 유량을 flow 배열에 추가한다.
             * 이 때, 반대 방향으로 흐르는 -유량 값도 flow 배열에 추가한다.
             * (어디까지나 이론을 완성하기 위한 가공의 값이다)
             */
            for(int i = dest; i != src; i = visited[i]) {
                flow[visited[i]][i] += minFlow;
                flow[i][visited[i]] -= minFlow;
            }

            result += minFlow;

        } // outer while end
    }

    public int getMaxFlow(int src, int dest) {
        maxFlow(src, dest);
        return result;
    }

}
