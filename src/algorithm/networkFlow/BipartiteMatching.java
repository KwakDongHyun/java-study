package algorithm.networkFlow;

import java.util.Arrays;
import java.util.List;

/*
 * Bipartite Matching(이분 매칭) 알고리즘
 * 이분 그래프에서 모든 경로의 방향이 한쪽으로만 흐르는 상태라고 가정할 때, 이 그래프의 최대 유량을 구하는 알고리즘이다.
 * https://yjg-lab.tistory.com/209 자료를 참고해서 보도록 한다. 그림 없이 이해하기에는 설명하기가 어렵다.
 * Network Flow 알고리즘에 대해서 알고 있다면 이 알고리즘에 대해서 좀 더 이해하기가 쉽다.
 *
 *  - 기본 개념 -
 *  1. Network Flow는 한 정점에서 다른 정점까지 데이터의 최대 유량을 확인하는 알고리즘이다.
 *     유향 그래프에서 각 간선은 데이터가 흐를 수 있는 정해진 용량으로 제한되어 있으며, 이를 최대한의 양으로 얼마나 흐르게 할 수 있는지
 *     확인할 수 있다.
 *
 *  2. Network Flow는 도로망의 교통 흐름을 분석하거나 전자 회로의 전류, 배수관을 흐르는 유체, 유량 등을 연구하는데 사용한다.
 *     현재 흐르고 있는 데이터의 양을 '유량', 간선에 흐를 수 있는 최대 데이터의 양을 '용량'이라고 표현한다.
 *     기본적으로 평면의 직선상에서 Max Flow를 구한다면 용량이 가장 작은 Edge가 답이 된다.
 *
 *  3. Bipartite Matching을 이해하기 위해서는 먼저 '이분 그래프'에 대해서 알아야만 한다.
 *     '이분 그래프'란 서로 다른 그룹에 속하는 두 개의 vertex 그룹이 존재할 때 모든 edge의 용량이 1인 특수한 그래프를 '이분 그래프'라고 한다.
 *     Network Flow의 관점으로 본다면 이분 그래프도 경로가 있는(방향이 있는) 유향 그래프로 볼 것이다.
 *
 *     '이분 매칭'은 이분 그래프의 모든 edge에서 데이터가 한쪽 그룹에서 또다른 그룹으로 흐르도록 고정된 '특수한 환경'에서 Max Flow 문제를
 *     해결하기 위한 알고리즘이다.
 *     이분 그래프는 모든 edge의 용량이 1이기 때문에 max flow를 구한다는 말은 다른 관점으로 본다면 두 그룹의 정점 간의 최대 매칭 수를
 *     구한다는 말과 일맥상통하다고 생각한다.
 *     최대한 많이 연결해야 최대한 많은 유량이 흐를 것이기 때문이다.
 *     정리하자면 '이분 매칭' 알고리즘은 특수한 Network 환경에서 Max Flow를 해결하기 위한 알고리즘이고, 이 알고리즘의 목적은
 *     두 그룹의 정점들 간에 최대 매칭 수를 구하는 것이 목적이다.
 *     이런 이유로 Edmonds-Karp 알고리즘을 설명할 때도 언급했듯이, 이분 매칭 알고리즘이 Edmonds-Karp 알고리즘의 상위 버전으로 사용될 수도
 *     있다.
 *
 *     이분 매칭을 적용할 수 있는 문제는 주변에서 쉽게 예시를 찾을 수 있다.
 *     강에 최대한 많은 다리를 건설하는 문제, 커플 매칭 등이 있다.
 *
 *  4. 이분 매칭은 Edmonds-Karp과 다르게 DFS를 적용했다.
 *     이분 매칭에서 다루는 '특수한 조건을 가진 이분 그래프'의 환경 특성과 맞물려서 Edmonds-Karp보다 더 효율적인 동작을 보여준다.
 *     시간복잡도는 O(v*e)이다. (정점 수 * 간선 수)
 *
 *  - 번외 -
 *  가급적이면 네트워크 플로우와 관련된 기본 논문부터 쾨니그의 정리, 홀의 정리, 최대유량 최소컷 정리까지 모두 읽기를 권한다.
 */
public class BipartiteMatching {

    private final int INF = Integer.MAX_VALUE;

    private List<List<Integer>> graph; // 그래프 정보를 저장하고 있는 리스트

    private int leftVertexCnt; // 좌측 vertex 집합의 개수

    private int rightVertexCnt; // 우측 vertex 집합의 개수

    private boolean[] matched; // 우측 vertex가 좌측의 정점과 연결되어 있는지의 여부를 저장하는 배열

    private int[] matchInfo; // 우측 vertex가 좌측의 어떤 vertex와 연결되어 있는지를 저장하는 배열

    private int count; // 연결 가능한 최대 매칭 수

    public BipartiteMatching(List<List<Integer>> graph, int leftVertexCnt, int rightVertexCnt) {
        this.graph = graph;
        this.count = 0;
        this.leftVertexCnt = leftVertexCnt;
        this.rightVertexCnt = rightVertexCnt;
    }

    private boolean findMatching(int srcVtx) {
        List<Integer> connectedVertexes = graph.get(srcVtx);

        for(int connectedVertex : connectedVertexes) {
            if(matched[connectedVertex]) {
                continue;
            }
            matched[connectedVertex] = true;

            /*
             * matchInfo가 실제로 의미있고 중요한 정보이다.
             * 이미 다른 정점과 연결되어 있을 경우, srcVtx에게 연결을 우선으로 양보하고 새로운 경로를 찾아나선다.
             * 좌측 vertex를 단계별로 추가하면서 최대 매칭을 찾는 과정이기 때문에 이 사고과정에 적합한 DFS를 사용한 것이다.
             *
             * if의 두 번째 조건은 새로 추가된 vertex를 우선적으로 매칭하고서, 매칭되어 있었던 나머지 vertex들이
             * 새로운 matching이 있는지를 돌면서 확인한다.
             * 만약 이미 max matching일 경우 새로 추가된 vertex가 고유한 다른 matching 경로가 있는지 찾고,
             * 없으면 다음 좌측 vertex의 matching 경로를 찾기 위해 종료된다.
             *
             * 정리하자면 아래와 같다.
             * if 조건의 핵심은 매칭이 없으면 바로 매칭, 이미 매칭된 vertex가 있을 경우 이 매칭을 선점했을시
             * 나머지 vertex들에게 선택 가능한 다른 matching 경로가 존재하는지 확인한다.
             * 다른 경로가 존재하면 선점한 매칭을 그대로 사용, max matching 상태라면 나머지 매칭 가능한 경로를
             * 계속해서 탐색한다.
             */
            if(matchInfo[connectedVertex] == 0 || findMatching(matchInfo[connectedVertex])) {
                matchInfo[connectedVertex] = srcVtx;
                return true;
            }
        }

        return false;
    }

    public void matchBipartite() {
        this.matched = new boolean[this.rightVertexCnt + 1];
        this.matchInfo = new int[this.rightVertexCnt + 1];
        this.count = 0;

        for(int i = 1; i <= leftVertexCnt; i++) {
            Arrays.fill(matched, false);
            /*
             * 경로를 탐색하는 방법으로 DFS를 사용함.
             */
            if(findMatching(i)) {
                count++;
            }
        }

    }

}
