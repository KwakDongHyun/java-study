package algorithm.kmp;

import java.util.LinkedList;
import java.util.List;

/**
 *  KMP 알고리즘
 *  문자열 검색에 사용되는 알고리즘 중 하나이다.
 *  워드프로세스의 문자열 검색 기능 등에 사용된다.
 *  최악의 경우에는 N이 Text의 길이, M이 Pattern의 길이라고 정의하면 시간 복잡도는 O(NM)이 된다.
 *  https://bowbowbow.tistory.com/6 자료를 참고하자. 굉장히 잘 설명되어 있다.
 *
 *  - 동작 과정 -
 *  1. 검색어(이하 Pattern 혹은 Search로 명명) 를 바탕으로 pi배열을 생성한다.
 *     pi배열이란 pattern의 0부터 i번째 인덱스까지의 부분 문자열에서 prefix와 suffix가 동일한 문자열들의 집합 중, 가장 긴 문자열의
 *     길이를 저장하는 배열이다.
 *     예를 들어 ababa~~...ababa 라는 문자열이 있을 때 (a, aba, ababa)가 prefix와 suffix가 같은 부분 문자열이지만, pi배열에 저장되는
 *     값은 가장 긴 ababa의 길이인 5가 저장된다.
 *  2. pi배열을 사용하여 자료에서(이하 Target 혹은 Text로 명명) Pattern과 일치하는 문자열을 찾는다.
 *  3. 중간에 불일치할 경우 pi배열을 사용해 불필요하게 비교하지 않아도 되는 문자열을 건너뛰고, Pattern에서 재검색을 할 index를 찾아서
 *     해당 지점부터 다시 문자열 검색을 이어나간다.
 *  4. 일치할 경우 카운트를 하고서, pi배열을 사용하여 Pattern내에서 prefix와 일치하는 suffix부터 다시 검색을 이어나간다.
 *     토마토마토마토.. 라는 문자열에서 토마토를 검색한다는 상황을 이해할 수 있으면 4번 절차의 의미를 알 수 있다.
 *
 */
public class Kmp {

    /**
     * 검색어가 발견된 위치를 반환한다.
     * 여기서는 사람의 기준으로 반환하기 때문에 index+1을 return해준다.
     *
     * @param inputData 첫 번째는 text, 두 번째는 검색어가 들어있는 길이가 2인 배열을 입력으로 받는다.
     * @return
     */
    public List<Integer> solveProblem(String[] inputData) {
        /*
         * 2가지가 알고리즘을 구현의 핵심이다.
         *  1. pattern 문자열 전처리를 통해서 pi배열 정보 얻기
         *  2. 1번을 토대로 문자열 검색 진행
         */
        String text = inputData[0];
        String pattern = inputData[1];
        List<Integer> results = new LinkedList<Integer>();

        int tLen = text.length();
        int pLen = pattern.length();

        int[] pi = new int[pLen];
        char[] patternChars = pattern.toCharArray();
        int patternIdx = 0;

        /*
         * pi배열 구하기
         * 위 해석내용 참고.
         * 어떤 관점으로는 DP의 메모이제이션과 같은 풀이법이기도 하다.
         * 전체 문자열은 제외이므로 길이가 1인 경우 0임.
         *
         * pi배열을 생성하는 것도 KMP 알고리즘의 원리를 적용하면 O(M)시간에 수행할 수 있다. (m은 pattern의 길이)
         * pi배열에 저장하는 길이가 곧 prefix에서 다음에 비교할 index 지점이라는 것을 염두하고 생각해보자.
         * 다음에 비교할 지점에서의 문자가 i지점에 추가된 문자와 불일치한 상황에서 다시 prefix와 suffix가 같은 것 중 가장 긴 것을 찾아야한다.
         * prefix와 suffix가 같기 때문에, 여태까지 검증한 지점인 i-1지점까지의 문자열에서 prefix == suffix인 것 중에서 가장 긴 부분을
         * 구할 수만 있다면 다음에 비교할 지점을 알 수가 있게 된다.
         * 게다가 i-1지점까지의 문자열은 pattern의 부분 문자열이라서 pi배열을 그대로 사용하면 되기 때문에 메모이제이션을 통한
         * 빠른 수행이 가능하다는 장점이 있다.
         *
         * ex) 빠른 이해를 위한 그림
         * -----------  pattern           -----------  pattern
         * -------      prefix     ->     ----         prefix
         *     -------  suffix                   ----  suffix
         * 이런식으로 prefix와 suffix가 같은 상태에서 교차되어 있다고 생각하면 이해가 된다.
         * ------- 내에서 pi배열을 구하면 prefix와 suffix가 같은 부분인 ----을 찾으면
         * 앞에는 ----|---가 선택되고 뒤에는 ---|----가 선택되는 것이기 때문에 prefix == suffix 조건을 만족하게 된다.
         */
        for(int i = 1; i < pLen; i++) {
            while(patternIdx > 0 && patternChars[i] != patternChars[patternIdx]) {
                /*
                 * 다음에 비교할 부분 문자열의 인덱스
                 * 단순한 -1이 아닌 pi의 값이 포인트다. 패턴은 변화가능하기 때문.
                 */
                patternIdx = pi[patternIdx - 1];
            }

            if(patternChars[i] == patternChars[patternIdx]) {
                pi[i] = ++patternIdx;
            }
        }

//        // 검증
//        for(int i = 0; i < pi.length; i++) {
//            System.out.println("pi[" + i + "] : " + pi[i]);
//        }

        /*
         * pi배열을 사용하여 문자열 검색하기
         */
        char[] textChars = text.toCharArray();
        patternIdx = 0;

        for(int i = 0; i < tLen; i++) {
            while(patternIdx > 0 && textChars[i] != patternChars[patternIdx]) {
                patternIdx = pi[patternIdx - 1]; // 나머지 부분 문자열에서 pi배열을 찾아나간다.
            }

            if(textChars[i] == patternChars[patternIdx]) {
                /*
                 * 검색어와 일치하는 문자열을 찾았을 경우, pi배열을 사용하여 다시 부분적으로 일치하는 문자열 부분부터 검색을 이어나간다.
                 * 반복 패턴부분부터 검색어가 다시 존재할 수도 있기 때문에
                 */
                if(patternIdx == pLen - 1) {
                    results.add(i - pLen + 1);
                    patternIdx = pi[patternIdx];
                } else {
                    patternIdx++;
                }
            }
        }

        return results;
    }

}
