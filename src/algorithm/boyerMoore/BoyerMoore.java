package algorithm.boyerMoore;

/*
 * Boyer-Moore(보이어-무어) 알고리즘
 * 문자열 검색에 사옹되는 알고리즘 중에 하나이다. KMP보다 개선된 시간 복잡도를 가지고 있다.
 * https://yoongrammer.tistory.com/94 자료를 참고해서 보도록 한다.
 * 그림과 더불어서 직관적으로 이해하기 쉽게 설명하고 있다.
 *
 *  - 기본 개념 -
 *  1. 보이어-무어 알고리즘은 보통 상황에서 문자열은 앞부분보다는 뒷부분에서 불일치가 일어날 확률이 높다는 성질에 주목하고 이를 활용한다.
 *     그래서 기존 검색 알고리즘과 달리 Pattern의 오른쪽 끝에서 왼쪽으로 역방향 비교 방식을 사용한다.
 *     또한, KMP 알고리즘처럼 Pattern(검색어)의 Pre-processing을 통해 얻은 정보를 토대로 불필요한 문자 비교를 가능한 많이 생략하여
 *     유의미한 비교만을 수행하여 비교 횟수를 줄이게끔 설계되어 있다.
 *     Pattern을 Text의 오른쪽으로 상황에 따라 적절히 이동시킴으로써(offset만큼 이동) 비교 횟수를 줄이고자 하는 것이다.
 *
 *  2. 1번에서 설명한 것처럼 이동을 얼만큼 할지 정하기 위해서 Boyer-Moore 알고리즘은 두 가지 휴리스틱을 사용한다.
 *     불일치한 경우에 아래의 두 휴리스틱에서 제공하는 offset 중 가장 큰 offset을 사용해서 Pattern 내에서 다음에 비교할 위치로 이동시킨다.
 *
 *      1) Bad Character Heuristic
 *      2) Good Suffix Heuristic
 *
 *     pattern을 이동시키는 작업을 하기 위해서 pattern을 구성하는 각 문자에 대해서 얼마만큼 검색 위치를 이동시켜야 하는지를 알아야 한다.
 *     이를 저장하는 array를 'skip table' 또는 'jump table' 이라고 명명한다.
 *     아래에서 각각의 휴리스틱에 대해서 하나씩 설명하도록 하겠다.
 *     또한 각각의 휴리스틱과 이를 사용하는 검색을 따로 구현하도록 하겠다. 주석으로 메모를 남길테니 참고하기 바란다.
 *
 *  3. 이 항목은 Bad Character Heuristic를 설명한다.
 *     Bad Character는 패턴의 현재 문자와 일치하지 않는 Text의 문자를 말한다.
 *     Bad Character Heuristic는 Bad Character가 발생했을 때 Pattern을 이동시키기 위한 offset을 제공한다.
 *     Bad Character가 발생하는 경우는 아래와 같은 두 가지 상황이 있고, Bad Character Heuristic는 각 상황에 맞는 offset을 제공한다.
 *
 *     case 1. Bad Character가 Pattern 내에 존재할 때
 *     Bad Character와 같은 Pattern 내의 문자 중 가장 오른쪽에 위치하는 문자에 맞춰서 Pattern을 이동한다.
 *
 *     ex1)
 *      Text     A B A A B A B A D B   before
 *     Pattern   A A B C B
 *
 *      Text     A B A A B A B A D B   after
 *     Pattern       A A B C B
 *
 *     text의 index 3에 있는 A와 pattern의 C가 일치하지 않는다.
 *     Bad Character A가 Pattern 내에서 index 0과 1 두 곳에 위치해 있지만 가장 우측에 있는 index 1에 위치한 A의 위치에 맞춰서
 *     Pattern을 이동시킨다.
 *     -------------------------------------------------------------------------------------------------------
 *     case 2. Bad Character가 같은 Pattern의 문자 중 가장 우측에 있는 문자가 Bad Character가 발생한 지점을 지나쳤을 때
 *     case 1을 만족한 상황에서 추가로 발생하는 case이다.
 *     한편, Bad Character와 같은 Pattern의 문자 중 가장 우측에 있는 문자가 Bad Character가 발생한 위치보다 우측에 있으면
 *     Pattern을 현재 위치에서 +1만큼 뒤로 이동시킨다.
 *     이렇게 하는 이유는 특정 Bad Character가 Pattern 내에서 여러 개가 존재하더라도 가장 우측에 있는 문자에 대한 값만을 저장하기 때문에
 *     나머지 위치에 존재하는 문자에 대한 값은 알 수가 없다.
 *     만약 값을 저장해둔다 하더라도 Bad Character가 발생한 지점과 가장 가까운 곳에 맞춰서 Pattern을 이동시키기 위해서는 최악의 경우
 *     같은 상황이 발생할 때마다 Pattern의 길이만큼 배열을 조회하여(O(M)) 해당 값을 찾아야만 한다.
 *     이렇게 되면 본래 이 알고리즘의 의도와는 정반대로 처리하게 되는 것이다.
 *
 *     그러한 이유로 인해서 차라리 Naive한 방법처럼 +1만큼 Pattern을 뒤로 이동시켜서 검색 위치를 빠르게 찾는 차선책의 방법을 사용한 것으로
 *     추측된다.
 *     +1만큼 해서 pattern을 한 번에 찾으면 좋고, Bad Character가 발생하더라도 Bad Character가 발생한 지점이 Bad Character와 일치하는
 *     Pattern의 문자 중에서 가장 우측에 있는 문자의 위치를 넘지 않으면 빠르게 다음 탐색 위치를 잡을 수 있어서 손해볼 것은 없다.
 *     아래 example2에서 이를 설명한다.
 *
 *     ex2)
 *      Text     A B D D B A B A D B   before
 *     Pattern   A D B D B
 *
 *      Text     A B D D B A B A D B   after
 *     Pattern     A D B D B
 *
 *     text의 index 2에 있는 D와 pattern의 B가 일치하지 않는다.
 *     Bad Character가 발생한 위치가 pattern 내에서 Bad Character와 동일하면서 가장 우측에 위치한 곳인 index 3의 D를 이미 지나쳤다.
 *     그래서 차선책으로 Naive한 처리 방식처럼 +1만큼 Pattern을 이동시켜서 검색을 이어서 진행할 수 있도록 한다.
 *     -------------------------------------------------------------------------------------------------------
 *     case 3. Bad Character가 Pattern 내에 존재하지 않을 때
 *     Pattern의 선두에 있는 문자가 Bad Character가 발생한 다음 위치에 있도록 이동시킨다.
 *     즉, Bad Character가 발생한 다음 위치부터 Text와 비교할 수 있도록 Pattern의 위치를 조정한다.
 *
 *     ex1)
 *      Text     A B A C B A B A D B   before
 *     Pattern   A D B D B
 *
 *      Text     A B A C B A B A D B   after
 *     Pattern           A D B D B
 *
 *     Bad Character가 발생한 위치의 다음 위치인 text의 index 4 이후에 pattern이 위치할 수 있도록 이동시킨다.
 *
 *  4. 이 항목은 Good Suffix Heuristics를 설명한다.
 *     Bad Character Heuristics 방법으로는 case 3이 발생했을 때 최적의 위치로 jump를 할 수가 없어서 한 칸만 이동할 수 밖에 없다.
 *     Bad Character Heuristics의 case 3에 대한 처리방법을 개선하기 위해서 등장한 방법이 Good Suffix Heuristics이다.
 *     이 휴리스틱을 통해서 Bad Character Heuristic case 3일 경우에도 최대 이동 거리를 도출하는 것이 가능하다.
 *
 *     Good Suffix는 Pattern의 문자 일부와 일치하는 text의 부분 문자열을 말한다.
 *     Boyer-Moore 알고리즘은 특성상 Pattern의 뒤에서부터 비교를 하기 때문에 Bad Character가 발생하기 전까지 일치하는 부분 문자열은
 *     결국 pattern과 현재 비교 중인 text의 부분 문자열 입장에서는 suffix가 된다.
 *     그렇기 때문에 Good Suffix라고 명명하는 것이 아닌가라고 생각한다.
 *     또한 Suffix Array에 대해서 알고 있다면 Good Suffix Heuristic이 어떻게 처리를 하는지 대강은 유추할 수 있을 것 같다.
 *     Good Suffix가 발생하는 구체적인 경우가  세 가지 상황이 있으며, 각 상황에 맞는 offset을 제공한다.
 *
 *     case 1. Good Suffix가 Pattern내에 다른 곳에도 존재하는 경우
 *     다른 곳에 존재하는 good suffix와 일치하는 문자열을 Good Suffix에 맞춰서 일치하도록 pattern을 이동시킨다.
 *
 *     ex1)
 *      Text     A B A A A B C A B B   before
 *     Pattern   A A B C A B
 *
 *      Text     A B A A A B C A B B   after
 *     Pattern         A A B C A B
 *
 *     Bad Character가 발생한 위치는 Text의 index 3 지점이고, Bad Character는 2개 이상 Pattern 내에 존재하면서 가장 우측에
 *     존재하는 Bad Character는 이미 Bad Character가 발생한 지점으로부터 지나쳐버린 상황이다.
 *     즉, Bad Character Heuristics case 3인 상황이다.
 *
 *     Good Suffix는 Bad Character가 발생하기 전까지 일치했던 Text의 index 4부터 5까지의 접미사인 A B이다.
 *     Good Suffix와 일치하는 부분 문자열은 Pattern의 index 1부터 2까지인 곳에 1개가 존재한다.
 *     이 부분 문자열을 Good Suffix에 맞춰서 다음 검색을 진행할 수 있도록 Pattern을 이동시킨다.
 *     여기서는 3만큼 Pattern을 이동하는 것이다.
 *     -------------------------------------------------------------------------------------------------------
 *     case 2. Good Suffix의 일부만 Pattern의 시작 부분에 있는 경우
 *     Good Suffix의 부분 문자열이 Pattern의 접두사에 있는 경우를 말한다.
 *     처리 방법은 pattern의 접두사 부분을 good suffix 부분에 맞춰서 일치하도록 pattern을 이동시킨다.
 *
 *     ex2)
 *      Text     A A B A B A B A D B   before
 *     Pattern   A B B A B
 *
 *      Text     A A B A B A B A D B   after
 *     Pattern         A B B A B
 *
 *     text의 index 2부터 4까지의 접미사 B A B가 Good Suffix이다.
 *     pattern의 index 0부터 1까지의 접두사 A B는 Good Suffix의 부분 문자열이기 때문에 다음 탐색 지점으로 맞춰서 비교할 수 있다.
 *     따라서 여기서는 text의 index 3부터 4에 맞춰서 일치하도록 Pattern을 3만큼 이동하는 것이다.
 *
 *     case 2에 대해서 심화적으로 이해를 하기 위해서 case 3으로 넘어가기 전에 본 내용을 서술한다.
 *     case 2가 발생하는 경우에 대해서 자세히 생각해보면 추후에 나올 case 3에 처리 이유와 효율적인 처리 방법인 이유에 대해서 알 수 있다.
 *     결론부터 먼저 말하면 case 2는 Good Suffix가 존재하는 상황에서 Pattern내에서 good suffix의 부분 문자열(혹은 일부)이
 *     접두사에 존재하여 prefix와 suffix가 같은 환경이 조성된 경우를 말한다.
 *     즉, prefix와 suffix가 같기 위해서는 prefix에 Good Suffix의 부분 문자열 중에서 접미사 형태인 문자열이 있어야만 한다.
 *     B A B C D 가 Good Suffix라고 했을 때, D, (C, D), (B C D), (A B C D)만이 prefix에 있어야 한다는 것이다.
 *
 *     그렇다면 prefix와 suffix가 같은 형태가 아닌 상태로 접두사에 존재하게 되는 경우는 왜 고려하지 않는 걸까?
 *     Good Suffix의 접두사 형태일 경우, 해당 부분 문자열과 일치한다고 할지라도 그 후에 등장하는 문자는 무조건 Good Suffix와 일치하지 않는
 *     문자일 것이다.
 *     접두사, 접미사가 아닌 형태의 부분 문자열의 경우도 마찬가지로 뒤의 문자가 일치하지 않다는 같은 문제가 있다.
 *     즉, 부분 문자열의 뒤에 있는 문자로 인해서 Good suffix와 불일치하게 만들기 때문이다.
 *     또한, 이 경우는 가만히 생각해보면 KMP 알고리즘에서 처리하는 핵심 방법과도 같다라는 것을 알 수 있다.
 *     -------------------------------------------------------------------------------------------------------
 *     case 3. Good Suffix가 Pattern의 다른 곳에 없는 경우
 *     case 1도 2도 아닌 경우에 해당한다.
 *     어떠한 효율적인 방법도 없기 때문에 Good Suffix 이후로 Pattern을 이동한다.
 *     즉 Pattern의 길이만큼 완전히 skip한다는 것과 같다.
 *
 *     ex1)
 *      Text     A B A A B A B A D B   before
 *     Pattern   C A C A B
 *
 *      Text     A B A A B A B A D B   after
 *     Pattern             C A C A B
 *
 *     text의 index 2에서 Bad Character가 발생했다.
 *     good suffix 이후의 문자인 index 5부터 위치할 수 있도록 5칸 만큼 skip을 했다.
 *
 *     case 3에서는 pattern만큼 skip을 한 이유는 Good Suffix Heuristics case 2에서 발견한 이유에서 좀 더 확장된다.
 *     case 2처럼 prefix와 suffix가 같은 형태가 될 수 있도록 good suffix의 부분 집합의 원소 중 하나인 접미사 형태의 부분 문자열이
 *     prefix에 오지 않은 경우라고 생각해보면 이해가 된다.
 *     prefix와 suffix가 같지 않기 때문에 good suffix에 맞춰서 pattern을 이동해서 비교한다고 할지라도 good suffix의 앞뒤로
 *     문자들이 일치하지 않는다.
 *     위의 예시를 들어서 보충 설명을 하면 아래와 같다.
 *
 *     pattern의 index 1에 위치한 A는 good suffix A B의 부분 문자열이기 때문에 good suffix에 맞춰서 pattern을 이동시킨다고 하자.
 *     이동 후 pattern의 index 0과 2에 위치한 C와 비교하게 되는 문자는 text의 index 2에 있는 A와 index 4에 있는 B가 된다.
 *     위에 설명한 이유처럼 good suffix의 부분 문자열이 접두사가 아닌 위치에 존재하더라도 그 문자열의 앞뒤에 있는 문자로 인해서
 *     Good Suffix와의 비교 혹은 Good suffix 앞의 문자와의 비교시 불일치하게 된다.
 *
 *     따라서 case 2처럼 부분 문자열에 맞춰서 pattern을 이동시킬 이유가 전혀 없기 때문에 good suffix 이후로 이동하게 된다.
 *
 *  5. 이 항목은 Good Suffix Heuristics case 1을 보완한 방법인 Strong Good Suffix Rule을 설명한다.
 *     Good Suffix Heuristics case 1의 경우 pattern 내에 다른 곳에 존재할 때 앞에 다른 문자가 있을 수도 있다.
 *     하지만 이는 약한 규칙이라서 반복되는 불필요한 비교를 하게 될 경우가 종종 있을 수가 있다.
 *     이를 좀 더 보완한 방법이 바로 Strong Good Suffix Rule이다.
 *     설명은 아래에서 예시와 함께 진행하겠다.
 *
 *     Before
 *     Index    0  1  2  3  4  5  6  7  8  9  10 11 12 13 14
 *     Text     A  A  B  A  B  A  B  A  C  B  A  C  A  B  B
 *     Pattern  A  A  C  C  A  C  C  A  C
 *
 *     After
 *     Index    0  1  2  3  4  5  6  7  8  9  10 11 12 13 14
 *     Text     A  A  B  A  B  A  B  A  C  B  A  C  A  B  B
 *     Pattern                    A  A  C  C  A  C  C  A  C
 *
 *     Text의 index 6에서 Bad Character가 발생했다. 이 때 Bad Character가 발생한 곳에 대응하는 Pattern의 문자는 C이다.
 *     앞서 살펴본 Good Suffix Heuristics case 1에서의 처리 방식과 달리 하나의 조건을 더 따지는게 특징이다.
 *     Good Suffix와 일치하는 문자열을 Pattern에서 찾고 나서, 이 문자열의 앞 글자가 Bad Character가 발생한 곳에 대응하는
 *     Pattern의 문자와 다른지 검사한 후, 다를 때만 Pattern을 이동한다.
 *     위의 예시로 보면 pattern의 index 4부터 5까지의 문자열 A C는 Good Suffix와 같지만, 앞에 있는 문자인 index 3의 C가
 *     Bad Character가 발생한 곳에 대응하는 pattern의 index 6에 있는 C와 같기 때문에 다른 곳을 검사한다.
 *     이후 Good Suffix와 일치하는 또다른 문자열이 존재하는 index 1부터 2까지의 문자열 A C를 찾아냈고, 그 앞의 문자는 C와 다르기 때문에
 *     이 문자열에 맞춰서 pattern을 이동한다.
 *     그 결과 index 0부터 6으로 이동하여 6만큼 이동하게 된다.
 *
 *     Strong Good Suffix Rule은 검색 속도를 좀 더 높이기 위해서 Good Suffix가 반복되는 구간을 찾아서 검색 속도를 올리되,
 *     같은 불일치 유형의 문자열은 최대한 피하려는 의도로 만들어진 것 같다고 생각한다.
 *
 *  6. 매 번 모든 문자를 확인하지 않고서도 중간에 불일치할 경우 빠르게 검색위치를 조정하면서 진행하므로 시간 복잡도는 평균적으로
 *     O(N)보다 적다고 한다.
 *     최악의 경우는 모든 text의 문자가 pattern과 일치하는 경우이고, 이 때는 시간 복잡도가 O(NM)이다.
 *      N은 자료의 총 길이, M은 Pattern의 길이라고 할 때 text가 aaaaaaaa... 이고 pattern이 aaaa.. 와 같은 형태인 경우가
 *     최악의 경우이다. 즉, 모든 Text에 Pattern을 대조하여 비교하는 경우라고 생각한다.
 *     반면 best case는 pattern의 길이 M만큼 계속 skip하게 될 경우이고, 시간 복잡도는 O(N/M)이 된다.
 *
 *     이러한 특성으로 인해 평균적으로 KMP 알고리즘보다 더 높은 성능을 보인다고 알려져 있다.
 *     단점으로는 이해와 구현이 복잡하고 어렵다는 것인데, 특히 Good Suffix heuristics의 이해와 구현이 꽤나 어렵다고 한다.
 *     이보다 더 향상된 알고리즘으로 'Boyer-Moore-Horspool이라는 알고리즘이 있다.
 *
 *
 *  - 동작 과정 -
 *  1. 접미사 집합을 만들고 이를 토대로 접미사 배열을 생성한다.
 *  2. 검색 원리를 이용하여 접미사 배열을 Binary Search를 통해서 Pattern이 출현하는 시작 위치를 찾는다.
 *
 */
public class BoyerMoore {

    private final int NUMS_OF_EXPANDED_ASCII = 256;

    private void badCharacterHeuristic(int[] badChar, char[] charsOfPattern, int patternLen) {
        /*
         * 문자가 일치하여 이동하지 않는 경우도 있기 때문에 0으로 초기화를 할 수 없다.
         */
        for(int i = 0; i < NUMS_OF_EXPANDED_ASCII; i++) {
            badChar[i] = -1;
        }

        /*
         * 중복되는 문자는 가장 오른쪽에 있는 문자를 기준으로 offset 값이 저장된다.
         * 덮어씌워지는 원리로 말이다.
         */
        for(int i = 0; i < patternLen; i++) {
            badChar[(int) charsOfPattern[i]] = i;
        }
    }

    public void search_badCharacterHeuristic(char[] charsOfText, char[] charsOfPattern) {
        int textLen = charsOfText.length;
        int patternLen = charsOfPattern.length;
        int[] badChar = new int[NUMS_OF_EXPANDED_ASCII];

        // Pattern 전처리
        badCharacterHeuristic(badChar, charsOfPattern, patternLen);
        /*
         * Pattern과 비교할 text의 시작지점 index.
         * 이 index부터 + patternLen만큼의 문자열을 pattern과 비교한다.
         */
        int textIdx = 0;
        // 문자열 검색
        while(textIdx + patternLen <= textLen) { // 양변에 덧셈의 항등원으로 +1씩 더할 경우 이런 조건식이 된다. 양쪽에 +(-1)과 동치.

            int patternIdx = patternLen - 1;
            while(patternIdx >= 0 && charsOfPattern[patternIdx] == charsOfText[textIdx + patternIdx])
                patternIdx--;

            if(patternIdx < 0) {
                // Text에서 Pattern을 발견했을 경우. 모두 같다는 것을 증명하여 idx가 -1을 가리킴.
                if(textIdx + patternLen < textLen) {
                    /*
                     * 비교할 Text가 남아있다면 다음에 비교할 위치로 index를 이동
                     * 새로운 위치에 있는 문자가 Pattern내에 존재하는 Bad Character일 수 있다.
                     * 이 경우에는 시작지점을 앞으로 당겨서 Pattern에 존재하는 Bad Character와 비교하는 지점을 맞춘다.
                     * 만약 pattern내에 존재하지 않는 Bad Character일 경우 시작지점을 다음 문자로 건너뛰게 된다.
                     *
                     * KMP알고리즘과 비슷한 원리라고 생각한다.
                     * 겹치는 문자가 있을 경우, 놓치지 않고 최대한 많이 비교할 수 있도록 하는 것이다.
                     * 토마토마토마... 가 text이고 토마토가 pattern인 경우를 생각해보면 된다.
                     * 어떻게 해야 최대한 토마토 검색어를 정확하게 많이 찾겠는가?
                     */
                    textIdx += patternLen - badChar[charsOfText[textIdx + patternLen]];
                } else {
                    // 모든 Text와 비교했다면 loop를 빠져나오기 위해 +1을 더함.
                    textIdx++;
                }

            } else {
                /*
                 * Text에서 Pattern을 발견하지 못했을 경우. Bad Character가 발생했을 경우에 idx가 0 이상이다.
                 * skip은 Bad Character가 발생한 지점과 Bad Character와 일치하는 Pattern의 문자 중 가장 오른쪽에
                 * 있는 문자가 있는 지점 사이의 거리가 얼마나 되는지를 나타낸다.
                 */
                int skip = patternIdx - badChar[charsOfText[textIdx + patternIdx]];

                if(skip < 0) {
                    /*
                     * Bad Character와 일치하는 pattern의 문자 중 가장 오른쪽에 있는 문자의 위치가
                     * Bad Character가 발생한 위치를 지나쳤을 경우(뒤에 있을 경우) 한칸만 이동한다.
                     *
                     * Bad Character가 발생한 지점을 기준으로 계산하기 때문에 이미 지나쳤을 경우 -값이 나오게 된다.
                     * 이 때 +1을 해주는 것은 Bad Character가 여러 개가 존재하더라도 가장 우측에 있는 문자에 대한 값만을 저장하기 때문에
                     * 나머지 위치에 대한 값을 알 수 없기 때문에 그냥 단순하게 현재 textIdx의 다음 위치로 검색할 위치를 잡는 것이다.
                     */
                    textIdx += 1;
                } else {
                    /*
                     * Bad Character와 일치하는 문자가 pattern내에 존재하지 않거나
                     * Pattern내에 존재하는 상황에서 Bad Character가 발생한 지점이 가장 오른쪽에 있는 문자의 위치를 아직 지나치지 않았을 경우
                     * skip만큼 이동한다. 음수가 아닌 정수 값이 나온다는 것이 그 반증이다.
                     */
                    textIdx += skip;
                }
            }
        }
    }

    /*
     * Good Suffix Heuristic case 1을 해결하기 위한 pre-process method를 오랜 시간에 걸쳐서 분석한 결과를 기록한다.
     * 위에 서술한 개념처럼 case 1을 보완하기 위해서 Strong Good Suffix Rule을 적용하여 처리할 수 있도록 구현했다.
     * 이 알고리즘에서 핵심은 bpos array와 shift array이다. 두 가지를 이해하기 위해서 border를 먼저 알아야 한다.
     *
     * 1. border
     * suffix이면서 동시에 prefix가 될 수 있는 string을 border라고 한다.
     * 테두리, 경계라는 의미처럼 단어의 앞과 뒤에서 같은 문자열이 감싸고 있는 형태라서 그렇다.
     *
     * 2. bpos array
     * 접미사에 위치한 border의 시작 지점 인덱스를 저장하는 배열이 bpos(border position)다.
     * bpos[i]는 pattern의 i번째 index부터 시작하는 접미사에 대해서, 이 문자열(접미사)의 접미사에 위치한 border의
     * 시작 지점 인덱스를 저장한다.
     *
     * 3. shift array
     * good suffix가 pattern 내에 다른 곳에 존재하면서 good suffix의 앞의 문자가 bad character와 다른 경우
     * pattern을 오른쪽으로 얼만큼 이동할 지를 저장하는 배열이다.
     * shift[i]는 pattern의 i-1번째 문자에서 bad character가 발생할 시 pattern을 우측으로 이동할 칸 수를 저장한다.
     * 이는 다르게 생각하면 pattern의 i번째 문자부터 시작하는 good suffix가 pattern 내에 여러 개가 존재할 수 있지만,
     * 가장 가까운 곳에 존재하는 것은 좌측으로 shift[i]만큼 떨어진 곳에 있다는 것을 의미한다.
     * strong good suffix rule을 적용했기 때문에 각각의 good suffix의 앞에 다른 문자가 존재하고 있다.
     *
     * 소스를 자세히 보다보면 KMP 알고리즘에서 pi array를 만드는 과정과 아주 유사하다는 것을 알 수 있다. (사실 똑같다고 보면 된다)
     * bpos라는게 결국 prefix == suffix라는 조건을 만족하는 문자열에 대해서 정보를 저장하는데 pi배열을 생성하는 조건도 같기 때문이다.
     * 이걸 염두하고서 소스를 보면 확실히 이해가 잘 된다.
     */
    public void goodSuffixHeuristic_strongGoodSuffixRule(int[] shift, int[] bpos, char[] charsOfPattern, int patternLen) {
        /*
         * kmp 알고리즘의 i와 j의 관계가 반대로 되어 있다고 생각하면 된다.
         *
         * i는 pattern의 문자를 하나씩 탐색하기 위해서 사용하는 index pointer 역할을 한다.
         * 이는 kmp 알고리즘에서 for문의 i pointer 역할과 같다.
         * kmp 알고리즘에서 for문의 index가 prefix == suffix 정보를 탐색할 때 뒤에서 계속해서 추가되는 문자열을 가리키는 용도인 것처럼,
         * i는 접미사의 앞에서 새롭게 계속해서 추가될 pattern의 문자를 가리킨다.
         *
         * j는 i-1까지의 접미사에서 확인한 border에 i번째 문자가 맨 앞에 추가됐을 때, 그와 비교하기 위한 문자를 가리키는 index pointer 역할을 한다.
         * KMP 알고리즘에서 patternIdx의 역할처럼, 반대편에 위치한 border에서 새롭게 추가되는 문자와 비교하기 위한 문자를 가리키는 pointer 역할과 같다.
         * 새롭게 문자가 앞에 추가될 시 그 문자와 비교할 문자는 접미사에 위치한 border의 시작 문자의 바로 옆에 있는 문자이다.
         * 즉, 소스로 설명하면 i번째 문자는 bpos[i+1]에 있는 문자의 바로 왼쪽에 위치한 문자가 된다.
         * border에 포함되지 않는 문자가 나타나지 않는 border의 초기 데이터 특성 때문에 기본적으로 i와 j는 서로 1만큼 차이가 있다.
         *
         * 예를 들어 설명하면 아래와 같다.
         * 어떤 pattern이 존재하는데 pattern[len-2]와 pattern[len-1]에 위치한 문자가 B B 라는 pattern의 접미사에서의 border를 구하는 상황이라 가정하자.
         * 이 접미사의 border는 B인데 border가 B인지 알기 위해서는 len-2에 위치한 B와 len-1에 위치한 B가 일치하는지 비교해야한다.
         * 앞에 B가 추가되서 B B B 라는 접미사가 됐다면, 이 접미사의 border를 알기 위해서 다음에 비교해야할 문자는 len-3의 B와 len-2의 B가 될 것이다.
         * 각각의 문자가 새롭게 문자가 추가되기 이전의 접미사의 border에서 앞에 추가된 문자이기 때문이다.
         *
         * 이러한 구현 방식으로 인해서 bpos array에는 결국 i번째 인덱스에 j라는 value가 들어가게 된다.
         * border에 새로 추가되는 i pointer가 가리키는 문자와 비교할 문자의 pointer j가 결국 bpos에 들어갈 데이터가 되기 때문이다.
         * 다시 말해 접미사에 위치한 border의 시작 지점 index를 가리키는 pointer가 바로 j이기 때문이다.
         *
         */
        int i = patternLen;
        int j = patternLen + 1;
        bpos[i] = j;

        while(i > 0) {
            /*
             * strong good suffix rule을 적용하여 미리 앞에 있는 문자들을 비교한다.
             * 앞의 문자가 다를 경우에는 case 1의 처리 방법대로 다음에 문자를 비교할 위치를 맞추기 위해서
             * good suffix가 bad character가 발생한 곳 우측에 올 수 있도록 pattern을 이동시켜야 한다.
             * 이동해야하는 칸은 border의 시작 지점끼리의 차가 되므로 j-i가 될 것이다.
             * 이동해야하는 칸을 shift 배열에 저장한다.
             *
             * 한편, shift가 0일 때 한 번만 저장하는 이유는 참고한 글에는 아래와 같이 적혀있다.
             * 동일한 경계를 갖는 접미사에 대해서 s값의 수정을 방지하기 위해 s 값이 0일 때만 값을 수정해준다.
             * 왜 한 번만 저장하도록 했는지에 대해서 고민해봤고 그에 대한 추측은 아래와 같았다.
             * pattern 내에서 good suffix가 여러 개가 존재할 수 있다.
             * 여러 개가 존재하지만 다음에 pattern을 이동시킬 때 기준으로 잡는 것은 good suffix와 최대한 가까운 곳에 위치한 곳이라고 생각한다.
             * 최대한 가까운 곳을 기준으로 탐색해야지만 놓치는 것 없이 꼼꼼하게 pattern과 일치하는 것을 최대한 많이 찾을 수 있기 때문이다.
             * 먼 곳에 위치한 것을 기준으로 shift할 경우, 중간에 일치하는 것을 발견할 가능성을 놓치게 된다고 생각한다.
             * 따라서 shift가 이미 변경됐을 경우, 이후에는 값이 변경하지 않도록 구현을 한 것이라고 생각한다.
             * ------------------------------------------------------------------------------------------------
             * 마지막으로 i와 j pointer가 왜 pattern의 길이에서부터 시작하는지를 설명한다.
             * Strong Good Suffix Rule을 적용하여 구현했기 때문에 i와 j pointer의 앞에 있는 문자를 비교하여 shift 배열의 값을 채우게 된다.
             * 뿐만 아니라 이 비교한 결과를 그대로 사용하여 bpos array의 값을 채우기 때문에, 어떻게 보면 앞선 문자가 위치한 j-1에 대한
             * bpos array의 값을 구하는 셈이 된다.
             * 이로 인해서 (pattern length - 2)와 (pattern length - 1)을 가리키기 위해선 각각에 +1을 해줘야 하고 그 결과
             * i는 (pattern length - 1)이 되고, j는 pattern length이 되어야 한다.
             *
             * 한편, i가 가리키는 문자와 j가 가리키는 문자가 불일치가 발생할 수 있다.
             * 그 경우에는 KMP 알고리즘의 pi 배열 생성과 마찬가지로 메모이제이션을 통한 방법마냥 bpos 배열의 정보를 활용해서
             * i번째 문자 이전까지 일치했던 bpos[i+1]부터 시작하는 border에서 bpos를 찾는 방식으로 새롭게 비교할 문자를 계속해서
             * 찾아나갈 것이다. 즉, 이전 border들의 앞 문자와 새로 추가된 i번째 문자를 계속해서 비교한다는 말이다.
             * 그런데 계속해서 불일치할 경우 최후에는 가장 오른쪽에 있는 문자, (pattern length - 1)에 있는 문자와 비교를 해야 된다.
             * 그러기 위해선 border가 없을 경우에는 bpos가 (pattern length)의 값을 저장하도록 설계해야만 하고,
             * 그러기 위해선 위에서 내린 결론인 i는 (pattern length - 1), j는 pattern length 에다가 또다시 +1일 해줘야 한다.
             *
             * 그리하여 최종적으로 i와 j는 각각 pattern length와 pattern length + 1로 초기화하고 전처리 로직을 실행하게 된다는
             * 결론에 도달한다.
             *
             */
            while(j <= patternLen && charsOfPattern[i-1] != charsOfPattern[j-1]) {
                if(shift[j] == 0) {
                    shift[j] = j-i;
                }
                j = bpos[j];
            }

            /*
             * strong good suffix rule을 적용하여 미리 앞에 있는 문자들을 비교한다.
             * 앞에 문자가 같으면 border의 길이는 점차 늘어나기 때문에 i와 j를 --하기 전에 bpos에 데이터를 저장한다.
             */
            i--;
            j--;
            bpos[i] = j;
        }
    }

    /*
     * Good Suffix Heuristic case 2를 해결하기 위한 pre-process method이다.
     * 위에 있는 method의 주석을 모두 이해하고 있어야 이 pre-process의 목적과 구현 방식을 이해할 수 있다.
     *
     * case 2는 good suffix의 접미사 형태의 부분 문자열이 prefix에 존재하고 있는 case이다.
     * 접두사에 위치한 good suffix의 일부를 good suffix 부분에 맞춰서 pattern을 이동시켜야 하므로
     * pattern의 첫 번째 문자를 bpos[0]만큼 이동시켜야 한다.
     * 근데 희한하게 로직을 살펴보면 good suffix의 시작 지점(bpos[0]의 value)을 포함해서 이전의 문자들은(i <= bpos[0])
     * shift array의 값이 초기화 상태인 0일 경우에는 bpos[0]을 그대로 대입하게 되어있다.
     * 이렇게 하는 이유는 case 1을 제외하고 다음 검색 위치를 보다 효과적으로 찾기 위한 최선의 방법은 오로지 접두사를
     * good suffix에 맞추는 것이기 때문이다.
     * good suffix와 같지 않거나 case 2처럼 접미사 형태의 부분 문자열이 접두사에 있지 않는다면, 위에 개념을 설명할 때 처럼
     * pattern을 다음 검색에 유리하도록 효과적으로 조정할 방법이 없다.
     * 그렇기 때문에 good suffix의 시작 지점까지는 shift[i]에 bpos[0]을 저장하는 이유다. 그만큼 점프하는게 가장 최선이라서.
     *
     * 그런데 bpos[0]을 넘어서는 왜 bpos[j]를 다시 대입해서 작업을 하는 것인가?
     * bpos[0] 이후로는 good suffix가 아예 다른 형태가 되기 때문에, bpos[0]에서의 bpos를 기준으로 다시 shift array에
     * value를 저장하는 것이다.
     * 이 때 bpos가 pattern length 값을 저장하고 있을 경우, 이것이 의미하는 것은 good suffix heuristic case 3인 경우라서
     * good suffix 이후로 pattern을 이동시켜야 한다는 것을 의미한다.
     * 왜냐하면 bpos가 없다는 말은(pattern을 벗어난 인덱스니까 사실상 없다는 표현이다 이건) pattern을 최적으로 옮겨줄 기준
     * 자체가 없기에 pattern의 길이만큼 옮겨서 아예 새로운 환경에서 다시 재탐색을 하는게 낫기 때문이다.
     *
     * 따라서 사실상 이 전처리 method를 통해서 case 2뿐만 아니라 case 3까지도 처리할 수 있다고 생각한다.
     */
    public void goodSuffixHeuristic_case2(int[] shift, int[] bpos, char[] charsOfPattern, int patternLen) {
        int i, j;
        j = bpos[0];
        for(i = 0; i <= patternLen; i++) {
            if(shift[i] == 0) {
                shift[i] = j;
            }

            if(i == j) {
                j = bpos[j];
            }
        }
    }

    /*
     * 용어 설명을 다시 반복해서 설명한다.
     *
     * 1. border
     * suffix이면서 동시에 prefix가 될 수 있는 string을 border라고 한다.
     * 테두리, 경계라는 의미처럼 단어의 앞과 뒤에서 같은 문자열이 감싸고 있는 형태라서 그렇다.
     *
     * 2. bpos array
     * 접미사에 위치한 border의 시작 지점 인덱스를 저장하는 배열이 bpos(border position)다.
     * bpos[i]는 pattern의 i번째 index부터 시작하는 접미사에 대해서, 이 문자열(접미사)의 접미사에 위치한 border의
     * 시작 지점 인덱스를 저장한다.
     *
     * 3. shift array
     * good suffix가 pattern 내에 다른 곳에 존재하면서 good suffix의 앞의 문자가 bad character와 다른 경우
     * pattern을 오른쪽으로 얼만큼 이동할 지를 저장하는 배열이다.
     * shift[i]는 pattern의 i-1번째 문자에서 bad character가 발생할 시 pattern을 우측으로 이동할 칸 수를 저장한다.
     * 이는 다르게 생각하면 pattern의 i번째 문자부터 시작하는 good suffix가 pattern 내에 여러 개가 존재할 수 있지만,
     * 가장 가까운 곳에 존재하는 것은 좌측으로 shift[i]만큼 떨어진 곳에 있다는 것을 의미한다.
     * strong good suffix rule을 적용했기 때문에 각각의 good suffix의 앞에 다른 문자가 존재하고 있다.
     *
     */
    public void search_goodSuffixHeuristic(char[] charsOfText, char[] charsOfPattern) {
        int patternLen = charsOfPattern.length;
        int textLen = charsOfText.length;

        /*
         * Pattern과 비교할 text의 시작지점 index.
         * 이 index부터 + patternLen만큼의 문자열을 pattern과 비교한다.
         */
        int textIdx = 0;
        int[] bpos = new int[patternLen + 1];
        int[] shift = new int[patternLen + 1];

        // preprocess
        goodSuffixHeuristic_strongGoodSuffixRule(shift, bpos, charsOfPattern, patternLen);
        goodSuffixHeuristic_case2(shift, bpos, charsOfPattern, patternLen);

        // core logic
        while(textIdx < textLen - patternLen) {
            // pattern의 가장 끝 index로 초기화
            int j = patternLen - 1;

            while(j >= 0 && charsOfPattern[j] == charsOfText[textIdx + j])
                textIdx--;

            if(j < 0) {
                /*
                 * pattern을 모두 발견했을 경우
                 * j가 -1인 경우는 text와 pattern의 비교한 결과 문자열이 모두 일치한 경우이다.
                 * pattern을 다음 검색을 하기 위한 위치도 이동시킨다.
                 * 최대한 많은 가능성을 염두하고 탐색해야 하므로 shift만큼 이동시킨다.
                 */
                textIdx += shift[0];
            } else {
                /*
                 * Bad Character가 발생한 경우이다.
                 * good suffix heuristic대로 처리하므로 shift만큼 pattern을 이동시킨 다음 다시 비교한다.
                 * 불일치가 발생하기 이전 문자를 기준으로 shift 해야한다.
                 */
                textIdx += shift[j + 1];
            }
        }

    }

}
