package algorithm.lis;

/*
 * Longest Increasing Subsequence(최장 증가 부분 수열)
 * 원소가 n개인 배열의 일부 원소를 골라내서 만든 부분 수열 중, 각 원소가 이전 원소보다 크다는 조건을 만족하고,
 * 그 길이가 최대인 부분 수열을 '최장 증가 부분 수열'이라고 한다.
 *
 *  - LIS 길이를 구하는 방법 -
 *  LIS 길이를 구하는 방법은 두 가지가 있다.
 *  1. DP를 이용한 방법
 *  DP를 이용하게 될 경우 간편하지만, 시간 복잡도가 O(n^2)가 되기 때문에 좋은 방법은 아니다.
 *
 *  2. 이분탐색(Binary Search)를 활용한 방법
 *  이분탐색은 일반적으로 시간 복잡도가 O(logn)으로 알려져 있기 때문에, 배열의 길이만큼만 돌면서 이분탐색을 하면 되므로
 *  시간 복잡도가 O(nlogn)이 된다. DP보다 훨씬 빠르다.
 */
public class LongestIncreasingSubsequence {

    public int getLengthByDp(int[] arr) {
        int length = arr.length;
        int[] dp = new int[length];

        for (int k = 0; k < length; k++){
            dp[k] = 1;
            for (int i = 0; i < k; i++){
                if(arr[i] < arr[k]){
                    dp[k] = Math.max(dp[k], dp[i] + 1);
                }
            }
        }

        return dp[length - 1];
    }

    public int getLengthByBinarySearch(int[] arr) {
        int length = arr.length;
        int[] LIS = new int[length];

        LIS[0] = arr[0];
        int j = 0;
        int i = 1;

        while(i < length) {
            if(LIS[j] < arr[i]) {
                LIS[j + 1] = arr[i];
                j += 1;
            } else {
                int index = binarySearch(arr, 0, j, arr[i]);
                LIS[index] = arr[i];
            }

            i += 1;
        }

        return j + 1;
    }

    /**
     * binary search의 경우 일치하는 값을 어디서 찾느냐를 정함에 따라서 알고리즘이 약간 달라질 수 있다.
     * right를 반환하는 이유는 target <= arr[mid] 인 구조를 해야지만 일치하는 값이 있을 때 제대로 반환할 수 있다.
     *
     * @param arr
     * @param left
     * @param right
     * @param target
     * @return
     */
    private int binarySearch(int[] arr, int left, int right, int target) {
        int mid;

        while(left < right) {
            mid = (left + right) / 2;

            if(arr[mid] < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        return right;
    }

}
