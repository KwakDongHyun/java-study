package algorithm.tree.mst;

import java.util.List;

public class Kruskal {

    private int vertex;
    private int[] parent;
    private List<List<Integer>> edges;

    class KruskalNode {
        
    }

    private int findRoot(int x) {
        /*
         * x가 root라면 반환한다.
         * root의 부모는 자기자신을 가리키도록 설계한다.
         */
        if(x == parent[x]) {
            return x;
        }

        return findRoot(parent[x]);
    }

    /*
     * 자식이 1개뿐인 직선 형태의 트리인 경우에 대해서도 안정적인 성능을 제공하기 위해서 나온 개선안
     */
    private int findRootByPathCompression(int x) {
        /*
         * x가 root라면 반환한다.
         * root의 부모는 자기자신을 가리키도록 설계한다.
         */
        if(x == parent[x]) {
            return x;
        }

        /*
         * 이렇게 하면 x부터 root까지 부모와 자식 관계로 연결된 모든 노드들의 parent는
         * root 노드를 가리키게 된다.
         */
        return parent[x] = findRoot(parent[x]);
    }

    public void unionRoot(int x, int y) {
        /*
         * x, y 정점의 root를 찾는다. (x와 y가 속한 트리의 root node를 찾는다.)
         */
        x = findRoot(x);
        y = findRoot(y);

        if(x != y) {
            parent[x] = y;
        }
    }

    /*
     * find의 시간복잡도는 트리의 높이에 의해서 결정된다.
     * 따라서 union 연산을 할 때 높이를 줄이기 위해서, 높이가 낮은 트리를 높이가 높은 트리에 붙인다.
     * 이렇게 하면 union 이후에 트리의 높이는 높이가 높은 트리의 높이 값을 그대로 유지할 수 있게된다.
     */
    public void unionRootByRank(int x, int y) {
        /*
         * x, y 정점의 root를 찾는다. (x와 y가 속한 트리의 root node를 찾는다.)
         */
        x = findRoot(x);
        y = findRoot(y);

        if(x != y) {
            if(findRank(x) < findRank(y)) {
                parent[x] = y;
            } else {
                parent[y] = x;
            }

            /*
             * 두 트리의 높이가 같으면 루트 노드에 다른 트리의 루트 노드를 붙이는 형태가 되기 때문에
             * 다른 트리를 흡수하는 트리의 입장에서는 높이가 1 증가하는 형태가 된다.
             */
            if(findRank(x) == findRank(y)) {
                parent[x] = y;
            }
        }
    }

    /*
     * rank를 반환해주는 method가 있다고 가정함. (컴파일 오류 방지)
     */
    private int findRank(int x) {
        return 0;
    }

    private List<List<Integer>> kruskal() {
        List<List<Integer>> mst;

        for(int i = 0; i < edges.size(); i++) {

        }

    }

}
