package sort;

/**
 *  Merge Sort
 *  문제를 분할하고, 분할한 문제를 정복하여 합치는 정렬 방법
 *  분할 정복(Divide and Conquer) 알고리즘을 기반으로 하는 정렬 방식
 *
 *  - 추가 설명 -
 *  쉽게 말해 정렬해야 할 리스트가 주어지면 해당 리스트를 분할을 반복하여 최대한 작게 쪼개진 시점에 부분리스트에서 인접한 원소들끼리 비교하여 정렬하는 방식인 것이다.
 *  여기서 주의할 점은 합병정렬의 구현이 반드시 2개의 부분리스트로 나누어야 한다는 점은 아니다.
 *  어디까지나 가장 일반적으로 구현되는 방식이 절반으로 나누는 방식일 뿐이다.
 *  두 개의 부분리스트로 나누는 방식을 two-way 방식이라고 하니 참고하시면 좋을 듯 하다.
 *
 *  일단 우리가 이해하고 있어야 할 점은 각각의 부분리스트는 '정렬된 상태'라는 점이다.
 *  정렬된 부분리스트를 Merge 할떄는 각각의 리스트에 인덱스를 두고 탐색하면서, 순차적으로 값을 새로이 병합할 리스트에 넣는다는 거다.
 *  예시는 아래와 같다.
 *
 *  ex)
 *  2 4 6 8   3 5 7 9
 *  ^         ^
 *  2
 *
 *  2 4 6 8   3 5 7 9
 *    ^       ^
 *  2 3
 * 이런식으로 말이다.
 *
 * 1. 주어진 리스트를 절반으로 분할하여 부분리스트로 나눈다. (Divide : 분할)
 * 2. 해당 부분리스트의 길이가 1이 아니라면 1번 과정을 되풀이한다.
 * 3. 인접한 부분리스트끼리 정렬하여 합친다. (Conqure : 정복)
 *
 */
public class MergeSort {

    public static void main(String[] args) {
        int[] data = {8, 2, 6, 4, 7, 3, 9, 5, 1};

        // Merge 하는 과정에서 sort하여 element를 담을 임시배열
//        int[] sorted = new int[data.length];
        int startIdx = 0;
        int endIdx = data.length-1;

        /*
         * 1 - 2 - 4 - 8 - ... 식으로 1부터 서브리스트를 나누는 기준을 두 배씩 늘린다.
         * 기본 개념이 절반씩 나누어서 원소가 1개 남을때가지 divide 하는거였다.
         *
         * Top-Down 방식의 입장에서는 재귀로 아래와 같이 진행하면 된다.
         * 1. 리스트 길이를 절반씩 쪼개면서 원소가 1개 남을때가지 divide
         * 2. divide 된 것들을 정렬해서 임시배열에 담아두고, 이를 원본배열에 복사해서 붙여넣는다.
         * 3. 어차피 끝까지 다 병합하면 원본배열은 정렬된 상태로 나오니까 2번은 신경 안써도 된다.
         *
         * Bottom-Up에서는 이를 반대로 봐야한다.
         * 1개씩부터 해서 2, 4, 8 식으로 2의 배수형태로 올라가는 시각으로 봐야한다.
         * 1. 탑다운의 1번을 for 문을 돌면서 size 단위로 배열의 일부분을 탐색하고 정렬한다.
         * 2. 정렬한 값들을 임시배열에 담아두고, 이를 원본배열에 복사해서 붙여넣는다.
         *
         * 참고라 아래 for 문에서는 배열을 다루는 것이라서 실제 index에 맞춰서 계산을 하는거 같다.
         */
        for(int divideSize = 1; divideSize <= endIdx; divideSize = (2 * divideSize)) {
            /*
             * 두 부분리스트을 순서대로 병합해준다.
             * 예로들어 현재 부분리스트의 크기가 1(size=1)일 때
             * 왼쪽 부분리스트(low ~ mid)와 오른쪽 부분리스트(mid + 1 ~ high)를 생각하면
             * 왼쪽 부분리스트는 low = mid = 0 이고,
             * 오른쪽 부분리스트는 mid + 1부터 low + (2 * size) - 1 = 1 이 된다.
             *
             * 이 때 high가 배열의 인덱스를 넘어갈 수 있으므로 right와 둘 중 작은 값이
             * 병합되도록 해야한다.
             */
            System.out.println("=============================");
            System.out.println("divideSize : " + divideSize);

            for(int left = 0; left <= endIdx - divideSize; left += (2 * divideSize)) {
                /* - 개인적인 분석과 추가 설명 -
                 * 나누어지는 size에 맞춰서 다음 부분리스트의 인덱스 지점도 맞추어서 계산이 되어야 한다.
                 * 예를 들어 size가 1이고 시작지점이 0이니까 size가 1인 것끼리 비교하면,
                 * 다음에 서로 비교하기 위한 인덱스 시작지점은 +2인 2가 된다.
                 * size가 2인 경우 (0,1) (2,3) 이니까 4부터 시작지점이 되는거다.
                 *
                 * 주의할 점은, rightIdx 계산을 하다보면 실제 data array의 길이를 넘어설 수 있기 때문에 넘지 않게 해줘야한다.
                 * 길이를 넘어설 경우 실제 길이까지만 담기도록 해야한다.
                 *
                 */
                int leftIdx = left;                                             // 배열의 시작지점
                int midIdx = left + divideSize - 1;                             // 배열의 중간지점. -1을 안하면 우측 부분리스트의 시작 index이다.
                int rightIdx = Math.min(left + (2 * divideSize) - 1, endIdx);   // 배열의 끝지점
                merge(data, leftIdx, midIdx, rightIdx);

                System.out.println("--------------------------------");
                System.out.print("{");
                for(int k = 0; k < data.length; k++) {
                    System.out.print(data[k] + " ,");
                }
                System.out.println("}");
            }
        }

    }

    /**
     * 합칠 부분리스트는 arr배열(param)의 leftIdx ~ rightIdx 까지이다.
     *
     * @param arr       정렬할 배열
     * @param leftIdx   배열의 시작지점 Index
     * @param midIdx    배열의 중간지점 Index
     * @param rightIdx  배열의 끝지점 Index
     */
    public static void merge(int[] arr, int leftIdx, int midIdx, int rightIdx) {
        int[] sorted = new int[arr.length];  // Merge 하는 과정에서 sort하여 element를 담을 임시배열

        int leftSubIdx = leftIdx;       // 좌측 부분리스트 탐색 Index. 초기 시작지점 세팅
        int rightSubIdx = midIdx + 1;   // 우측 부분리스트 탐색 Index. 초기 시작지점 세팅
        int sortedIdx = leftIdx;        // sorted 배열의 Index

        // 맨 위의 Merge Sort 설명 구간의 예시에서 보여주는 과정이 여기서 실질적으로 진행된다.
        while(leftSubIdx <= midIdx && rightSubIdx <= rightIdx) {
            // 부분리스트의 탐색 index가 어느 것 하나라도 끝지점에 도달하지 못했을 때에만 탐색 진행.

            if(arr[leftSubIdx] <= arr[rightSubIdx]) {
                sorted[sortedIdx] = arr[leftSubIdx];
                sortedIdx++;
                leftSubIdx++;
            } else {
                sorted[sortedIdx] = arr[rightSubIdx];
                sortedIdx++;
                rightSubIdx++;
            }
        }

        /*
         * 좌축 부분리스트가 먼저 탐색이 끝날 경우 (leftSubIdx > midIdx)
         * 우측 부분리스트에 남은 원소들을 다 넣어줘야한다.
         */
        if(leftSubIdx > midIdx) {
            while(rightSubIdx <= rightIdx) {
                sorted[sortedIdx] = arr[rightSubIdx];
                sortedIdx++;
                rightSubIdx++;
            }
        }

        /*
         * 반대인 경우도 마찬가지.
         * 우축 부분리스트가 먼저 탐색이 끝날 경우 (rightSubIdx > rightIdx)
         * 좌측 부분리스트에 남은 원소들을 다 넣어줘야한다.
         */
        if(rightSubIdx > rightIdx) {
            while(leftSubIdx <= midIdx) {
                sorted[sortedIdx] = arr[leftSubIdx];
                sortedIdx++;
                leftSubIdx++;
            }
        }

        /*
         * 정렬되서 들어있는 임시배열에 있는 값들을 원본 배열에 복사해서 옮겨준다.
         */
        for(int i = leftIdx; i <= rightIdx; i++) {
            arr[i] = sorted[i];
        }
    }

}
