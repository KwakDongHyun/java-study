package sort;

/**
 * Tree 부분의 Heap 구조를 사용하지 않고, 입력된 배열을 우선순위 큐처럼
 * Max Heap 또는 Min Heap으로 만든 다음, 하나씩 데이터를 poll하여 정렬하는 방법이다.
 *
 * 힙의 개념이 적용된 것이지만, 힙 자료구조를 만들어서 사용하는 방식이 아니다보니
 * 메모리 절약, 빠른 시간 안에 적은 양의 코드로 정렬 효과를 볼 수 있다는 것이 장점.
 * 하지만 거대한 시스템이나 서비스 운영에는 제대로 된 자료구조를 만들고 사용하는 것이
 * 훨씬 장점이 크니까 절대적으로 뭐가 낫다고 말할 수는 없다고 본다.
 * 고로 이 방법은 메모리나 시간이 제약될 때, 또는 정렬만 빠르게 하고 싶은 경우 추천.
 *
 * 아래는 최대힙을 구현하여, 오름차순으로 정렬하는 경우를 상정하여 구현했다.
 * 원리는 poll을 한 후, 새로운 배열에 차례대로 데이터를 넣는 것이 아니라 배열의 특성을 이용해서
 * swap을 통해서 max 데이터를 뒤에서부터 채워넣고, 정렬시점을 뒤에서부터 -1씩 줄여가면서 하는 방식이다.
 *
 * 일반적으로 Max Heap - 내림차순, Min Heap - 오름차순 이지만
 * 요점은 Heap을 만들고 전부 poll해서 정렬을 하는 것이지, 위처럼 맞춰서 하는건 아니다.
 */
public class HeapSort {

    /**
     * 입력된 배열은 0부터 채워가므로, 아래와 같은 수식이 된다.
     * @param child
     * @return 부모 인덱스
     */
    private static int getParent(int child) {
        return (child - 1) / 2;
    }

    // 두 인덱스의 원소를 교환
    private static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    /**
     * @param a         입력받은 배열
     * @param lastIdx   마지막 노드
     */
    private static void heapify(int[] a, int parentIdx, int lastIdx) {

        int leftChildIdx;
        int rightChildIdx;
        int largestIdx;

        /*
         * parentIdx에는 swap이 일어날 경우 자식노드의 idx가 대입된다.
         * 이럴 경우 leaf 노드에 도달했을 때 빠져나오기 위한 검토를 해야하는데 단순히 parentIdx <= lastIdx를 하면
         * Full Binary Tree일 경우(모든 노드가 0개 또는 2개의 자식노드를 가짐) swap은 왼쪽자식 노드와 일어났는데
         * 인덱스로 보면 오른쪽 자식노드 인덱스보다 작아서 while이 또 돌게 된다.
         *
         * 그렇기 때문에 leaf노드의 정의를 이용하여, 조건값으로 parentIdx대신 왼쪽 자식노드의 인덱스를 잡는거다.
         * 오른쪽 자식노드는 없을 경우도 있기 때문에 왼쪽을 기준으로 비교한다.
         */
        while((2 * parentIdx + 1) <= lastIdx) {

            /*
             * 현재 트리에서 부모 노드의 자식노드 인덱스를 각각 구한다.
             * 현재 부모 인덱스를 가장 큰 값을 갖고있다고 가정한다.
             */
            leftChildIdx = 2 * parentIdx + 1;
            rightChildIdx = 2 * parentIdx + 2;
            largestIdx = parentIdx;


//            /*
//             * left child node와 비교
//             *
//             * 자식노드 인덱스가 배열 끝 원소의 인덱스보다 크지 않으면서
//             * 현재 가장 큰 인덱스보다 왼쪽 자식노드의 값이 더 클 경우
//             * 가장 큰 인덱스를 가리키는 largestIdx를 왼쪽 자식노드 인덱스로 바꿔준다.
//             */
//            if(leftChildIdx < lastIdx && a[largestIdx] < a[leftChildIdx]) {
//                largestIdx = leftChildIdx;
//            }

            if(leftChildIdx <= lastIdx && a[largestIdx] < a[leftChildIdx]) {
                largestIdx = leftChildIdx;
            }

//            /*
//             * right child node와 비교
//             *
//             * 부모와 양쪽 자식 모두와 비교하여 가장 큰 값을 도출하기 위함.
//             * 위처럼 오른쪽 자식노드에도 동일하게 진행
//             */
//            if(rightChildIdx < lastIdx && a[largestIdx] < a[rightChildIdx]) {
//                largestIdx = rightChildIdx;
//            }

            /* leaf노드가 왼쪽 자식만 가질 경우에는 indexOutOfBound 에러가 난다.
             * 우측 노드 자식이 없는데 값을 참조하려니까 인덱스 범위를 벗어나기 때문.
             * 그래서 마지막 인덱스보다 작은지 검사해야함.
             */
            if(rightChildIdx <= lastIdx && a[largestIdx] < a[rightChildIdx]) {
                largestIdx = rightChildIdx;
            }


            /*
             * largestIdx와 parentIdx가 같지 않다는 것은
             * 위 자식노드와 비교하는 과정에서 현재 부모노드보다 큰 노드가 존재한다는 의미이다.
             * 그럴 경우 해당 자식노드와 부모노드를 swap한다.
             *
             * 재귀방식은 swap이 일어나면 일어난 인덱스 지점을 부모로한 하위 sub tree를 heap상태인지 검토를 해야하는데
             * 이걸 재귀로 해결했었다.
             * 재귀로 할 때는 parameter로 parentIdx를 넣어줬으니 반복문에서는 아예 parentIdx 변수에
             * 값을 대입하면 된다.
             */
            if(largestIdx != parentIdx) {
                swap(a, largestIdx, parentIdx);
//                heapify(a, largestIdx, lastIdx);	// Top-Down 재귀 호출이 발생
                parentIdx = largestIdx;
            } else {
                return;
            }

        }

    }

    public static void heapSort(int[] arr) {
        int size = arr.length;

        /*
         * 부모노드와 heapify 과정에서 음수가 발생하면 잘못된 참조가 발생하기 때문에
         * 원소가 1개이거나 0개일 경우는 정렬할 필요가 없으므로 함수 종료.
         */
        if(size < 2) {
            return;
        }

        // 가장 마지막 노드의 부모 노드 인덱스
        int parentIdx = getParent(size - 1);

        // max heap 만들기
        for(int i = parentIdx; i >= 0; i--) {
            // 부모노드(i)를 -1씩 줄이면서 subTree를 차례대로 max heap으로 만들어간다.
            // leaf노드의 부모노드부터 root로 올라가면서 heap으로 재구성 하는거다.
            heapify(arr, i, size - 1);
        }

        /*
         * 정렬과정
         * poll해서 실제 데이터를 빼는 부분만 생략되어 있음.
         * 개념으로는 poll해서 루트노드를 삭제하면, 트리구조를 유지시키기 위해서 last node를 root 노드에 넣는다.
         * 그리고 나서 다시 max heap으로 만들기 위해서 heap을 탐색한다.
         * 루트 아래의 데이터는 이미 Max heap 상태로 정렬되어 있기 때문에 최초로 힙을 만들때처럼 leaf 노드의
         * 부모노드부터 탐색할 필요는 없고, 재귀방식일 때는 루트에서 한번만 확인하면 알아서 내려가면서 heap으로 다시
         * 만들어준다.
         */
        for(int i = size - 1; i > 0; i--) {
            swap(arr, 0, i);
            heapify(arr, 0,i - 1);
        }
    }

}

// 테스트용 클래스
class main {
    public static void main(String[] args) {
        int[] arr = {3, 7, 5, 4, 2, 8};
        System.out.print(" 정렬 전 original 배열 : ");
        for (int val : arr) {
            System.out.print(val + " ");
        }

        System.out.println();

        // 힙정렬 후
        HeapSort.heapSort(arr);
        System.out.print("\n 정렬 후 배열 : ");
        for(int val : arr) {
            System.out.print(val + " ");
        }

    }
}
