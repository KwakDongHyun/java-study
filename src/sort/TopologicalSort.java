package sort;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *  Topological Sort
 *  정렬 알고리즘의 일종으로, 순서가 정해져 있는 일련의 작업을 차례대로 수행해야 할 때 사용하는 알고리즘이다.
 *  예를 들어, 대학교 선수 과목 수강 같은 것이 있다. (필수 전공을 이수해야 특정 전공 과목을 이수할 수 있는 것)
 *
 *  - 기본 개념 -
 *  1. 사이클이 없는 방향 그래프(Direct Acyclic Graph)에 대해서만 수행할 수 있다.
 *  2. 위상 정렬에는 여러 가지 답이 존재할 수 있다.
 *  3. 모든 원소를 방문하기 전에 큐가 비게 된다면 사이클이 존재한다고 판단할 수 있다.
 *  4. 보통 큐로 구현하나, 스택을 이용한 DFS를 이용해서 위상 정렬을 구현할 수도 있다.
 *  5. 시간 복잡도는 O(V+E)이다. vertex 개수만큼 순회하고서 edge를 제거하기 때문.
 *  6. 위상 정렬을 알기 위해서는 먼저 진입차수(Indegree)와 진출차수(Outdegree)를 알고 있어야 한다.
 *     Indegree : 특정한 노드로 들어오는 간선의 개수
 *     Outdegree : 특정한 노드에서 나가는 간선의 개수
 *
 *  - 동작 과정 -
 *  1. 진입차수가 0인 노드를 큐에 넣는다.
 *  2. 큐가 빌 때까지 다음의 과정을 반복한다.
 *  2-1. 큐에서 원소를 꺼내 해당 노드에서 나가는 간선을 그래프에서 제거
 *  2-2. 새롭게 진입차수가 0이 된 노드를 큐에 삽입
 *
 */
public class TopologicalSort {

    /**
     * parameter 구성은 아래와 같다.
     * 1. index(vertex) 마다 indegree 정보를 저장해둔 배열
     * 2. 연결리스트로 표현한 그래프
     *
     * @param indegrees
     * @param graph
     * @return List<Integer>
     */
    public List<Integer> topologicalSort(int[] indegrees, List<List<Integer>> graph) {
        Queue<Integer> queue = new LinkedList<Integer>();
        List<Integer> result = new LinkedList<Integer>();

        /*
         * 정렬 전 준비과정
         * 진입차수가 0인 노드를 넣고 시작.
         */
        for(int i = 1; i < indegrees.length; i++) {
            if(indegrees[i] == 0) {
                queue.offer(i);
            }
        }

        int vertex;
        List<Integer> linkedVertexs;
        while(!queue.isEmpty()) {
            vertex = queue.poll();
            result.add(vertex);

            linkedVertexs = graph.get(vertex);
            for(Integer linkedVertex : linkedVertexs) {
                indegrees[linkedVertex]--;

                if(indegrees[linkedVertex] == 0) {
                    queue.offer(linkedVertex);
                }
            }
        }

        return result;
    }

}
