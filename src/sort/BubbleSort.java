package sort;

/**
 *  Bubble Sort
 *  정렬 과정에서 원소의 이동이 마치 거품이 수면위로 올라오는 것 같다고 해서 거품(Bubble)이다.
 *
 *  -기본 개념-
 *  1. 앞에서부터 현재 원소와 바로 다음의 원소를 비교한다.
 *  2. 현재 원소가 다음 원소보다 크면 원소를 교환한다.
 *  3. 다음 원소로 이동하여 해당 원소와 그 다음원소를 비교한다.
 *
 * 1. 각 루프마다
 *  - n번째 항과 n+1번째 항의 크기를 비교한다
 *  - 이전 항이 이후 항보다 크면 교환한다.
 *  - 우측부터 가장 큰 값이 쌓여간다.
 *  - 이중 반목문으로 구현해야 한다.
 *
 * 2. 모든 원소를 다 돌때까지 한다.
 */
public class BubbleSort {

    public static void main(String[] args) {
        int[] input = {5, 16, 21, 8, 7, 23, 4, 1};

        for(int i = 1; i < input.length; i++) {
            for(int j = 0; j < input.length-i; j++) {
                if(input[j] > input[j+1]) {
                    int temp = input[j];
                    input[j] = input[j+1];
                    input[j+1] = temp;
                }
            }

            System.out.println("======================");
            System.out.print("{");
            for(int k = 0; k < input.length; k++) {
                System.out.print(input[k] + " ");
            }
            System.out.println("}");
        }


    }

}
