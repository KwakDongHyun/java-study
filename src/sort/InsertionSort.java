package sort;

/**
 *  Insertion Sort
 *  현재 비교하고자 하는 target(타겟)과 그 이전의 원소들과 비교하며 자리를 교환(swap)하는 정렬 방법
 *
 *  - 추가 설명 -
 *  개념상 올바른 위치로 끼워넣는 것이지만, 프로그램 차원에서는 넣는 위치부터 타겟위치까지 값을 뒤로 밀어줘야한다.
 *  이 작업을 건너뛰게 도와주는게 뒤에서부터 비교하면서 스왑하는 방식이다.
 *  뒤에서부터 비교하면서 스왑하기 때문에 자동으로 정렬이 되기 때문에 불필요한 작업을 줄일 수 있다.
 *
 * 1. 현재 타겟이 되는 숫자와 이전 위치에 있는 원소들을 비교한다. (첫 번째 타겟은 두 번째 원소부터 시작한다.)
 * 2. 타겟이 되는 숫자가 이전 위치에 있던 원소보다 작다면 위치를 서로 교환한다.
 * 3. 그 다음 타겟을 찾아 위와 같은 방법으로 반복한다.
 *
 */
public class InsertionSort {

    public static void main(String[] args) {
        int[] data = {7, 3, 2, 8, 9, 4, 6, 1, 5};

        for(int i = 1; i < data.length; i++) {
            for(int j = i; j >= 1; j--) {
                if(data[j-1] > data[j]) {
                    int temp = data[j-1];
                    data[j-1] = data[j];
                    data[j] = temp;
                }
            }

            System.out.println("=============================");
            System.out.print("{");
            for(int k = 0; k < data.length; k++) {
                System.out.print(data[k] + " ,");
            }
            System.out.println("}");
        }

        System.out.println("=============================");
        System.out.println("End");
        System.out.print("{");
        for(int i = 0; i < data.length; i++) {
            System.out.print(data[i] + " ,");
        }
        System.out.print("}");

    }

}
