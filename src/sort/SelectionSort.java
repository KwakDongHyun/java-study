package sort;

/**
 *  Selection Sort
 *  선택 정렬은 말 그대로 현재 위치에 들어갈 데이터를 찾아 선택하는 알고리즘이다.
 *
 * 1. 각 루프마다
 *  - 최대 원소를 찾는다
 *  - 최대 원소와 맨 오른쪽 원소를 교환한다.
 *  - 맨 오른쪽 원소를 제외한다.
 *
 * 2. 하나의 원소만 남을 때까지 위의 루프를 반복한다.
 */
public class SelectionSort {

    public static void main(String[] args) {

        int[] data = {5, 6, 2, 8, 7, 23, 4, 1};
        int max;
        int index;
        // 총 탐색 회차는 (리스트 길이 - 1)만큼만 하면 된다.
        for(int i = 0; i < data.length; i++) {
            max = data[0];  // 초기값
            index = 0;
            // 우측에서부터 정렬되기 때문에 긱 회차마다 탐색 범위가 -1씩 줄어든다.
            for(int j = 1; j < data.length-i; j++) {
                // 최대값을 찾으면 해당 위치를 기억.
                if(max < data[j]) {
                    max = data[j];
                    index = j;
                }
            }
            // 최대값과 최우측값을 교환한다.
            int temp = data[(data.length-1)-i];
            data[(data.length-1)-i] = max;
            data[index] = temp;

            System.out.println("======================");
            System.out.print("{");
            for(int k = 0; k < data.length; k++) {
                System.out.print(data[k] + " ");
            }
            System.out.println("}");

        }

    }

}
